from flask_migrate import Migrate

import celery
import os
import sys
from swisesight.libs.utils.loghelper import init_logger
from dotenv import load_dotenv

logger = init_logger()
load_dotenv()

from configs.config import config_dict
from app.app_init import create_app, db

get_config_mode = os.environ.get("AIMSIGHT_MONITORING_CONFIG_MODE", "Debug")

try:
    config_mode = config_dict[get_config_mode.capitalize()]
except KeyError:
    sys.exit("Error: Invalid AIMSIGHT_MONITORING_CONFIG_MODE environment variable entry.")

# os.environ["FLASK_APP"] = "app:create_app(config_mode)"
redis_url =  os.environ.get("AIMSIGHT_MONITORING_REDIS_URL", "redis://127.0.0.1:6379")
celery_app = celery.Celery(__name__, broker=redis_url, backend=redis_url)
app = create_app(config_mode, celery_app)
Migrate(app, db)

if __name__ == "__main__":
    app.run(host=config_mode.HOST, port=config_mode.PORT, debug=config_mode.DEBUG)
