"""
  Copyright (c) 2020-2021  AIMsight SA
  Written By Olivier Chabloz
"""

import pandas as pd, numpy as np
import pytest
import os
from Dashboard.processing import (
    interpolation_prioritize,
    mask_isolated_values,
    process_Sim_matrices,
)
from skimage.transform import SimilarityTransform

pd.options.plotting.backend = "plotly"
pd.set_option("display.expand_frame_repr", False)
pd.set_option("display.max_columns", 15)
pd.set_option("display.max_rows", None)


def test_prioritize():
    signal = pd.read_csv("tests/datasets/635B_last_week.csv", parse_dates=["time_utc"])
    signal.set_index("time_utc", inplace=True)
    signal_processed = interpolation_prioritize(signal, 99)
    isolated_signal = mask_isolated_values(signal)
    signal_processed_isolated = interpolation_prioritize(isolated_signal, 99)
    assert signal == signal_processed


def test_new_prioritize():
    signal = pd.read_csv("tests/datasets/1820_N2_full.csv", parse_dates=["time_utc"])
    signal.set_index("time_utc", inplace=True)
    signal_processed = interpolation_prioritize(signal, 99)
    fig = signal_processed.plot()
    fig.show()
    assert True is False


def test_transform():
    import plotly.graph_objects as go
    from Dashboard.processing import filter_error_from_threshold

    signal = pd.read_csv(
        "tests/datasets/sim_transform_filter.csv", parse_dates=["time_utc", "time_utc_ref"]
    )
    signal = signal.set_index("id")

    filtered_signal = filter_error_from_threshold(signal)
    sim_mat = process_Sim_matrices(signal)

    pass
