"""
  Copyright (c) 2020-2021  AIMsight SA
  Written By Olivier Chabloz
"""
from opcode import opname
import pandas as pd
import pytest


@pytest.fixture
def test_app():
    from app.app_init import create_app
    from configs.config import config_dict

    config = config_dict["Debug"]
    config.TESTING = True
    app = create_app(config)
    return app


def test_get_sensors():
    from Dashboard.queries import (
        get_sensor_references,
        build_sensor_list,
        get_optical_shifts_from_sensors,
        process_matrices,
    )
    from Dashboard.processing import substract_reference, filter_error_from_threshold

    sensors = pd.DataFrame(
        build_sensor_list(
            8058, {"error_threshold": 0.7, "pattern_type": "target", "measurement_id": 13927612}
        )
    )
    refs = get_sensor_references(sensors)

    sn = sensors.serial_number
    shifts_raw = get_optical_shifts_from_sensors(sn, start_date="2021-01-24")
    shifts = shifts_raw.pipe(substract_reference, refs)
    shifts = shifts.pipe(filter_error_from_threshold)
    matrices = process_matrices(shifts)

    assert False is True


def test_convert_sensor_db(test_app):
    from app.extensions import AIMsightDB_new_session

    with test_app.app_context():
        from app.base.models import Device

        aim_db = AIMsightDB_new_session()
        devices = Device.get_all_devices()
        for d in devices:
            for k, o in d.outputs.items():
                s = aim_db.get_sensor(o.sensor1)
                if s is not None:
                    o.sensor1 = s.id
                s = aim_db.get_sensor(o.sensor2)
                if s is not None:
                    o.sensor2 = s.id
        Device.db_commit(d)


def test_get_sensors():
    from Dashboard.queries import (
        get_sensors_data,
        build_sensor_list,
        get_optical_shifts_from_sensors,
    )

    from Dashboard.processing import (
        substract_reference,
        filter_error_from_threshold,
        process_Sim_matrices,
    )
    from datetime import datetime

    sensors = [10092, 10093, 10094]
    default_params = {
        "error_threshold": 0.7,
        "pattern_type": "target",
        "measurement_id": None,
    }
    sensors_device = pd.DataFrame(build_sensor_list(8666, default_params))  # VS004

    sensors_to_fetch = sensors_device[
        sensors_device.sensor_id.isin(sensors) | (sensors_device.pattern_type == "global_ref")
    ]

    shifts = get_sensors_data(sensors_to_fetch, datetime(2022, 2, 28), "2022-03-01", True)
    corr_sim_mat = process_Sim_matrices(shifts)

    values_columns = [
        "measurement_id",
        "horizontal_shift",
        "vertical_shift",
        "x",
        "y",
        "x_ref",
        "y_ref",
    ]
    q = shifts[["sensor_id", "time_utc"] + values_columns]
    q = q.pivot(index="time_utc", columns="sensor_id", values=values_columns)

    d1 = q.loc[:, (slice(None), 10092)]

    d1.columns = d1.columns.get_level_values(0)
    d1 = d1.reset_index().merge(corr_sim_mat, left_on="measurement_id", right_on="measurement_id")
    d1 = d1.set_index("time_utc")

    from skimage.transform import SimilarityTransform
    import numpy as np

    def apply_transform(row):
        t = SimilarityTransform(row.matrix)
        coords = np.array([row.x, row.y])
        tr = t.inverse(coords)
        row.x = tr[0][0]
        row.y = tr[0][1]
        return row

    d1 = d1.apply(apply_transform, axis=1)
    rx = d1["x"] - d1["x_ref"]
    ry = d1["y"] - d1["y_ref"]
    result = pd.DataFrame({"horizontal_shift": rx, "vertical_shift": ry})

    assert False is True

    print("yay")