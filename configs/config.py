"""
  Copyright (c) 2020-2021  AIMsight SA
  Written By Olivier Chabloz
"""
import os

dir_path = os.path.dirname(os.path.realpath(__file__))


class Config(object):
    SECRET_KEY = os.environ.get("AIMSIGHT_MONITORING_SECRET_KEY")
    CONFIG_FOLDER = os.environ.get("AIMSIGHT_MONITORING_CONFIG_FOLDER", dir_path)
    SQLALCHEMY_DATABASE_URI = "sqlite:///{}/database.db".format(CONFIG_FOLDER)
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # THEME SUPPORT
    #  if set then url_for('static', filename='', theme='')
    #  will add the theme name to the static URL:
    #    /static/<DEFAULT_THEME>/filename
    # DEFAULT_THEME = "themes/dark"
    TESTING = False
    DEFAULT_THEME = None
    AUTH0_CLIENT_ID = os.environ.get("AUTH0_CLIENT_ID")
    AUTH0_DOMAIN = os.environ.get("AUTH0_DOMAIN")
    AUTH0_CLIENT_SECRET = os.environ.get("AUTH0_CLIENT_SECRET")
    AUTH0_CALLBACK_URL = os.environ.get("AUTH0_CALLBACK_URL")
    AUTH0_AUDIENCE = os.environ.get("AUTH0_AUDIENCE")

    AUTH0_API_CLIENTID = os.environ.get("AUTH0_API_CLIENTID")
    AUTH0_API_CLIENT_SECRET = os.environ.get("AUTH0_API_CLIENT_SECRET")
    AUTH0_API_AUDIENCE = os.environ.get("AUTH0_API_AUDIENCE")

    MAIL_SERVER = os.environ.get("MAIL_SERVER")
    MAIL_PORT = os.environ.get("MAIL_PORT")
    MAIL_USE_TLS = os.environ.get("MAIL_USE_TLS", "False").lower() in ("true", "1", "t")
    MAIL_USE_SSL = os.environ.get("MAIL_USE_SSL", "False").lower() in ("true", "1", "t")
    MAIL_USERNAME = os.environ.get("MAIL_USERNAME")
    MAIL_PASSWORD = os.environ.get("MAIL_PASSWORD")
    MAIL_DEFAULT_SENDER = os.environ.get("MAIL_DEFAULT_SENDER")
    TWILIO_USER = os.environ.get("TWILIO_USER")
    TWILIO_PASSWD = os.environ.get("TWILIO_PASSWD")
    TWILIO_PHONE = os.environ.get("TWILIO_PHONE")


class ProductionConfig(Config):
    DEBUG = False
    HOST = os.environ.get("MONITORING_HOST", "localhost")
    PORT = os.environ.get("MONITORING_PORT", 40092)


class DebugConfig(Config):
    DEBUG = True
    HOST = os.environ.get("MONITORING_HOST", "localhost")
    PORT = os.environ.get("MONITORING_PORT", 40092)


config_dict = {"Production": ProductionConfig, "Debug": DebugConfig}
