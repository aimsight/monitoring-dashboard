"""
  Copyright (c) 2020-2023  AIMsight SA
  Written By Olivier Chabloz
"""
import os

MONITORING_HOST = os.environ.get("MONITORING_HOST", "0.0.0.0")
MONITORING_PORT = os.environ.get("MONITORING_PORT", 40092)

bind = f"{MONITORING_HOST}:{MONITORING_PORT}"
workers = int(os.environ.get("MONITORING_WORKERS", 4))
worker_class = "gevent"
accesslog = "-"
loglevel = os.environ.get("MONITORING_LOGLEVEL", "info")
capture_output = True
enable_stdio_inheritance = True
timeout = int(os.environ.get("MONITORING_TIMEOUT", 300))