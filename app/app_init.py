"""
  Copyright (c) 2020-2021  AIMsight SA
  Written By Olivier Chabloz
"""
from flask import Flask, url_for
from .extensions import db, AIMsightDB
from importlib import import_module
from .base.models import Device
from .base.auth import register_auth, is_authenticated, is_admin, add_user as auth_add_user
from .tasks.async_tasks import celery_tasks
from .tasks.recuring_tasks import check_alarms as task_check_alarms
from .base.routes import register_errorhandlers
from Dashboard.queries import sensor_process_from_ref
from Dashboard import AlarmManager, Explore, ExploreEdit, PatternCreation, System
import configs, click
from uuid import uuid4
from dash import CeleryManager
from os import path, environ
import logging

logger = logging.getLogger()
global_config = {"debug": False}


def register_extensions(app, config):
    global_config["debug"] = config.DEBUG
    db.init_app(app)


def register_blueprints(app):
    for module_name in ("base", "home", "dashFrames", "settings"):
        module = import_module("app.{}.routes".format(module_name))
        app.register_blueprint(module.blueprint)


def configure_database(app):
    with app.app_context():
        db.create_all()

    @app.teardown_request
    def shutdown_session(exception=None):
        db.session.remove()


def configure_logs(app):
    # for combine gunicorn logging and flask built-in logging module
    if __name__ != "__main__":
        gunicorn_logger = logging.getLogger("gunicorn.error")
        app.logger.handlers = gunicorn_logger.handlers
        app.logger.setLevel(gunicorn_logger.level)
    # endif


def apply_themes(app):
    """
    Add support for themes.

    If DEFAULT_THEME is set then all calls to
      url_for('static', filename='')
      will modfify the url to include the theme name

    The theme parameter can be set directly in url_for as well:
      ex. url_for('static', filename='', theme='')

    If the file cannot be found in the /static/<theme>/ lcation then
      the url will not be modified and the file is expected to be
      in the default /static/ location
    """

    @app.context_processor
    def override_url_for():
        user_session = is_authenticated()

        user_is_admin = is_admin()
        is_logged = user_session is not None
        user_session = user_session or {}
        if user_is_admin:
            devices = Device.get_all_devices()
        else:
            devices = Device.get_for_user(user_session.get("user_id", None))
        return dict(
            url_for=_generate_url_for_theme,
            is_logged=is_logged,
            Is_admin=user_is_admin,
            explore=devices,
            **user_session,
        )

    def _generate_url_for_theme(endpoint, **values):
        if endpoint.endswith("static"):
            themename = values.get("theme", None) or app.config.get("DEFAULT_THEME", None)
            if themename:
                theme_file = "{}/{}".format(themename, values.get("filename", ""))
                if path.isfile(path.join(app.static_folder, theme_file)):
                    values["filename"] = theme_file
        return url_for(endpoint, **values)


def register_CLIs(app):
    @app.cli.command()
    @click.argument("name")
    @click.argument("email")
    @click.argument("password")
    def add_user(name, email, password):
        """Run a Threshold alarm check"""
        db.create_all()
        auth_add_user(name, email, password, is_admin=True)

    @app.cli.command()
    @click.argument("start_date")
    @click.argument("sensors", nargs=-1)
    def reprocess_sensors(start_date, sensors):
        sensor_process_from_ref(sensors, start_date)

    @app.cli.command()
    def check_alarms():
        task_check_alarms()


def create_app(config=None, celery_handle=None):
    if config is None:
        get_config_mode = environ.get("AIMSIGHT_MONITORING_CONFIG_MODE", "Debug")
        config = configs.config.config_dict[get_config_mode]
    app = Flask(__name__, static_folder="base/static")
    app.config.from_object(config)
    # cache_uuid = uuid4()
    # logger.info("Cache UUID: %s", cache_uuid.__repr__())
    if celery_handle is not None:
        app.celery_handle = celery_handle
        celery_tasks(app, celery_handle)
    register_extensions(app, config)
    register_blueprints(app)
    register_auth(app, config)
    configure_database(app)
    configure_logs(app)
    apply_themes(app)
    register_CLIs(app)
    app = Explore.Add_Dash(app)
    app = ExploreEdit.Add_Dash(app)
    app = PatternCreation.Add_Dash(app)
    app = System.Add_Dash(app)
    app = AlarmManager.Add_Dash(app)
    app = register_errorhandlers(app)

    return app
