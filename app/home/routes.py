"""
  Copyright (c) 2020-2021  AIMsight SA
  Written By Olivier Chabloz
"""
from . import blueprint
from flask import render_template
from ..base.auth import is_authenticated, report_user, is_admin, requires_admin
from ..base.notifications import get_notifications
from Dashboard.queries import get_battery_report

from logging import getLogger

logger = getLogger()


@blueprint.route("/")
def index():
    session = is_authenticated()
    if session is None:
        return render_template("index_unlogged.html")

    report_user("Homepage", "/")
    user_notifications = get_notifications(session["user_id"], unread_only=True)
    return render_template("index.html", notifications=user_notifications)


@blueprint.route("/reference")
def reference_page():
    return render_template("regular_page.html")


@blueprint.route("/tasks")
@requires_admin
def tasks_page():
    return render_template("tasks_page.html")


@blueprint.route("/system_banner")
def system_banner_page():
    if is_admin():
        bat_lvl = get_battery_report()
        export_bat_lvl = bat_lvl.droplevel(0, axis=1).to_dict(orient="list")
    else:
        export_bat_lvl = dict()
    return render_template("system_banner.html", bat_lvl=export_bat_lvl)
