PATTERN_TYPES_COLORS = {
    "global_ref": "lime",
    "ref": "yellow",
    "target": "aqua",
    "target02": "magenta",
    "target03": "ivory",
    "target04": "darkorange",
    "target05": "olivedrab",
}