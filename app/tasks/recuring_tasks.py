from logging import getLogger
from datetime import datetime, timedelta
import json
from app.base.models import Device, Alarm
from app.base.auth import get_user_info
from app.base.send_notification import send_notification, send_email, send_sms
from Dashboard.Dash_fun import load_parent_alarm_from_model
import pytz

logger = getLogger()


def check_rearm_alarms():
    for dev in Device.get_all_devices(no_archive=True):
        dev_alarms = dev.alarms.filter_by(active=False)
        for alarm in dev_alarms:
            alarm_event = alarm.get_last_event()
            if alarm_event is not None and alarm_event.status == "active":
                if alarm.rearming_period in [None, -1]:
                    continue
                rearming_date = alarm_event.event_date + timedelta(hours=alarm.rearming_period)
                if rearming_date <= datetime.now():
                    alarm.update(active=True)
                    alarm_event.status = json.dumps({"mode": "auto", "user_id": "system"})
                    alarm_event.db_commit()
                    logger.info("Alarm %s from Device %s REARMED", alarm.label, dev.name)


def check_alarms():
    check_rearm_alarms()
    events = []
    for dev in Device.get_all_devices(no_archive=True):
        dev_alarms = dev.get_parents_or_single_alarms()
        if dev_alarms.count() == 0:
            continue
        logger.info("Alarm Processing: Checking Device %s", dev.name)
        for alarm in dev_alarms:
            try:
                alarm_data = load_parent_alarm_from_model(dev, alarm, active_only=True)
            except ValueError:
                logger.critical("Alarm %d (%s): Error while loading", alarm.id, alarm.label)

            if alarm_data.empty:
                continue

            def process_single(df):
                if df.trigger.any():
                    first = df.loc[df.trigger].iloc[0]
                    alarm = Alarm.query.filter_by(id=df.name).one()
                    ev = alarm.new_event(first.time_utc.to_pydatetime(), df.to_json(), first.value)
                    logger.info("New Alarm Event for Alarm %s on Device %s", alarm.label, dev.name)
                    events.append(ev)

            alarm_data.groupby("alarm_id").apply(process_single)

    for ev in events:
        generate_alarm_report(ev)


def generate_alarm_report(alarm_event):
    # create a list of recipient for sms, email and web notification
    if alarm_event.alarm.group is None:
        logger.warning("Alarm %s (%d) has no attributed group", alarm_event.alarm.label, alarm_event.alarm.id)
        return
    sms_list, email_list, notif_list = [], [], []
    for user in alarm_event.alarm.group.users:
        info = get_user_info(user.user_id)
        email_list.append({"email": info["email"], "name": info["name"]})
        notif_list.append({"user_id": user.user_id})
        metadata = info.get("user_metadata", {})
        sms_group_enabled = alarm_event.alarm.group.sms_notifications
        if sms_group_enabled and metadata.get("allow_sms", False):
            phone_number = metadata.get("phone_number", "")
            if len(phone_number) > 10:
                sms_list.append({"phone_number": phone_number, "name": info["name"]})
            else:
                log_text = "Alarm Notification: Phone number of user '%s' (%s) is not defined properly"
                logger.warning(log_text, user.user_id, info["name"])
    # add extra sms and extra emails:
    if len(alarm_event.alarm.group.extra_phones) > 0:
        extra_phones = alarm_event.alarm.group.extra_phones.split(",")
        sms_list.extend([{"phone_number": num, "name": ""} for num in extra_phones])
    if len(alarm_event.alarm.group.extra_emails) > 0:
        extra_emails = alarm_event.alarm.group.extra_emails.split(",")
        email_list.extend([{"email": email, "name": ""} for email in extra_emails])

    template_context = {
        "user_group": {
            "name": alarm_event.alarm.group.name,
            "description": alarm_event.alarm.group.description,
        },
        "alarm": alarm_event.alarm,
        "alarm_event": alarm_event,
        "link": f"/explore/{alarm_event.alarm.device.name}",
    }

    if len(sms_list) > 0:
        send_sms(sms_list, f"alarms/sms_{alarm_event.alarm.template}.txt", template_context)
    if len(email_list) > 0:
        send_email(email_list, f"alarms/email_{alarm_event.alarm.template}.html", template_context)
    if len(notif_list) > 0:
        send_notification(
            notif_list, f"alarms/notification_{alarm_event.alarm.template}.html", template_context
        )
