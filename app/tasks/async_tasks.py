import celery
import time
import pandas as pd
from Dashboard.queries import sensor_process_from_ref
from Dashboard.Dash_fun import query_and_process_outputs
from app.base.models import Device
from .recuring_tasks import check_alarms

from celery.utils.log import get_task_logger
from celery.schedules import crontab

logger = get_task_logger(__name__)


def celery_tasks(flask_instance, celery_instance: celery.Celery):
    @celery_instance.task
    def a_super_task():
        return "hello world"

    @celery_instance.task(bind=True)
    def a_long_task(self, n):
        observer = lambda x, y: None
        if not self.request.called_directly:
            observer = lambda x, y: self.update_state(state="PROGRESS", meta={"current": x, "total": y})
        for i in range(n):
            time.sleep(1)
            observer(i, n)

        return "coucou.txt", f"done {n} tasks"

    @celery_instance.task(bind=True)
    def process_sensor_from_ref(self, acquisition_unit_id, sensor_list):
        observer = lambda x, y: None
        if not self.request.called_directly:
            observer = lambda x, y: self.update_state(state="PROGRESS", meta={"current": x, "total": y})
        with flask_instance.app_context():
            logger.info("Starting processing for Acquisition_unit %d", acquisition_unit_id)
            sensor_process_from_ref(sensors=sensor_list, observer=observer)

    @celery_instance.task(bind=True)
    def prepare_explore_download(
        self, device_name, format, start_date, end_date, filtered_outputs, no_filter=False, median=False
    ):
        self.update_state(state="PROGRESS")
        with flask_instance.app_context():
            dev = Device.get_from_pathname(device_name)
            outputs = list()
            for _, d in dev.outputs.items():
                if len(filtered_outputs) > 0 and d.name not in filtered_outputs:
                    continue
                d = d.as_dict()
                if no_filter:
                    d.update(
                        {
                            "scale": 1,
                            "horizontal_offset": 0,
                            "vertical_offset": 0,
                            "interp": None,
                            "roll_med": 1,
                        }
                    )
                outputs.append(d)
            result = query_and_process_outputs(
                dev,
                outputs,
                dev.threshold,
                start_date,
                end_date,
                dev.reference_measurement,
            )
            try:
                full = pd.concat([x for x in result if x is not None])
            except ValueError:
                return None

            p = full.pivot(columns="signal_name")
            p = p.drop(p.columns[p.apply(lambda col: col.isnull().all())], axis=1)
            # renaming columns (flatten multiIndex)
            # i = ["{}.{}".format(a[1], a[0]) for a in p.columns]
            # p.columns.set_levels(i, level=1, inplace=True)
            # p.columns = p.columns.droplevel(0)
            if p.empty:
                return None
            if median:
                last_index = p.iloc[-1].name
                p = p.median().to_frame()
                p.rename(columns={0: last_index}, inplace=True)
                p = p.transpose()

            dev_filename = dev.name.replace(" ", "_")
            return f"{dev_filename}_from_{start_date}_to_{end_date}.{format}".format(
                dev_filename, start_date, end_date
            ), p.to_csv(sep=";")

    @celery_instance.task
    def periodic_alarm_check():
        with flask_instance.app_context():
            check_alarms()

    # @celery_instance.on_after_finalize.connect
    # def setup_periodic_tasks(sender, **kwargs):
    #     logger.info("Setting up periodic tasks")
    #     sender.add_periodic_task(10, a_super_task.s(), name='say hello every 10s')
    #     sender.add_periodic_task(1800, periodic_alarm_check.s(), name='check alarms every 30 min')

    celery_instance.conf.beat_schedule = {
        "Check alarm every 5 min": {
            "task": "app.tasks.async_tasks.periodic_alarm_check",
            "schedule": crontab(minute="*/5"),
        },
    }
