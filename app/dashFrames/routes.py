"""
  Copyright (c) 2020-2021  AIMsight SA
  Written By Olivier Chabloz
"""
from . import blueprint
from flask import render_template, request, send_file, url_for, abort
from Dashboard import AlarmManager, Explore, ExploreEdit, PatternCreation, System
from ..base.auth import requires_auth, can_explore_device, report_user, get_user_info
from ..base.models import Device, Alarm

import io, logging
import numpy as np, pandas as pd
from sqlalchemy.orm.exc import NoResultFound
from ..extensions import AIMsightDB

logger = logging.getLogger()


@blueprint.route("/<device_name>")
@can_explore_device
def explore_page(device_name):
    report_user(f"Explore {device_name}", f"/explore/{device_name}")
    try:
        dev = Device.query.filter_by(name=device_name).one()
    except NoResultFound:
        return abort(404)
    try:
        serial_number = AIMsightDB.nodes.filter_by(id=dev.serial_number).one().serial_number
    except NoResultFound:
        return abort(404)

    notifications = []
    users = {}
    for item in dev.get_notifications():
        item["time"] = item["date"].strftime("%d.%m.%Y %H:%M")
        if item["user"] not in users:
            users[item["user"]] = get_user_info(item["user"])
        item["author"] = users[item["user"]]["name"]
        notifications.append(item)

    return render_template(
        "explore.html",
        dash_url=Explore.url_base + dev.name,
        device_name=dev.name,
        device_id=serial_number,
        notifications=notifications,
        edit_url=url_for("explore_blueprint.edit_page", device_name=dev.name),
    )


@blueprint.route("/edit/<device_name>")
@requires_auth
def edit_page(device_name):
    report_user(f"/explore/edit/{device_name}")
    try:
        dev = Device.query.filter_by(name=device_name).one()
    except NoResultFound:
        return abort(404)

    serial_number = AIMsightDB.nodes.filter_by(id=dev.serial_number).one().serial_number
    return render_template(
        "edit.html",
        dash_url=ExploreEdit.url_base + dev.name,
        full_download=ExploreEdit.url_base + "/full_download/" + dev.name + "/pickle",
        device_name=dev.name,
        device_id=serial_number,
        explore_url=url_for("explore_blueprint.explore_page", device_name=dev.name),
        pat_creator_url=url_for("explore_blueprint.pattern_creation_page", device_name=dev.name),
    )


@blueprint.route("/pattern_creator/<device_name>")
@requires_auth
def pattern_creation_page(device_name):
    try:
        dev = Device.query.filter_by(name=device_name).one()
    except NoResultFound:
        return abort(404)
    serial_number = AIMsightDB.nodes.filter_by(id=dev.serial_number).one().serial_number
    return render_template(
        "edit.html",
        dash_url=PatternCreation.url_base + dev.name,
        full_download=ExploreEdit.url_base + "/full_download/" + dev.name + "/pickle",
        device_name=dev.name,
        device_id=serial_number,
        explore_url=url_for("explore_blueprint.explore_page", device_name=dev.name),
    )


@blueprint.route("/system/<system_name>")
@requires_auth
def system_page(system_name):
    system = AIMsightDB.get_account(system_name)
    if system is None:
        return abort(404)
    return render_template(
        "edit.html",
        dash_url=System.url_base + system_name,
        device_name=system_name,
    )


@blueprint.route("/alarms/<device_name>")
@requires_auth
def alarm_summary_page(device_name):
    try:
        dev = Device.query.filter_by(name=device_name).one()
    except NoResultFound:
        return abort(404)

    return render_template(
        "edit.html",
        dash_url=AlarmManager.url_base + dev.name,
        device_name=dev.name,
    )
