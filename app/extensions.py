"""
  Copyright (c) 2020-2021  AIMsight SA
  Written By Olivier Chabloz
"""
from flask_sqlalchemy import SQLAlchemy
from swisesight.libs.storage.aimsight_database import AIMsight_db
import os

db = SQLAlchemy()


def AIMsightDB_new_session():
    return AIMsight_db(
        os.environ.get("AIMSIGHT_DATABASE_TYPE", None),
        os.environ.get("AIMSIGHT_DATABASE_NAME", None),
        False,
        os.environ.get("AIMSIGHT_DATABASE_USER", None),
        os.environ.get("AIMSIGHT_DATABASE_PSWD", None),
        os.environ.get("AIMSIGHT_DATABASE_TURL", None),
    )


AIMsightDB = AIMsightDB_new_session()