"""
  Copyright (c) 2020-2021  AIMsight SA
  Written By Olivier Chabloz
"""
from flask import Blueprint

blueprint = Blueprint(
    "settings_blueprint",
    __name__,
    url_prefix="/settings",
    template_folder="templates",
    static_folder="static",
)
