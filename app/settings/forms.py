"""
  Copyright (c) 2020-2021  AIMsight SA
  Written By Olivier Chabloz
"""
from flask_wtf import FlaskForm
from wtforms import (
    StringField,
    DateField,
    DateTimeField,
    PasswordField,
    widgets,
    SelectMultipleField,
    SelectField,
    TextAreaField,
    IntegerField,
    BooleanField,
    SubmitField,
    ValidationError,
    validators,
)
from flask_wtf.file import FileField, FileAllowed
from app.base.models import User, Device, DeviceNotification, UserVisit, UserGroup, NoResultFound
from app.base.auth import get_users, get_user_info, update_user_info, is_authenticated, add_user
from app.extensions import AIMsightDB
from flask import request, current_app
from werkzeug.utils import secure_filename
import datetime, os


## login and registration
def log_event(params, page_url):
    user = is_authenticated()
    UserVisit.new(page_name=params, page_url=page_url, user_id=user["user_id"])


class add_user_Form(FlaskForm):
    username = StringField("Username", id="username_create")
    email = StringField("Email")
    password = PasswordField("Password", id="pwd_create")


class delete_user_Form(FlaskForm):
    username = StringField("Username", id="username_delete")


class setting_password_Form(FlaskForm):
    username = StringField("Username", id="username_setting")
    password = PasswordField("Password", id="pwd_setting")


class change_password_Form(FlaskForm):
    origin_password = PasswordField("Type Origin Password", id="origin_assword")
    new_password = PasswordField("Type New Password", id="new_assword")
    new_password2 = PasswordField("Type New Password Again", id="new_assword2")


class tableRow(widgets.TableWidget):
    def __init__(self):
        super().__init__(with_table_tag=False)

    def __call__(self, field, **kwargs):
        kwargs.setdefault("id", field.id)
        self.header = widgets.html_params(**kwargs)
        self.field = {subfield.label.text: str(subfield) for subfield in field}

        return super().__call__(field, **kwargs)


class MultiCheckboxField(SelectMultipleField):
    widget = tableRow()
    option_widget = widgets.CheckboxInput()


class UserSettings(FlaskForm):
    form_type = "User Settings"

    fullname = StringField(
        "Full name",
        id="user_fullname",
        validators=[validators.InputRequired()],
        render_kw={"class": "form-control", "placeholder": "Full Name"},
    )

    email = StringField(
        "Email",
        id="user_email",
        validators=[validators.InputRequired()],
        render_kw={"class": "form-control", "readonly": True},
    )

    password = PasswordField(
        "Password",
        id="password_field",
        validators=[validators.EqualTo("password_repeat", message="Passwords must match")],
    )
    password_repeat = PasswordField("Repeat Password", id="password_repeat")

    phone = StringField(
        "Phone number",
        id="user_phone",
        validators=[
            validators.Optional(),
            validators.Regexp(r"^\+[0-9]{11,15}$", message="Not a valid phone number (+41..)"),
        ],
        render_kw={"class": "form-control", "placeholder": "+41.."},
    )
    allow_sms_notif = BooleanField("SMS notifications", render_kw={"class": "form-control"})

    submit = SubmitField("Save")

    def __init__(self, user_id=None, fetch=False):
        super().__init__()
        self.user_id = user_id
        if user_id is not None and fetch:
            user_info = get_user_info(user_id)
            self.email.data = user_info["email"]
            self.fullname.data = user_info["name"]
            user_metadata = user_info.get("user_metadata", {})
            self.phone.data = user_metadata.get("phone_number", "")
            self.allow_sms_notif.data = user_metadata.get("allow_sms", True)
        elif user_id is None:
            self.email.render_kw["readonly"] = False
        self.visible_fields = [
            self.fullname,
            self.email,
            self.password,
            self.password_repeat,
            self.phone,
            self.allow_sms_notif,
        ]

    def create(self):
        return add_user(
            name=self.fullname.data,
            email=self.email.data,
            password=self.password.data,
            phone=self.phone.data,
            allow_sms=self.allow_sms_notif.data,
        )

    def update(self):
        log_event(f"user update:{self.user_id}", "/settings/user")
        return update_user_info(
            self.user_id,
            name=self.fullname.data,
            phone=self.phone.data,
            password=self.password.data,
            allow_sms=self.allow_sms_notif.data,
        )

    @property
    def title(self):
        return self.email.data


class UserGroupSettings(FlaskForm):
    form_type = "Group Settings"
    name = StringField(
        "Group name",
        id="group_name",
        validators=[validators.InputRequired()],
        render_kw={"class": "form-control", "placeholder": "Group name"},
    )
    description = StringField(
        "Description",
        id="description",
        render_kw={"class": "form-control"},
    )
    users = SelectMultipleField("Users", id="user_items", render_kw={"class": "form-control"})
    extra_phones = StringField(
        "Extra phones",
        render_kw={"class": "form-control"},
        validators=[
            validators.Optional(),
            validators.Regexp(  # match +41765439321,+41765439321 or +41765439321
                r"^(\+[0-9]{11,15},)*\+[0-9]{11,15}$",
                message="Must be comma (,) separated phones (+41000000000)",
            ),
        ],
    )
    extra_emails = StringField(
        "Extra emails",
        render_kw={"class": "form-control"},
        validators=[
            validators.Optional(),
            validators.Regexp(
                r"^([\w+-.%]+@[\w.-]+\.[A-Za-z]{2,4})(,[\w+-.%]+@[\w.-]+\.[A-Za-z]{2,4})*$",
                message="Must be comma (,) separated emails",
            ),
        ],
    )

    sms_notif = BooleanField("SMS notifications", render_kw={"class": "form-control"})

    submit = SubmitField("Save")
    delfield = SubmitField("Delete")

    def __init__(self, device_id, group_id=None):
        super().__init__()
        self.device_id = int(device_id)
        self.group_id = group_id

        self.visible_fields = [
            self.name,
            self.description,
            self.extra_phones,
            self.extra_emails,
            self.users,
            self.sms_notif,
        ]

    def load_from_db(self):
        if self.group_id is None:
            raise NoResultFound
        grp = UserGroup.get(self.group_id)
        if self.device_id != grp.device_id:
            raise NoResultFound

        self.name.data = grp.name
        self.description.data = grp.description
        self.extra_emails.data = grp.extra_emails
        self.extra_phones.data = grp.extra_phones
        self.users.data = [x.user_id for x in grp.users]
        self.sms_notif.data = grp.sms_notifications

    @property
    def title(self):
        dev = Device.get_by_id(self.device_id)
        return dev.name

    def load_choices(self):
        users = get_users()
        self.users.choices = [(u["user_id"], u["name"]) for u in users]
        self.users.choices.sort(key=lambda x: x[1])

    def check_delete(self, force=False):
        if not self.delfield.data and not force:
            return False
        UserGroup.get(self.group_id).delete()
        return True

    def save(self):
        if self.group_id is not None:
            grp = UserGroup.get(self.group_id)
        else:
            grp = UserGroup.new(self.device_id)
        grp.update_values(
            name=self.name.data,
            desc=self.description.data,
            extra_emails=self.extra_emails.data,
            extra_phones=self.extra_phones.data,
            users=self.users.data,
            sms_notifications=self.sms_notif.data,
        )


class DeviceSettings(FlaskForm):
    """
    Form Class representings parameters for a Group
    Fields:
        - device_name: Str, Unique
        - serial_number: Int (AIMsight db node_id)
        - users_checkbox: list of user_id with access
        - pattern_file: filename of the static image to display
        - description: Text, A small description of the group
        - reference_measurement: The default reference measurement
        - start_date: minimum date allowed for query
        - archive_date: maximum date allowed for query, Optional

    ! DeviceSettings should be groupSettings since our terminology has changed
    """

    device_name = StringField(
        "Device Name",
        id="device_name_settings",
        validators=[validators.InputRequired()],
        render_kw={"class": "form-control", "placeholder": "Group name"},
    )
    serial_number = SelectField("Serial Number", render_kw={"class": "form-control"})

    pattern_file = FileField(
        label="Patterns File",
        id="pattern_file_settings",
        render_kw={"class": "form-control", "accept": ".jpg,.jpeg,.png"},
        validators=[FileAllowed(["jpeg", "jpg", "png"], "An image is required")],
    )
    description = TextAreaField(
        label="Description", id="description_field", render_kw={"class": "form-control"}
    )
    reference_measurement = IntegerField(
        label="Reference Measurement",
        id="ref_meas",
        render_kw={"class": "form-control"},
        validators=[validators.Optional()],
    )
    start_date = DateTimeField(
        "Start Date",
        id="start_date",
        validators=[validators.InputRequired()],
        render_kw={"class": "DateTimeSelectors form-control"},
        format="%d-%m-%Y %H:%M",
    )
    archive_date = DateTimeField(
        "Archive Date",
        id="archive_date",
        render_kw={"class": "DateTimeSelectors form-control"},
        format="%d-%m-%Y %H:%M",
        validators=[validators.Optional()],
    )
    min_autoscale = IntegerField(
        label="Minimum Auto scale value",
        render_kw={"class": "form-control"},
        validators=[validators.Optional()],
        description="Set the minimum auto scale value, will be overridden if the auto-scale value goes bellow",
    )
    max_autoscale = IntegerField(
        label="Maximum Auto scale value",
        render_kw={"class": "form-control"},
        validators=[validators.Optional()],
        description="Set the maximal auto scale value, will be overridden if the auto-scale value goes higher",
    )

    upload_date = DateTimeField(
        "Media capture date",
        id="capture_date",
        render_kw={"class": "DateTimeSelectors form-control"},
        format="%d-%m-%Y %H:%M",
        validators=[validators.Optional()],
    )
    upload_file = FileField(
        label="Media file",
        render_kw={"class": "form-control", "accept": ".jpg,.jpeg,.png,.mp4,.avi"},
        validators=[FileAllowed(["jpg", "png", "mp4", "avi"], "Media files")],
    )

    submit = SubmitField("Save")
    delfield = SubmitField("Delete")

    def __init__(self, device_name=None):
        super().__init__()
        self.get_serial_number_choices()
        self.groups = {}
        self.device_id = None
        if not device_name is None:
            self.load_from_db(device_name)
        self.left_fields = [
            self.device_name,
            self.serial_number,
            self.pattern_file,
            self.description,
            self.reference_measurement,
            self.start_date,
            self.archive_date,
            self.min_autoscale,
            self.max_autoscale,
        ]
        self.upload_fields = [self.upload_date, self.upload_file]

    def update_device(self, device_name):
        dev = Device.query.filter_by(name=device_name).one()  # throw error if not found
        self.save(dev)

    def save_new(self):
        dev = Device(self.device_name.data)
        try:
            dev.add_to_db()
        except:
            dev.rollback()
            return False
        else:
            self.save(dev)
            return True

    def resolve_device_id(self, device_name):
        dev = Device.query.filter_by(name=device_name).one()
        self.device_id = dev.id

    def load_from_db(self, device_name):
        """
        Load the Device object into the DeviceSettings object
        """
        self.resolve_device_id(device_name)
        dev = Device.query.filter_by(id=self.device_id).one()
        self.serial_number.data = str(dev.serial_number)
        self.device_name.data = dev.name
        self.reference_measurement.data = dev.reference_measurement
        self.description.data = dev.description
        self.start_date.data = dev.start_date
        self.archive_date.data = dev.end_date
        self.min_autoscale.data = dev.min_autoscale
        self.max_autoscale.data = dev.max_autoscale

    def save(self, dev: Device):
        """
        Update the database entry of the given Device object with DeviceSettings parameters
        """
        dev.serial_number = self.serial_number.data
        self.device_name.data = self.device_name.data.strip()
        dev.name = self.device_name.data
        dev.reference_measurement = self.reference_measurement.data
        dev.description = self.description.data
        dev.start_date = self.start_date.data
        dev.end_date = self.archive_date.data
        dev.max_autoscale = self.max_autoscale.data
        dev.min_autoscale = self.min_autoscale.data

        if self.pattern_file.data is not None:
            filename = secure_filename(f"{dev.name}_{self.pattern_file.data.filename}")
            filePath = os.path.join(current_app.config["CONFIG_FOLDER"], "images", filename)
            self.pattern_file.data.save(filePath)
            dev.patterns = filename

        dev.db_commit()

    def check_delete(self, device_name):
        if not self.delfield.data:
            return False
        Device.get(device_name).delete()
        return True

    def get_users_groups(self):
        """
        Fetch the user list
        """
        users = get_users()
        # create a list of value/description tuples
        user_list = {x["user_id"]: x["name"] for x in users}
        if self.device_id is None:
            self.resolve_device_id(self.device_name.data)
        dev = Device.get_by_id(self.device_id)
        self.groups = {
            (x.name, x.id): [(k.user_id, user_list[k.user_id]) for k in x.users] for x in dev.user_groups
        }
        for grps in self.groups.values():
            grps.sort(key=lambda x: x[1])

    def get_serial_number_choices(self):
        """
        Fetch AIMsightDB.acquisition_units
        """
        acq = (
            AIMsightDB.acquisition_units.join(AIMsightDB.T.Node)
            .order_by(AIMsightDB.T.Node.serial_number)
            .all()
        )
        self.serial_number.choices = [(a.id, a.serial_number) for a in acq]
