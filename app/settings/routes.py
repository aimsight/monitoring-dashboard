"""
  Copyright (c) 2020-2021  AIMsight SA
  Written By Olivier Chabloz
"""
from logging import getLogger
import datetime

from flask import render_template, request, redirect, url_for
from .forms import (
    DeviceSettings,
    UserSettings,
    UserGroupSettings,
    log_event,
)
from . import blueprint
from ..base.models import Device, DeviceNotification, NoResultFound
from ..base.auth import requires_auth, is_authenticated, requires_admin, get_users, delete_user


logger = getLogger(__name__)


@blueprint.route("/device/<device_name>", methods=["GET", "POST"])
@requires_admin
def settings_device(device_name):
    form = DeviceSettings()

    title = f"{device_name} settings"
    if form.is_submitted():
        if form.check_delete(device_name):
            return redirect(url_for("home_blueprint.index"))
        if form.validate():
            form.update_device(device_name)
        if len(form.errors) > 0:
            status = form.errors.__repr__()
            form.get_users_groups()
            return render_template("device_settings.html", form=form, status=status, title=title)
        return redirect(  # redirect to the new device_name settings since it is possible that the device name has changed
            url_for("settings_blueprint.settings_device", device_name=form.device_name.data, message="OK")
        )
    else:
        try:
            form.load_from_db(device_name)
        except NoResultFound:
            return redirect("/page_404")
    status = request.args.get("message", "")
    form.get_users_groups()
    return render_template("device_settings.html", form=form, status=status, title=title)


@blueprint.route("/notification/<device_name>", methods=["POST", "GET"])
@requires_admin
def add_notification(device_name):
    dev = Device.get(device_name)
    form = request.form.to_dict()
    user = is_authenticated()
    date_formatted = datetime.datetime.strptime(form["date"], "%d-%m-%Y %H:%M")
    dev.new_notification(
        form["title"],
        date_formatted,
        form["description"],
        user["user_id"],
    )
    return redirect(f"/explore/{device_name}")


@blueprint.route("/notification/<device_name>/delete/<notification_id>", methods=["GET"])
@requires_admin
def delete_notification(device_name, notification_id):
    DeviceNotification.delete(notification_id)
    return redirect(f"/explore/{device_name}")


@blueprint.route("/new_device", methods=["GET", "POST"])
@requires_admin
def new_device():
    form = DeviceSettings()
    status = ""
    if form.is_submitted():
        if form.validate():
            if form.save_new():
                return redirect(  # redirect to settings_device with ok message
                    url_for(
                        "settings_blueprint.settings_device",
                        device_name=form.device_name.data,
                        message="OK",
                    )
                )
            else:  # save_new failed because the device name already exists
                status = "Device Name already exists"
        else:
            status = form.errors.__repr__()
    return render_template("device_settings.html", form=form, status=status, title="New Device")


@blueprint.route("/add_user", methods=["GET", "POST"])
@requires_admin
def add_User():
    # form = add_user_Form(request.form)
    # if "Add" in request.form:
    #     user = User.query.filter_by(username=request.form["username"]).first()
    #     email = User.query.filter_by(email=request.form["email"]).first()
    #     if user:
    #         status = "Username is existing"
    #     elif email:
    #         status = "Email is existing"
    #     else:
    #         User(**request.form).add_to_db()
    #         status = "Add user success !"
    #     return render_template("add_user.html", form=form, status=status)
    # return render_template("add_user.html", form=form, status="")
    return redirect("/page_403")


@blueprint.route("/users", methods=["GET", "POST"])
@requires_admin
def setting_users():
    users_info = get_users()
    users_info.sort(key=lambda x: x["name"])
    status = status = request.args.get("status", "")
    return render_template("list_users.html", users_info=users_info, status=status)


@blueprint.route("/delete_user/<user_id>", methods=["GET"])
@requires_admin
def delete_user_route(user_id):
    status = "OK" if delete_user(user_id) else "Something went wrong"
    log_event(f"user delete: {user_id}", "/settings/delete_user/{user_id}")
    return redirect(url_for("settings_blueprint.setting_users", status=status))


@blueprint.route("/user", methods=["GET", "POST"])
@requires_auth
def settings_user():
    user = is_authenticated()
    form = UserSettings(user["user_id"])
    if form.is_submitted():
        status = "Something went wrong"
        if form.validate():
            if form.update():
                status = "OK"
        if len(form.errors) > 0:
            status = form.errors.__repr__()
        return render_template("generic_settings.html", form=form, status=status)

    else:
        try:
            form = UserSettings(user["user_id"], fetch=True)
        except NoResultFound:
            return redirect("/page_404")
    status = request.args.get("message", "")
    return render_template("generic_settings.html", form=form, status=status)


@blueprint.route("/user/<user_id>", methods=["GET", "POST"])
@requires_admin
def settings_specific_user(user_id):
    form = UserSettings(user_id)
    if form.is_submitted():
        status = "Something went wrong"
        if form.validate():
            if form.update():
                status = "OK"
        if len(form.errors) > 0:
            status = form.errors.__repr__()
        return render_template("generic_settings.html", form=form, status=status)

    else:
        try:
            form = UserSettings(user_id, fetch=True)
        except NoResultFound:
            return redirect("/page_404")
    status = request.args.get("message", "")
    return render_template("generic_settings.html", form=form, status=status)


@blueprint.route("/new_user", methods=["GET", "POST"])
@requires_admin
def settings_new_user():
    form = UserSettings()
    if form.is_submitted():
        status = "Something went wrong"
        if form.validate():
            if form.create():
                status = "OK"
        if len(form.errors) > 0:
            status = form.errors.__repr__()
        return render_template("generic_settings.html", form=form, status=status)

    status = request.args.get("message", status)
    return render_template("generic_settings.html", form=form, status=status)


@blueprint.route("/group/<device_id>/<group_id>", methods=["GET", "POST"])
@requires_admin
def goup_creation_and_update(device_id, group_id):
    group_id = None if group_id == "new" else int(group_id)
    form = UserGroupSettings(device_id=device_id, group_id=group_id)
    form.load_choices()
    if form.is_submitted():
        if form.check_delete():
            return redirect(url_for("settings_blueprint.settings_device", device_name=form.title))
        if form.validate():
            form.save()
        if len(form.errors) > 0:
            status = form.errors.__repr__()
            return render_template("generic_settings.html", form=form, status=status)
        return redirect(url_for("settings_blueprint.settings_device", device_name=form.title))
    elif group_id is None:
        return render_template("generic_settings.html", form=form)
    else:
        try:
            form.load_from_db()
        except NoResultFound:
            return redirect("/page_404")
    status = request.args.get("message", "")

    return render_template("generic_settings.html", form=form, status=status)


@blueprint.route("/group/<device_id>/<group_id>/delete")
@requires_admin
def delete_user_group(device_id, group_id):
    form = UserGroupSettings(device_id=device_id, group_id=group_id)
    form.check_delete(force=True)
    return redirect(url_for("settings_blueprint.settings_device", device_name=form.title))
