
if (!window.dash_clientside) { window.dash_clientside = {}; }
window.dash_clientside.clientside = {
    display: function (value) {
        if (typeof value === 'undefined') {
            return ""
        }
        var data = { time_utc: value["points"][0]["x"] }
        var event = new CustomEvent('showIRONdata', { detail: data })
        window.parent.document.dispatchEvent(event)
        return ""
    },
    select_y_data: function (data, selected) {
        return data[selected];
    }
}
