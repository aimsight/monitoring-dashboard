"""
  Copyright (c) 2020-2021  AIMsight SA
  Written By Olivier Chabloz
"""
from numpy import place
from app.base.auth.common import can_access_device
from flask import render_template, redirect, request
from uuid import uuid4

from ..extensions import AIMsightDB, AIMsight_db
from . import blueprint
from ..defines import PATTERN_TYPES_COLORS
from .models import Device, UserNotification
from .auth import requires_auth, is_authenticated, requires_admin, can_explore_device
from flask import request, send_file, abort, current_app, jsonify
from sqlalchemy.orm import exc
import os, zipfile, io, datetime, logging
import tempfile, sqlite3

from PIL import Image, ImageDraw, ImageFont
from aimsight_optical.pattern_converter import load_b64_image

logger = logging.getLogger()

dir_path = os.path.dirname(os.path.realpath(__file__))


@blueprint.route("/fixed_<template>")
@requires_auth
def route_fixed_template(template):
    return render_template("fixed/fixed_{}.html".format(template))


@blueprint.route("/page_<error>")
def route_errors(error):
    return render_template("errors/page_{}.html".format(error))


# @blueprint.route("/shutdown")
# def shutdown():
#     func = request.environ.get("werkzeug.server.shutdown")
#     if func is None:
#         raise RuntimeError("Not running with the Werkzeug Server")
#     func()
#     return "Server shutting down..."


## Errors


@blueprint.errorhandler(403)
def access_forbidden(error):
    return render_template("errors/page_403.html"), 403


@blueprint.errorhandler(404)
def not_found_error(error):
    return render_template("errors/page_404.html"), 404


@blueprint.errorhandler(500)
def internal_error(error):
    return render_template("errors/page_500.html"), 500


def register_errorhandlers(app):
    app.register_error_handler(403, access_forbidden)
    app.register_error_handler(404, not_found_error)
    app.register_error_handler(500, internal_error)
    return app


## Images
def get_image_from_pattern(device):
    path = os.path.join(current_app.config["CONFIG_FOLDER"], "patterns", device.patterns)
    if not os.path.exists(path):
        return None
    try:
        f = zipfile.ZipFile(path, "r")
        for ext in ["jpg", "png"]:
            try:
                b = io.BytesIO(f.read("capture.{}".format(ext)))
            except KeyError:
                pass
            else:
                return Image.open(b)
    except zipfile.BadZipFile:
        logger.warning("File %s is not a .zip file", device.patterns)


def get_image_file(device):
    path = os.path.join(current_app.config["CONFIG_FOLDER"], "images", device.patterns)
    if os.path.exists(path):
        return Image.open(path)
    return None


@blueprint.route("/pattern/<device_name>/qc.png")
def get_image(device_name):
    dev = Device.query.filter_by(name=device_name).one()
    im = None
    if dev.patterns is None:
        path = os.path.join(current_app.config["CONFIG_FOLDER"], "images", "default.png")
        if os.path.exists(path):
            im = Image.open(path)
    elif dev.patterns.endswith(".zip"):
        im = get_image_from_pattern(dev)
    else:
        im = get_image_file(dev)
    if im is None:
        return abort(404)

    thumbnail = request.args.get("thumbnail") is not None
    if thumbnail:
        w, h = im.size
        im = im.resize((int(w * 230 / h), 230))
        output = io.BytesIO()
        im.save(output, format="png")
        output.seek(0)
        return send_file(output, max_age=60, mimetype="image/png")

    imBin = io.BytesIO()
    im.save(imBin, format="png")
    imBin.seek(0)
    return send_file(imBin, max_age=60, mimetype="image/png")


def drawText(im, text) -> Image:
    """
    Draw text onto an PIL.Image instance at the bottom left corner of the image
    Usually for Timestamping purposes
    Returns the altered PIL.Image
    """
    d1 = ImageDraw.Draw(im)
    font_path = os.path.join(dir_path, "static", "images", "DejaVuSansMono.ttf")
    font = ImageFont.truetype(font_path, size=25)
    d1.text((28, im.height - 36), text, fill=(255, 0, 0), font=font)
    return im


def draw_sensors(im, sensor_list, im_ts, start_limit, draw_box=True, draw_pattern=False):
    d1 = ImageDraw.Draw(im)
    font_path = os.path.join(dir_path, "static", "images", "DejaVuSansMono.ttf")
    font = ImageFont.truetype(font_path, size=25)
    for s in sensor_list:
        proc_chain = s.get_active_processing_chain(im_ts, from_limit=start_limit)
        if proc_chain is None:
            continue
        parameters = proc_chain.parameters
        if not "corners" in parameters:
            continue
        if parameters.get("archive", False):
            continue
        color = PATTERN_TYPES_COLORS[parameters["pattern_type"]]
        corners = parameters["corners"]
        if draw_pattern:
            pat = load_b64_image(parameters["pattern"])
            im.paste(pat, [*corners[0], *corners[1]])
        if draw_box:
            d1.rectangle([*corners[0], *corners[1]], outline=color, width=3)
            d1.text((corners[0][0], corners[0][1] - 25), s.name, fill=color, font=font)


def query_measurement(dev, measurement_type, time_utc=None):
    """time_utc: by default the latest measurement otherwise the closest from time_utc"""
    acq_unit = AIMsightDB.acquisition_units.filter_by(id=dev.serial_number).first()
    if acq_unit is None:
        return None
    mtype = AIMsightDB.get_measurement_type(measurement_type)
    query = acq_unit.measurements.filter_by(measurement_type=mtype)

    if time_utc is not None:
        try:
            t, *_ = time_utc.split(".")
            time_utc = datetime.datetime.fromisoformat(t)
        except (TypeError, AttributeError):
            pass  # ? it is possible that time_utc is already a datetime ?
        except ValueError:
            try:
                time_utc = datetime.datetime.strptime(time_utc, "%Y-%m-%d %X.%f")
            except:
                return None
        # ! Check time_utc range from start_date to end_date
        if time_utc < dev.start_date:
            return None
        elif dev.is_archive and time_utc > dev.end_date:
            return None

        query = query.filter(AIMsightDB.T.Measurement.time_utc >= time_utc)
        m = query.order_by(AIMsightDB.T.Measurement.time_utc.asc()).first()
    else:
        if dev.is_archive:  # ! Get the latest image available BEFORE end_date
            query = query.filter(AIMsightDB.T.Measurement.time_utc <= dev.end_date)
        m = query.order_by(AIMsightDB.T.Measurement.time_utc.desc()).first()

    return m


@blueprint.route("/image/<measurement_id>")
@requires_admin
def get_mid_image_from_db(measurement_id):
    m = AIMsightDB.get_measurement(measurement_id)
    try:
        im = Image.open(io.BytesIO(m.data))
    except:
        abort(404)
    format_ = im.format.lower()
    return send_file(
        io.BytesIO(m.data),
        mimetype=f"image/{format_}",
        as_attachment=True,
        download_name=f"{m.acquisition_unit.serial_number}_diff_{m.time_utc.strftime('%d_%m_%YT%H_%M_%S')}.{format_}",
    )


@blueprint.route("/image/<device_name>/<int:measurement_id>/<wild>.<format>")
@can_explore_device
def get_device_mid_image_from_db(device_name, measurement_id, wild, format):
    if not can_access_device(device_name):
        return abort(403)

    if not format in ["jpg", "jpeg", "png"]:
        return abort(404)

    timestamp = request.args.get("timestamp") is not None
    drawSensors = request.args.get("sensors") is not None

    m = AIMsightDB.get_measurement(measurement_id)
    dev = Device.get(device_name)
    if m is None or m.acquisition_unit_id != int(dev.serial_number):
        return abort(404)
    try:
        im = Image.open(io.BytesIO(m.data))
    except:
        abort(404)  # measurement is not an image..

    format_ = im.format.lower()
    if timestamp or drawSensors or format != format_:
        if im.mode == "I":
            im = im.convert("RGB")
        if timestamp:
            t = m.time_utc.strftime("%d/%m/%Y, %H:%M:%S")
            drawText(im, t)
        if drawSensors:
            draw_sensors(im, m.acquisition_unit.sensors, m.time_utc, dev.start_date)
        output = io.BytesIO()
        save_fmt = {"png": "PNG", "jpg": "JPEG", "jpeg": "JPEG"}
        im.save(output, format=save_fmt[format])
        output.seek(0)
    else:
        output = io.BytesIO(m.data)

    return send_file(output, mimetype=f"image/{format}")


@blueprint.route("/latest/<device_name>/<im_type>.<format>")
@can_explore_device
def redirect_to_diff_image(device_name, im_type, format):
    if not can_access_device(device_name) and not format in ["jpg", "png"]:
        return abort(403)
    if im_type not in ["iron", "diff"]:
        return abort(404)

    time_utc = request.args.get("time_utc")

    # args that needs to be forwarded
    query_ = []
    query_str = ""
    for arg in ["timestamp", "sensors"]:
        query_str = "?"
        v = request.args.get(arg)
        if v is None:
            continue
        if len(v) > 0:
            query_.append(f"{arg}={v}")
        else:
            query_.append(f"{arg}")
    query_str += "&".join(query_)

    dev = Device.get(device_name)
    m = query_measurement(dev, f"OPTICAL_SHIFT-{im_type.upper()}", time_utc=time_utc)
    if m is None or m.acquisition_unit_id != int(dev.serial_number):
        return abort(404)
    t_info = m.time_utc.strftime("%Y-%m-%dT%H_%M_%S")
    return redirect(
        f"/image/{device_name}/{m.id}/{device_name}_{t_info}_{im_type}.{format}{query_str}", code=302
    )  # temp redirect


@blueprint.route("/legacy_latest/<device>/diff.<format>")
@requires_auth
def get_comparative_image(device, format):
    if not can_access_device(device) and not format in ["jpg", "png"]:
        return abort(403)

    dev = Device.get(device)
    time_utc = request.args.get("time_utc", dev.end_date)
    if not dev.end_date is None:
        if time_utc > dev.end_date:
            time_utc = dev.end_date
    timestamp = request.args.get("timestamp") is not None

    m = query_measurement(dev, "OPTICAL_SHIFT-DIFF", time_utc=time_utc)
    if m is None:
        return abort(404)
    if format == "png":
        return send_file(
            io.BytesIO(m.data),
            max_age=300,
            mimetype="image/png",
            as_attachment=True,
            download_name=f"{device}_diff_{m.time_utc.strftime('%d_%m_%YT%H_%M_%S')}.png",
        )

    im = Image.open(io.BytesIO(m.data)).convert("RGB")
    _, _, im_band = im.split()
    im_ref = Image.new("RGB", im.size, 0)
    draw_sensors(im_ref, m.acquisition_unit.sensors, m.time_utc, dev.start_date, False, True)
    _, _, im_ref_band = im_ref.split()
    im = Image.merge("RGB", (im_band, im_ref_band, im_band.point(lambda i: 0)))

    if timestamp:  # Applying a timestamp if need be
        t = m.time_utc.strftime("%d/%m/%Y, %H:%M:%S")
        drawText(im, t)
    # draw_sensors(im, m.acquisition_unit.sensors, m.time_utc,  dev.start_date, True, False)

    output = io.BytesIO()
    im.save(output, format="JPEG")
    output.seek(0)

    return send_file(output, max_age=300, mimetype="image/jpg")


@blueprint.route("/redirect/notification/<notification_id>")
@requires_auth
def redirect_notification(notification_id):
    """
    When Getting (GET) a notification, it is marked as read in the DB and then the user
    is redirected to the notification's link
    """
    session = is_authenticated()
    try:
        n = UserNotification.get(notification_id)
    except exc.NoResultFound:
        return abort(404)
    if n.user_id == session["user_id"]:
        n.set_read()
        n.db_commit()
        return redirect(n.link)
    return abort(404)


@blueprint.route("/notification/clear_all")
@requires_auth
def clear_notification():
    session = is_authenticated()
    user_id = session["user_id"]
    UserNotification.clear_all(user_id)
    return redirect("/")


@blueprint.route("/export_pattern/all")
@requires_admin
def export_all_patterns():
    f = tempfile.NamedTemporaryFile("r", delete=False)
    cp_db = AIMsight_db("sqlite", f.name)
    sq = AIMsightDB.measurement_types.filter(AIMsightDB.T.Measurement_type.name == "OPTICAL_SHIFT")
    mid = [m.id for m in sq.all()]
    q = AIMsightDB.measurements.filter(AIMsightDB.T.Measurement.measurement_type_id.in_(mid))
    q = q.distinct(AIMsightDB.T.Measurement.acquisition_unit_id)
    acquisition_units = [m.acquisition_unit for m in q]
    refs = []
    for acq_unit in acquisition_units:
        ref = acq_unit.node.node_type.reference
        if ref not in refs:
            refs.append(ref)
            nt = cp_db.T.Node_type(ref, "")
            nt.id = acq_unit.node.node_type.id
            cp_db.add_record(nt)
        n = cp_db.T.Node(nt, acq_unit.serial_number)
        n.id = acq_unit.id
        cp_db.add_record(n)

        cp_acq = cp_db.T.Acquisition_unit(n)
        cp_db.add_record(cp_acq)

        for s in acq_unit.sensors:
            cp_s = cp_db.T.Sensor(id=s.id, name=s.name, acquisition_unit_id=cp_acq.id)
            cp_db.add_record(cp_s)
            proc = s.get_active_processing_chain(datetime.date.today())
            if proc is None:
                continue
            new_proc_chain = cp_db.T.Processing_chain(
                id=proc.id,
                name=proc.name,
                version=proc.version,
                parameters=proc.parameters,
                sensor_id=cp_s.id,
                start_date=proc.start_date,
            )
            cp_db.add_record(new_proc_chain)
    cp_db.commit_session()
    cp_db.close_session()
    f.close()
    now = datetime.datetime.now().strftime("%Y.%m.%d-%H:%M:%S")
    return send_file(f.name, as_attachment=True, attachment_filename=f"pattern_export_{now}.db")


@blueprint.route("/download/<device_name>/<format>")
@can_explore_device
def request_task(device_name, format):
    proc = current_app.celery_handle.tasks["app.tasks.async_tasks.prepare_explore_download"]
    session = is_authenticated()
    user_id = session["user_id"]
    kwargs = dict(
        start_date=request.args.get("from"),
        end_date=request.args.get("to"),
        no_filter=request.args.get("nofilter") is not None,
        median=request.args.get("median") is not None,
        filtered_outputs=request.args.getlist("output"),
    )
    task_id = uuid4()
    proc.apply_async(args=(device_name, format), kwargs=kwargs, task_id=f"{user_id}/{task_id}")

    return redirect(current_app.url_for("base_blueprint.get_update_task", task_id=task_id))


@blueprint.route("/request/get_update/<task_id>")
def get_update_task(task_id):
    ccelery = current_app.celery_handle
    session = is_authenticated()
    try:
        user_id = session["user_id"]
    except:
        return abort(403)
    async_result = ccelery.AsyncResult(f"{user_id}/{task_id}")
    if async_result.status == "SUCCESS":
        try:
            filename, content = async_result.result
        except TypeError:
            async_result.forget()
            return abort(404)
        async_result.forget()
        return send_file(
            io.BytesIO(content.encode("utf-8")),
            max_age=60,
            download_name=filename,
            as_attachment=True,
        )

    return_values = {
        "task_id": task_id,
        "status": async_result.status,
        "info": async_result.info if isinstance(async_result.info, dict) else async_result.info.__repr__(),
    }
    return jsonify(return_values), 503


@blueprint.route("/tasks/start_task/<task_name>")
@requires_admin
def start_task(task_name):
    proc = current_app.celery_handle.tasks[task_name]
    proc.apply_async()

    return "", 201
