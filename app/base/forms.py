"""
  Copyright (c) 2020-2021  AIMsight SA
  Written By Olivier Chabloz
"""
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, validators, ValidationError

## login and registration


class LoginForm(FlaskForm):
    username = StringField(
        "Username",
        id="username_login",
        validators=[validators.InputRequired()],
    )
    password = PasswordField(
        "Password",
        id="pwd_login",
        validators=[validators.InputRequired()],
    )
    submit = SubmitField("Save")


class SignUpForm(FlaskForm):
    username = StringField(
        "Username",
        id="username_new",
        validators=[validators.InputRequired()],
    )
    email = StringField(
        "email",
        id="email_new",
        validators=[validators.Email()],
    )
    password = PasswordField(
        "Password",
        id="pwd_new",
        validators=[validators.InputRequired()],
    )
    submit = SubmitField("Save")
