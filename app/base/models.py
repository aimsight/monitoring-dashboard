"""
  Copyright (c) 2020-2021  AIMsight SA
  Written By Olivier Chabloz
"""
from sqlalchemy import (
    Column,
    Integer,
    String,
    Boolean,
    Date,
    Float,
    ForeignKey,
    UniqueConstraint,
    CheckConstraint,
)
from sqlalchemy.sql.sqltypes import DateTime, JSON
from sqlalchemy.types import TIMESTAMP as Timestamp
from sqlalchemy.orm import relationship, validates
from sqlalchemy.orm.collections import attribute_mapped_collection, column_mapped_collection
from werkzeug.security import generate_password_hash, check_password_hash
from sqlalchemy.orm.exc import NoResultFound

from datetime import datetime, timedelta, date
import os
import urllib.parse

from app.app_init import db
from flask import current_app
from logging import getLogger


logger = getLogger()


class Device(db.Model):
    __tablename__ = "Device"

    id = Column(Integer, primary_key=True)
    serial_number = Column(Integer)
    name = Column(String, unique=True)
    show_vdiff = Column(Boolean, default=True)
    show_hdiff = Column(Boolean, default=True)
    patterns = Column(String)
    start_date = Column(DateTime, default=datetime(2023, 1, 1))
    end_date = Column(DateTime, default=None)
    theme = Column(String, default="plotly")
    threshold = Column(Boolean, default=False)
    reference_measurement = Column(Integer)
    description = Column(String(140))
    obstruction_alarm_flags = Column(String)
    min_autoscale = Column(Integer)
    max_autoscale = Column(Integer)

    # user_groups = relationship("UserGroup")
    alarms = relationship("Alarm", lazy="dynamic")

    outputs = relationship(
        "DeviceOutput",
        collection_class=attribute_mapped_collection("position"),
    )

    notifications = relationship("DeviceNotification", lazy="dynamic")

    def __init__(self, name):
        self.name = name

    def add_to_db(self):
        db.session.add(self)
        self.db_commit()

    def new_output(self):
        if len(self.outputs) == 0:
            position = 0
        else:
            position = max(self.outputs.keys()) + 1
        o = DeviceOutput(device_id=self.id, position=position)
        db.session.add(o)
        return o

    def new_notification(self, title, n_date, description, author):
        notification = DeviceNotification(
            device_id=self.id,
            title=title,
            description=description,
            n_date=n_date,
            user_id=author,
        )
        db.session.add(notification)
        self.db_commit()
        return notification

    def user_access(self, user_id):
        sub = db.session.query(UserGroup.id).filter_by(device_id=self.id).scalar_subquery()
        q = UserGroupEntry.query.filter_by(user_id=user_id).filter(UserGroupEntry.group_id.in_(sub))
        return q.count() > 0

    def new_alarm(self, **kwargs):
        parent_id = kwargs.pop("parent_id", None)
        new_alarm = Alarm(device_id=self.id, parent_id=parent_id)
        db.session.add(new_alarm)
        self.db_commit()
        new_alarm.update(**kwargs)
        return new_alarm

    def get_alarms(self, active_only=True):
        alarms = self.alarms
        if active_only:
            alarms = alarms.filter_by(active=True)
        return alarms

    def get_parents_or_single_alarms(self):
        return self.alarms.filter_by(parent_id=None)

    def get_notifications(self, from_date=None, until_date=None):
        notifs = self.notifications
        if from_date is not None:
            notifs = notifs.filter(DeviceNotification.n_date >= from_date)
        if until_date is not None:
            until = datetime.strptime(until_date, "%Y-%m-%d") + timedelta(days=1)
            notifs = notifs.filter(DeviceNotification.n_date <= until)
        notifications = notifs.order_by(DeviceNotification.n_date.desc())
        return [n.to_dict() for n in notifications]

    @property
    def is_internal(self):
        return len(self.access) == 0

    @property
    def is_archive(self):
        return self.end_date is not None and self.end_date <= datetime.now()

    @staticmethod
    def get(device_name):
        return Device.query.filter_by(name=device_name).one()

    @staticmethod
    def get_by_id(device_id):
        return Device.query.filter_by(id=device_id).one()

    @staticmethod
    def get_all_devices(no_archive=False):
        dev = Device.query.order_by(Device.name)
        if no_archive:
            dev = dev.filter((Device.end_date == None) | (Device.end_date > datetime.now()))
        return dev.all()

    @staticmethod
    def get_for_user(user_id, access_type="read"):
        if not user_id is None:
            subq = (
                db.session.query(UserGroup.device_id)
                .filter(UserGroupEntry.user_id == user_id)
                .join(UserGroupEntry)
            )
            return Device.query.filter(Device.id.in_(subq)).all()
        return []

    # sub = db.session.query(UserGroup.id).filter_by(device_id=self.id).scalar_subquery()
    # q = UserGroupEntry.query.filter_by(user_id=user_id).filter(UserGroupEntry.group_id.is_(sub))

    @staticmethod
    def get_from_pathname(p):
        """
        Retrieve the device name from the given path
        p: str, the complete URL
        returns app.base.models.Device()
        """
        dev_name = urllib.parse.unquote(p).split("/")[-1]
        dev = Device.get(dev_name)
        return dev

    def db_commit(self):
        db.session.commit()

    def rollback(self):
        db.session.rollback()

    def delete(self):
        self.query.filter_by(id=self.id).delete()
        db.session.commit()


class DeviceOutput(db.Model):
    __tablename__ = "DeviceOutput"

    id = Column(Integer, primary_key=True)
    device_id = Column(Integer, ForeignKey("Device.id"))
    position = Column(Integer)
    scale = Column(Float, default=1)
    sensor1 = Column(Integer)
    sensor2 = Column(Integer)
    process_type = Column(String)
    interp = Column(String)
    roll_med = Column(Integer, default=1)
    vertical_offset = Column(Float, default=0)
    horizontal_offset = Column(Float, default=0)
    Valarm_threshold = Column(Float)
    Halarm_threshold = Column(Float)
    Valarm_triggered = Column(Boolean, default=False)
    Halarm_triggered = Column(Boolean, default=False)
    name = Column(String)
    __table_args__ = (UniqueConstraint("device_id", "position"),)

    device = relationship("Device", uselist=False, back_populates="outputs")

    def as_dict(self):
        return {
            "scale": self.scale,
            "vertical_offset": self.vertical_offset,
            "horizontal_offset": self.horizontal_offset,
            "sensor1": self.sensor1,
            "sensor2": self.sensor2,
            "process_type": self.process_type,
            "interp": self.interp,
            "roll_med": self.roll_med,
            "name": self.name,
            "Halarm_threshold": self.Halarm_threshold,
            "Valarm_threshold": self.Valarm_threshold,
            "Valarm_triggered": self.Valarm_triggered,
            "Halarm_triggered": self.Halarm_triggered,
            "device_output_id": self.id,
        }

    def from_dict(self, d):
        self.scale = d["scale"]
        self.sensor1 = d["sensor1"]
        self.sensor2 = d["sensor2"]
        self.process_type = d["process_type"]
        self.interp = d["interp"]
        self.roll_med = d["roll_med"]
        self.name = d["name"]
        self.horizontal_offset = d["horizontal_offset"]
        self.vertical_offset = d["vertical_offset"]
        self.Valarm_threshold = d["Valarm_threshold"]
        self.Halarm_threshold = d["Halarm_threshold"]

    def delete(self):
        self.query.filter_by(id=self.id).delete()

    def db_commit(self):
        db.session.commit()

    @validates("Valarm_threshold", "Halarm_threshold")
    def validate_alarm_threshold(self, key, value):
        try:
            value = float(value)
        except (ValueError, TypeError):
            value = None
        return value


class DeviceNotification(db.Model):
    __tablename__ = "DeviceNotification"

    id = Column(Integer, primary_key=True)
    device_id = Column(Integer, ForeignKey("Device.id"))
    title = Column(String)
    n_date = Column(Timestamp)
    description = Column(String)
    user_id = Column(String)

    def to_dict(self):
        return {
            "notification_id": self.id,
            "title": self.title,
            "date": self.n_date,
            "description": self.description,
            "user": self.user_id,
        }

    def db_commit(self):
        db.session.commit()

    @classmethod
    def delete(cls, notification_id):
        db.session.query(cls).filter_by(id=notification_id).delete()
        db.session.commit()


class UserNotification(db.Model):
    __tablename__ = "UserNotification"
    UserNotification_id = Column(Integer, primary_key=True)
    read = Column(Boolean, default=False)
    title = Column(String)
    text = Column(String)
    author = Column(String)
    link = Column(String)
    time_utc = Column(Timestamp, default=datetime.now)
    user_id = Column(String)

    @classmethod
    def get_for_user(cls, user_id, unread_only=False, order=True):
        q = db.session.query(cls).filter_by(user_id=user_id)
        if unread_only:
            q = q.filter_by(read=False)
        if order:
            q = q.order_by(cls.time_utc.desc())
        return q

    @classmethod
    def get(cls, notification_id):
        q = db.session.query(cls).filter_by(UserNotification_id=notification_id).one()
        return q

    def set_read(self):
        self.read = True

    @classmethod
    def db_commit(cls):
        db.session.commit()

    @classmethod
    def new(cls, title, text, user_id, author, link):
        n = cls(title=title, text=text, user_id=user_id, author=author, link=link)
        db.session.add(n)
        return n

    @classmethod
    def clear_all(cls, user_id):
        cls.get_for_user(user_id, order=False).update({cls.read: True})
        db.session.commit()


class UserVisit(db.Model):
    __tablename__ = "UserVisit"

    id = Column(Integer, primary_key=True)
    user_id = Column(String)
    time_utc = Column(Timestamp, default=datetime.now)
    page_name = Column(String)
    page_url = Column(String)

    @classmethod
    def new(cls, page_name, page_url, user_id):
        n = cls(page_name=page_name, page_url=page_url, user_id=user_id)
        db.session.add(n)
        db.session.commit()
        return n


class User(db.Model):
    __tablename__ = "User"

    id = Column(Integer, primary_key=True)
    user_id = Column(String, unique=True)  # for backends purpose
    admin = Column(Boolean, default=False)
    password = Column(String)
    login_backend = Column(String)
    attributes = Column(JSON)
    is_authenticated = Column(Boolean, default=False)
    # notifications = relationship("UserNotification")

    def __init__(self, *args, **kwargs):
        for props, value in kwargs.items():
            if props == "password":
                value = generate_password_hash(value, method="sha256")
            setattr(self, props, value)

    def __repr__(self):
        return str(self.username)

    @classmethod
    def get(cls, email):
        q = db.session.query(cls)
        for user in q:
            if user.attributes.get("email") == email:
                return user

    @classmethod
    def get_byID(cls, user_id):
        q = db.session.query(cls).filter_by(user_id=user_id)
        return q.one_or_none()

    def get_id(self):
        return self.user_id

    def get_attributes(self):
        att = self.attributes
        att["user_id"] = self.user_id
        return att

    def check_login(self, password):
        return check_password_hash(self.password, password)

    def is_active(self):
        return True

    def add_to_db(self):
        db.session.add(self)
        self.db_commit()

    def db_commit(self):
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        self.db_commit()

    def push_notification(self, title, text):
        n = UserNotification(title=title, text=text, user_id=self.user_id)
        db.session.add(n)
        db.session.commit()


class Alarm(db.Model):
    __tablename__ = "Alarm"
    id = Column(Integer, primary_key=True)
    device_id = Column(Integer, ForeignKey("Device.id"))
    # alter table Alarm add column "parent_id" INTEGER REFERENCES Alarm (id) CHECK("parent_id" != "id");
    parent_id = Column(Integer, ForeignKey("Alarm.id"))
    _signal_type = Column("signal_type", String)  # device_output ou raw signal
    _signal_value_type = Column("signal_value_type", String)  # horizontal, vertical, error_value, etc.
    _sensor_id = Column("sensor_id", Integer)  # deviceOutput_id or sensor_id
    _label = Column("label", String)
    creation_date = Column(Timestamp, default=datetime.now)
    _min_range = Column("min_range", Float)
    _max_range = Column("max_range", Float)
    # consecutive events outside range / a percentage of events outside range
    _alarm_logic = Column("alarm_logic", String)
    _logic_value = Column("logic_value", Float)  # number of consecutive events / percentage of events
    _query_period = Column("query_period", Float)
    # temps de rearmement. Après X heures, l'alarme repasse en mode non triggé, et sera donc réévalué.
    _rearming_period = Column("rearming_period", Float)
    _group_id = Column("group_id", Integer, ForeignKey("UserGroup.id"))
    active = Column(Boolean, default=True)
    _template = Column("template", String, default="default")  # notification template

    constraint1 = CheckConstraint("parent_id != id")

    device = relationship("Device", uselist=False, back_populates="alarms")
    alarm_events = relationship("AlarmEvent", lazy="dynamic")
    _group = relationship("UserGroup", uselist=False)
    parent = relationship("Alarm", uselist=False, remote_side=[id], backref="children")

    def get_active_children(self):
        return Alarm.query.filter_by(parent_id=self.id, active=True)

    @property
    def group(self):
        return self._group or getattr(self.parent, "_group", None)

    @property
    def signal_type(self):
        return self._signal_type or getattr(self.parent, "_signal_type", None)

    @property
    def signal_value_type(self):
        return self._signal_value_type or getattr(self.parent, "_signal_value_type", None)

    @property
    def sensor_id(self):
        return self._sensor_id or getattr(self.parent, "_sensor_id", None)

    @property
    def label(self):
        return self._label or (
            f'{getattr(self.parent, "_label", None)} {self.resolve_sensor_id()} {self.signal_value_type}'
        )

    @property
    def min_range(self):
        if self._min_range is None:
            return getattr(self.parent, "_min_range", None)
        return self._min_range

    @property
    def max_range(self):
        if self._max_range is None:
            return getattr(self.parent, "_max_range", None)
        return self._max_range

    @property
    def alarm_logic(self):
        return self._alarm_logic or getattr(self.parent, "_alarm_logic", None)

    @property
    def logic_value(self):
        return self._logic_value or getattr(self.parent, "_logic_value", None)

    @property
    def query_period(self):
        return self._query_period or getattr(self.parent, "_query_period", None)

    @property
    def rearming_period(self):
        return self._rearming_period or getattr(self.parent, "_rearming_period", None)

    @property
    def group_id(self):
        return self._group_id or getattr(self.parent, "_group_id", None)

    @property
    def template(self):
        return self._template or getattr(self.parent, "_template", None)

    def update(self, **kwargs):
        """
        update row from keyed arguments
        """
        for key, value in kwargs.items():
            if not hasattr(self, key):
                logger.warning("Attr %s unknown in Object Alarm")
                continue
            if getattr(self, key) != value:
                # because some (but not all) attributes are prefixed by '_'
                # we check first if the attribute we're looking for is indeed prefixed
                if hasattr(self, f"_{key}"):
                    setattr(self, f"_{key}", value)
                else:
                    setattr(self, key, value)

        self.db_commit()

    def resolve_sensor_id(self):
        if self.signal_type == "device_output":
            try:
                return DeviceOutput.query.filter_by(id=self.sensor_id).one().name
            except NoResultFound:
                return "Unknown"
        if self.signal_type == "sensor":
            from app.extensions import AIMsightDB

            try:
                return AIMsightDB.get_sensor(self.sensor_id).name
            except:
                return self.sensor_id

    def new_event(self, event_date, description, value, status="active", unset_active=True):
        if unset_active:
            self.active = False
        event = AlarmEvent(
            alarm_id=self.id, event_date=event_date, description=description, value=value, status=status
        )
        db.session.add(event)
        self.db_commit()
        return event

    def must_be_evaluated(self) -> bool:
        return self.active and self.sensor_id is not None and self.signal_value_type is not None

    def get_status(self):
        if self.active:
            return "active"

        last_event = self.get_last_event()
        if last_event is None:
            return "inactive"
        elif last_event.status == "active":
            return "triggered"

        if self.rearming_period in [None, -1]:
            return "inactive"

        return "unknown"

    def get_last_event(self):
        return self.alarm_events.order_by(AlarmEvent.creation_date.desc()).first()

    def delete(self):
        AlarmEvent.query.filter_by(alarm_id=self.id).delete()
        self.query.filter_by(id=self.id).delete()
        self.db_commit()

    @classmethod
    def db_commit(cls):
        db.session.commit()


class AlarmEvent(db.Model):
    __tablename__ = "AlarmEvent"
    id = Column(Integer, primary_key=True)
    alarm_id = Column(Integer, ForeignKey("Alarm.id"))
    creation_date = Column(Timestamp, default=datetime.now)  # date of the row creation
    event_date = Column(Timestamp, default=datetime.now)  # date when event occured
    description = Column(String)
    value = Column(Float)
    status = Column(String)

    alarm = relationship("Alarm", uselist=False, back_populates="alarm_events")

    def db_commit(self):
        db.session.commit()


class UserGroup(db.Model):
    __tablename__ = "UserGroup"
    id = Column(Integer, primary_key=True)
    device_id = Column(Integer, ForeignKey("Device.id"))
    creation_date = Column(Timestamp, default=datetime.now)
    name = Column(String)
    description = Column(String)
    extra_emails = Column(String)  # comma separated
    extra_phones = Column(String)  # comma separated
    # alter table UserGroup add column "sms_notifications" BOOLEAN DEFAULT 0 CHECK("sms_notifications" IN (0, 1));
    sms_notifications = Column(Boolean)

    device = relationship("Device", uselist=False, backref="user_groups")

    @classmethod
    def get(cls, group_id):
        q = db.session.query(cls).filter_by(id=group_id)
        return q.one()

    @classmethod
    def new(cls, device_id):
        new_item = cls(device_id=device_id)
        db.session.add(new_item)
        db.session.commit()
        return new_item

    def add_user(self, user_id, commit=True):
        n = UserGroupEntry(group_id=self.id, user_id=user_id)
        db.session.add(n)
        if commit:
            db.session.commit()
        return n

    def delete(self):
        for i in self.users:
            self.del_user(i.user_id, commit=False)
        db.session.query(UserGroup).filter_by(id=self.id).delete()
        db.session.commit()

    def del_user(self, user_id, commit=True):
        q = db.session.query(UserGroupEntry).filter_by(group_id=self.id, user_id=user_id)
        q.delete()
        if commit:
            db.session.commit()

    def update_users(self, user_list):
        actual_users = [x.user_id for x in self.users]
        for i in user_list:
            if not i in actual_users:
                self.add_user(i, commit=False)
        for i in actual_users:
            if not i in user_list:
                self.del_user(i, commit=False)
        db.session.commit()

    def update_values(
        self, name=None, desc=None, extra_emails=None, extra_phones=None, users=None, sms_notifications=None
    ):
        self.name = name
        self.description = desc
        self.extra_emails = extra_emails
        self.extra_phones = extra_phones
        self.sms_notifications = sms_notifications
        if users is not None:
            self.update_users(users)
        db.session.commit()


class UserGroupEntry(db.Model):
    __tablename__ = "UserGroupEntry"
    id = Column(Integer, primary_key=True)
    group_id = Column(Integer, ForeignKey("UserGroup.id"))
    user_id = Column(String)
    __table_args__ = (UniqueConstraint("group_id", "user_id"),)

    group = relationship("UserGroup", backref="users")


class ExternalAPICost(db.Model):
    __tablename__ = "ExternalAPICost"
    id = Column(Integer, primary_key=True)
    creation_date = Column(Timestamp, default=datetime.now)  # date of the row creation
    full_response = Column(String)
    price = Column(Float)
    price_unit = Column(String)

    @classmethod
    def new(cls, full_response, price, price_unit):
        new_item = cls(full_response=full_response, price=price, price_unit=price_unit)
        db.session.add(new_item)
        db.session.commit()
        return new_item
