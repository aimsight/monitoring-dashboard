from sqlalchemy.sql.functions import user
from .models import UserNotification, Device
from logging import getLogger
from datetime import datetime, timedelta

logger = getLogger()


def send_notification_to_user(user_id, title, text, link):
    UserNotification.new(
        title=title,
        text=text,
        user_id=user_id,
        author="System",
        link=link,
    )

    UserNotification.db_commit()
    logger.info("Created notification for user %s", user_id)


def send_notification_to_subscriber(dev, access_type, text):
    has_to_commit = False
    accesses = {"Threshold Alarm": dev.threshold_alarm, "Obstruction Alarm": dev.obstruction_alarm}
    if access_type in accesses:
        for user_id in accesses[access_type]:
            UserNotification.new(
                title=access_type,
                text=text,
                user_id=user_id,
                author="System",
                link=f"/explore/{dev.name}",
            )
            has_to_commit = True
            logger.info("Created notification for user %s", user_id)
    if has_to_commit:
        UserNotification.db_commit()


def format_datetime(dt):
    delta = datetime.now() - dt
    if delta.days == 0:
        if delta.seconds < 3600:
            return f"{delta.seconds // 60} minutes ago"
        return f"{delta.seconds // 3600} hours ago"
    return dt.strftime("%d.%m.%Y")


def get_notifications(user_id, unread_only=False):
    notification_list = []
    for notif in UserNotification.get_for_user(user_id, unread_only=unread_only):
        n = dict(
            UserNotification_id=notif.UserNotification_id,
            title=notif.title,
            author=notif.author,
            description=notif.text,
            link=notif.link,
            time=format_datetime(notif.time_utc),
        )
        notification_list.append(n)

    return notification_list
