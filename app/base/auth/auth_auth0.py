from flask import session, request, redirect, url_for, _request_ctx_stack
from authlib.integrations.flask_client import OAuth
from authlib.integrations.base_client import errors
from six.moves.urllib.parse import urlencode, urljoin
from six.moves.urllib.request import urlopen
from datetime import datetime, timedelta
from logging import getLogger
import requests
from jose import jwt
import json, time
from collections import defaultdict


logger = getLogger()

AUTH0_config = {}

auth_instance = {}
tokens = {}
roles = {}


def get_auth():
    return auth_instance.get("auth0", None)


def login():
    return get_auth().authorize_redirect(
        redirect_uri=urljoin(request.base_url, AUTH0_config["AUTH0_CALLBACK_URL"]),
        audience=AUTH0_config["AUTH0_AUDIENCE"],
    )


def signup():
    logger.critical("Sign-up not implemented")


def logout():
    session.clear()
    params = {
        "returnTo": url_for("home_blueprint.index", _external=True),
        "client_id": AUTH0_config["AUTH0_CLIENT_ID"],
    }
    return redirect(get_auth().api_base_url + "/v2/logout?" + urlencode(params))


def is_authenticated():
    user_session = session.get("profile", None)
    if user_session is None:
        user_session = authenticate_with_token()
    return user_session


def is_admin():
    user_session = is_authenticated()
    is_admin = False
    is_logged = user_session is not None
    user_session = user_session or {}
    if "scope" in user_session:
        scope = user_session["scope"].split()
        if "reads:explore" in scope and user_session["exp"] > time.time():
            return True

    if is_logged:
        roles = get_roles(user_session["user_id"])
        is_admin = "admin" in roles
    return is_admin


def register_auth(server, config):
    oauth = OAuth(server)
    BASE_URL = "https://" + config.AUTH0_DOMAIN
    AUTH0_config["BASE_URL"] = BASE_URL + "/"
    AUTH0_config["AUTH0_DOMAIN"] = config.AUTH0_DOMAIN
    AUTH0_config["AUTH0_CALLBACK_URL"] = config.AUTH0_CALLBACK_URL
    AUTH0_config["AUTH0_AUDIENCE"] = config.AUTH0_AUDIENCE
    AUTH0_config["AUTH0_CLIENT_ID"] = config.AUTH0_CLIENT_ID
    AUTH0_config["AUTH0_API_CLIENTID"] = config.AUTH0_API_CLIENTID
    AUTH0_config["AUTH0_API_CLIENT_SECRET"] = config.AUTH0_API_CLIENT_SECRET
    AUTH0_config["AUTH0_API_AUDIENCE"] = config.AUTH0_API_AUDIENCE

    AUTH0_config["debug"] = config.DEBUG
    auth0 = oauth.register(
        "auth0",
        client_id=config.AUTH0_CLIENT_ID,
        client_secret=config.AUTH0_CLIENT_SECRET,
        api_base_url=BASE_URL,
        access_token_url=BASE_URL + "/oauth/token",
        authorize_url=BASE_URL + "/authorize",
        client_kwargs={
            "scope": "openid profile email",
        },
    )
    auth_instance["auth0"] = auth0


def callback():
    try:
        get_auth().authorize_access_token()
    except (errors.MismatchingStateError, errors.OAuthError):
        redirect(url_for("/page_403"))
    resp = get_auth().get("userinfo")
    userinfo = resp.json()

    session["jwt_payload"] = userinfo
    session["profile"] = {
        "email": userinfo["email"],
        "email_verified": userinfo["email_verified"],
        "user_id": userinfo["sub"],
        "name": userinfo["name"],
        "picture": userinfo["picture"],
    }
    logger.info("user %s (%s) has logged in", userinfo["sub"], userinfo["email"])
    return redirect(url_for("home_blueprint.index"))


def get_access_token(renew_token=False):
    payload = {
        "client_id": AUTH0_config["AUTH0_API_CLIENTID"],
        "client_secret": AUTH0_config["AUTH0_API_CLIENT_SECRET"],
        "audience": AUTH0_config["AUTH0_API_AUDIENCE"] + "/",
        "grant_type": "client_credentials",
    }
    if "auth0" in tokens and tokens["auth0"]["expire_date"] > datetime.now() and not renew_token:
        return tokens["auth0"]["access_token"]
    r = requests.post("https://aimsight.eu.auth0.com/oauth/token", json=payload)
    if r.status_code == 200:
        tokens["auth0"] = r.json()
        expires_sec = tokens["auth0"]["expires_in"] // 2
        tokens["auth0"]["expire_date"] = datetime.now() + timedelta(seconds=expires_sec)
        logger.info("Got Backend token. Set for renewal on %s", tokens["auth0"]["expire_date"])
        return tokens["auth0"]["access_token"]
    logger.info("Failed to get access token (%d), reason: %s", r.status_code, r.content.decode("utf-8"))


def get_roles(user_id):
    if not user_id in roles:
        resp = get_request(f"{AUTH0_config['AUTH0_API_AUDIENCE']}/users/{user_id}/roles")
        user_roles = resp.json()
        roles[user_id] = {r["name"]: r["description"] for r in user_roles}

    return roles[user_id]


def get_request(url, renew_token=False):
    access_token = get_access_token(renew_token)
    r = requests.get(url, headers={"authorization": "Bearer {}".format(access_token)}, timeout=5)
    if r.status_code != 200 and not renew_token:  # avoid recursive void
        return get_request(url, renew_token=True)
    return r


def delete_request(url, renew_token=False):
    access_token = get_access_token(renew_token)
    r = requests.delete(url, headers={"authorization": "Bearer {}".format(access_token)}, timeout=5)
    if r.status_code != 200 and not renew_token:  # avoid recursive void
        return delete_request(url, renew_token=True)
    return r


def post_request(url, payload: bytes, renew_token=False, method="post"):
    access_token = get_access_token(renew_token)
    headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "authorization": f"Bearer {access_token}",
    }
    if method == "post":
        r = requests.post(url, payload, headers=headers, timeout=5)
    elif method == "patch":
        r = requests.patch(url, payload, headers=headers, timeout=5)
    else:
        raise NotImplementedError(f"method {method} not implemented")
    if r.status_code == 401 and not renew_token:  # avoid recursive void
        return post_request(url, payload, renew_token=True, method=method)
    return r


def get_users():
    return get_request(f"{AUTH0_config['AUTH0_API_AUDIENCE']}/users").json()


def get_user_info(user_id):
    return get_request(f"{AUTH0_config['AUTH0_API_AUDIENCE']}/users/{user_id}").json()


def update_user_info(user_id, name=None, email=None, phone=None, allow_sms=None, password=None):
    update_data = defaultdict(dict)
    if name is not None:
        update_data["name"] = name
    if email is not None:
        update_data["email"] = email
    if password is not None and len(password) > 0:
        update_data["password"] = password
    if phone is not None:
        update_data["user_metadata"]["phone_number"] = phone
    if allow_sms is not None:
        update_data["user_metadata"]["allow_sms"] = allow_sms
    payload = json.dumps(update_data)
    r = post_request(f"{AUTH0_config['AUTH0_API_AUDIENCE']}/users/{user_id}", payload, method="patch")

    return r.status_code == 200


def add_user(name, email, password, phone=None, allow_sms=None):
    con = "Username-Password-Authentication"
    payload = json.dumps(
        {
            "name": name,
            "email": email,
            "password": password,
            "verify_email": True,
            "connection": con,
            "user_metadata": {"phone_number": phone, "allow_sms": allow_sms},
        }
    )
    r = post_request(f"{AUTH0_config['AUTH0_API_AUDIENCE']}/users", payload)
    return r.status_code == 201


def delete_user(user_id):
    r = delete_request(f"{AUTH0_config['AUTH0_API_AUDIENCE']}/users/{user_id}")
    return r.status_code == 204


# API


# Error handler
class AuthError(Exception):
    def __init__(self, error, status_code):
        self.error = error
        self.status_code = status_code


# Format error response and append status code
def get_token_auth_header():
    """Obtains the Access Token from the Authorization Header"""
    auth = request.headers.get("Authorization", None)
    if not auth:
        return
        raise AuthError(
            {
                "code": "authorization_header_missing",
                "description": "Authorization header is expected",
            },
            401,
        )

    parts = auth.split()

    if parts[0].lower() != "bearer":
        raise AuthError(
            {
                "code": "invalid_header",
                "description": "Authorization header must start with" " Bearer",
            },
            401,
        )
    elif len(parts) == 1:
        raise AuthError({"code": "invalid_header", "description": "Token not found"}, 401)
    elif len(parts) > 2:
        raise AuthError(
            {
                "code": "invalid_header",
                "description": "Authorization header must be" " Bearer token",
            },
            401,
        )

    token = parts[1]
    return token


def authenticate_with_token():
    token = get_token_auth_header()
    if token is None:
        return
    jsonurl = urlopen("https://" + AUTH0_config["AUTH0_DOMAIN"] + "/.well-known/jwks.json")
    jwks = json.loads(jsonurl.read())
    unverified_header = jwt.get_unverified_header(token)
    rsa_key = {}
    for key in jwks["keys"]:
        if key["kid"] == unverified_header["kid"]:
            rsa_key = {
                "kty": key["kty"],
                "kid": key["kid"],
                "use": key["use"],
                "n": key["n"],
                "e": key["e"],
            }
    if rsa_key:
        try:
            payload = jwt.decode(
                token,
                rsa_key,
                algorithms=["RS256"],
                audience="https://monitoring.aimsight.ch/",
                issuer=AUTH0_config["BASE_URL"],
            )
        except jwt.ExpiredSignatureError:
            raise AuthError({"code": "token_expired", "description": "token is expired"}, 401)
        except jwt.JWTClaimsError:
            raise AuthError(
                {
                    "code": "invalid_claims",
                    "description": "incorrect claims," "please check the audience and issuer",
                },
                401,
            )
        except Exception:
            raise AuthError(
                {
                    "code": "invalid_header",
                    "description": "Unable to parse authentication" " token.",
                },
                401,
            )
        payload["user_id"] = payload.get("azp")
        _request_ctx_stack.top.current_user = payload
        return payload
    raise AuthError({"code": "invalid_header", "description": "Unable to find appropriate key"}, 401)
