from ..forms import LoginForm, SignUpForm
from flask import redirect, render_template, url_for, request, session
from flask_login import login_user, LoginManager, logout_user, login_required
from uuid import uuid4
from ..models import User


def login():
    login_form = LoginForm(request.form)
    if login_form.validate_on_submit():
        u = User.get(login_form.username.data)
        if u is not None and u.check_login(login_form.password.data):
            login_user(user=u, remember=True)
            session["profile"] = u.get_attributes()
            return redirect(url_for("home_blueprint.index"))
    login_form.password = None
    return render_template("login/login.html", login_form=login_form)


def is_authenticated():
    return session.get("profile", None)


def is_admin():
    s = is_authenticated()
    if s is None:
        return False
    u = User.get_byID(s["user_id"])
    return u.admin


def get_users():
    all = User.query.all()
    return [u.get_attributes() for u in all]


def get_user_info(user_id):
    u = User.get_byID(user_id)
    return u.get_attributes()


def logout():
    logout_user()
    session.clear()
    return redirect(url_for("home_blueprint.index"))


def callback():
    return


def register_auth(server, config):
    login_manager = LoginManager()
    login_manager.login_view = "base_blueprint.login"
    login_manager.init_app(server)

    @login_manager.user_loader
    def load_user(user_id):
        # since the user_id is just the primary key of our user table, use it in the query for the user
        return User.get_byID(user_id)


def signup():
    sup = SignUpForm(request.form)


def add_user(name, email, password, is_admin=False):
    user_id = f"flask|{uuid4()}"
    u = User(
        user_id=user_id,
        password=password,
        admin=is_admin,
        login_backend="FLASK",
        attributes={"name": name, "email": email},
    )
    u.add_to_db()
