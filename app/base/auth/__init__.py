from dotenv import load_dotenv, find_dotenv
from os import environ as env


ENV_FILE = find_dotenv()
if ENV_FILE:
    load_dotenv(ENV_FILE)


AUTH_MODE = env.get("AUTH_MODE")

if AUTH_MODE == "AUTH0":
    from .auth_auth0 import *
elif AUTH_MODE == "FLASK":
    from .auth_flask import *

from .common import *
from . import routes  # load them as a way to register them to the blueprint
