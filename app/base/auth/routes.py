from .. import blueprint
from . import login as login_specific, logout as logout_specific, callback as callback_specific
from . import signup as signUP_specific


@blueprint.route("/login", methods=["GET", "POST"])
def login():
    return login_specific()


@blueprint.route("/signup", methods=["GET", "POST"])
def signUP():
    return signUP_specific()


@blueprint.route("/logout")
def logout():
    return logout_specific()


@blueprint.route("/callback")
def callback_handling():
    return callback_specific()