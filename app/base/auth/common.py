from functools import wraps
from flask import redirect, session, abort
from logging import getLogger
import urllib.parse
from . import is_authenticated, is_admin
from ..models import Device, UserVisit

_logger = getLogger()
global_config = {"debug": False}


def report_user(page_name, page_url=None):
    """log the page a user is browsing"""
    s = is_authenticated()
    if s is None:
        _logger.info("unlogged user is browsing '%s'", page_name)
    else:
        _logger.info("User %s (%s) is browsing '%s'", s.get("user_id", None), s.get("email", None), page_name)
        UserVisit.new(page_name, page_url, s.get("user_id", None))


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if "profile" not in session:
            return redirect("/login")
        return f(*args, **kwargs)

    return decorated


def can_explore_device(f):
    """
    Function to use as a decorator. Protect the access of an Endpoint by checking if the logged
    user can access the device
    """

    @wraps(f)
    def decorated(*args, **kwargs):
        """Looks for device_name in either args[0] or kwargs["device_name"]. abort on no rights"""
        default = args[0] if len(args) > 0 else ""
        if default is not None:
            default = urllib.parse.unquote(default).split("/")[-1]
        device_name = kwargs.get("device_name", default)
        try:
            can_access = can_access_device(device_name, raise_not_logged=True)
        except Exception:
            _logger.info("can_explore_device: not found %s", device_name)
            return abort(404)
        else:
            if not can_access:
                return abort(403)
        return f(*args, **kwargs)

    return decorated


def can_access_device(device_name, raise_not_logged=False):
    """
    Check if the current logged user can access a Device.
    If optional raise_not_logged is True: raise Exception if no user is logged
    Returns bool
    """
    profile = is_authenticated()
    if profile is None:
        if raise_not_logged:
            raise Exception("Not logged")
        return False
    dev = Device.get(device_name)  #! raise error when not found
    return dev.user_access(profile.get("user_id")) or is_admin()


def requires_admin(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        return f(*args, **kwargs) if is_admin() else redirect("/page_403")

    return decorated
