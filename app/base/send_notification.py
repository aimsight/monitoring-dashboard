import os, json
from logging import getLogger
import requests
from html.parser import HTMLParser
from flask_mail import Message, Mail
from flask import current_app
from .notifications import send_notification_to_user
from .models import ExternalAPICost

logger = getLogger()


REALLY_SEND_EMAIL = json.loads(os.environ.get("REALLY_SEND_EMAIL", "true"))
if REALLY_SEND_EMAIL:
    logger.warning("EMAILS will be sent.")


def send_email(recipients, template_name, template_context):
    """
    recipients: list of dict(email, name)
    """
    mail = Mail(current_app)
    template = current_app.jinja_env.get_template(template_name)
    with mail.connect() as conn:
        for recipient in recipients:
            render = template.render(**recipient, **template_context)
            subject = get_head_title(render)
            msg = Message(subject=subject, html=render, recipients=[recipient["email"]])
            if not REALLY_SEND_EMAIL:
                logger.info("Email NOT REALLY sent to %s => %s", recipient["email"], subject)
                continue

            conn.send(msg)
            logger.info("Email sent to %s => %s", recipient["email"], subject)


def send_sms(recipients, template_name, template_context):
    username = current_app.config.get("TWILIO_USER")
    password = current_app.config.get("TWILIO_PASSWD")
    phone_from = current_app.config.get("TWILIO_PHONE")
    template = current_app.jinja_env.get_template(template_name)
    url = f"https://api.twilio.com/2010-04-01/Accounts/{username}/Messages.json"

    for recipient in recipients:
        render = template.render(**recipient, **template_context)
        to = recipient["phone_number"]
        if not REALLY_SEND_EMAIL:
            logger.info("SMS NOT REALLY sent to %s => %s", to, render)
            continue
        try:
            response = requests.post(
                url, auth=(username, password), data={"To": to, "From": phone_from, "Body": render}, timeout=5
            )
        except TimeoutError:
            logger.critical("TWILIO API timeout")
        if response.status_code == 201:
            payload = response.json()
            ExternalAPICost.new(response.content, payload["price"], payload["price_unit"])
            logger.info("SMS sent to %s", to)
        else:
            logger.warning("TWILIO response Error: %d", response.status_code)
            logger.warning("TWILIO response: %s", response.content)


def send_notification(recipients, template_name, template_context):
    template = current_app.jinja_env.get_template(template_name)
    for recipient in recipients:
        render = template.render(**recipient, **template_context)
        title = get_head_title(render)
        send_notification_to_user(recipient["user_id"], title, render, template_context["link"])


class TitleParser(HTMLParser):
    def __init__(self):
        super().__init__()
        self.inside_title = False
        self.title = ""

    def handle_starttag(self, tag, attrs):
        if tag == "title":
            self.inside_title = True

    def handle_data(self, data):
        if self.inside_title:
            self.title += data

    def handle_endtag(self, tag):
        if tag == "title":
            self.inside_title = False


def get_head_title(html_text):
    try:
        parser = TitleParser()
        parser.feed(html_text)

        return parser.title.strip()

    except urllib.error.URLError as e:
        return ""
