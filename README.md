# Flask Gentelella

[Gentelella](https://github.com/puikinsh/gentelella) is a free to use Bootstrap admin template.

![Gentelella Bootstrap Admin Template](https://cdn.colorlib.com/wp/wp-content/uploads/sites/2/gentelella-admin-template-preview.jpg "Gentelella Theme Browser Preview")

This project integrates Gentelella with Flask using:

- [Blueprints](http://flask.pocoo.org/docs/0.12/blueprints/) for scalability.
- [flask_migrate](https://flask-migrate.readthedocs.io/en/latest/).
- [dash](https://dash.plot.ly/).

Here is an example of a real project implemented using Flask-Gentelella:

- [Online demo](http://afourmy.pythonanywhere.com/)
- [Source code](https://github.com/afourmy/eNMS)

## Install requirements

    pipenv sync
    or
    pip install -r requirements.txt

### Run the application

    sudo docker build -t aimsight_monitoring:1.0 monitoring/

    sudo docker run --env-file monitoring/prod_env -v /home/olivierchabloz/dev/monitoring_config:/configuration -d -p 40092:40092 aimsight_monitoring:1.0

## video demo

https://github.com/plotly/dash-object-detection
