FROM ubuntu:20.04

RUN apt-get update -y && \
    apt-get install -y python3-pip python3 python3-wheel

RUN mkdir /configuration
COPY ./ /app/

WORKDIR /app

RUN pip3 install -r requirements.txt
RUN pip3 install dependencies/*

ENTRYPOINT [ "bash" ]

CMD [ "run_web.sh" ]