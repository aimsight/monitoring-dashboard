#!/bin/bash

if [ "$MONITORING_APP" == "app" ]
then
    export FLASK_APP=run:create_app
    gunicorn run:app --config=./configs/gunicorn.py
fi;

if [ "$MONITORING_APP" == "worker" ]
then
    celery -A run.celery_app worker --loglevel=INFO
fi;

if [ "$MONITORING_APP" == "beat" ]
then
    celery -A run.celery_app beat --loglevel=INFO
fi;