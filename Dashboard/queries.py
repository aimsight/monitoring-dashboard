"""
  Copyright (c) 2020-2021  AIMsight SA
  Written By Olivier Chabloz
"""
from logging import getLogger
import time
from datetime import timedelta, datetime

from sqlalchemy import or_
import pandas as pd

from swisesight.libs.storage.aimsight_database.measurement_processing import process_shift_sensors

from app.extensions import AIMsightDB_new_session
from .processing import substract_reference, filter_error_from_threshold

logger = getLogger()

# map of columns to be renamed. For now it is done in the codebase.
rename_cols = {"error_factor": "error_value", "horizontal_shift": "x", "vertical_shift": "y"}


def get_sensor_references(sensors, db=None):
    """
    return a pd.Dataframe of reference points
    """
    measurement_ids = [int(x) for x in sensors.measurement_id.unique()]
    dt = [pd.to_datetime(x) for x in sensors.start_date.unique()]
    if len(measurement_ids) > len(dt):
        # dans ce cas là, c'est la merde. au moins 1 des sensor n'aura pas de valeur.
        # du coup pour le moment on vire la contrainte dt
        dt = None
    if db is None:
        db = AIMsightDB_new_session()
    m = db.get_optical_shifts_from_measurement(measurement_ids, limit_to_datetime=dt)
    m = m.drop(columns=["id", "name"])

    references = sensors.merge(m, on=["measurement_id", "sensor_id"])

    # list of sensors without a ref.
    no_ref_list = list(sensors[~sensors.sensor_name.isin(references.sensor_name)].sensor_name)
    if len(no_ref_list) > 0:
        logger.warning("get_sensor_references: Sensors have no reference points: %s", no_ref_list)
    references = references.rename(columns=rename_cols)
    references = references.rename(columns={"measurement_id": "reference_id"})
    return references


def get_optical_shifts_from_sensors(sensors, start_date=None, end_date=None, db=None):
    if db is None:
        db = AIMsightDB_new_session()
    if isinstance(end_date, str):
        end_date = datetime.fromisoformat(end_date)
        if end_date.hour == 0 and end_date.minute == 0:
            end_date = end_date.date() + timedelta(days=1)
    if isinstance(start_date, str):
        start_date = datetime.fromisoformat(start_date)
    shifts = db.get_optical_shifts_from_sensors(sensors, start_date, end_date)
    shifts = shifts.rename(columns=rename_cols)  # to modify in DB!
    shifts.loc[shifts.error_value.isnull(), "error_value"] = 1.0
    return shifts


def get_first_valid_value(db, sensor_map, start_date):
    time_utc = db.T.Shift_sensor.time_utc
    sensor_name = db.T.Sensor.name.label("sensor_name")
    shifts = db.T.Shift_sensor
    if isinstance(start_date, str):
        start_date = datetime.fromisoformat(start_date)
    q = db.session.query(shifts, sensor_name).distinct(shifts.sensor_id).filter(time_utc < start_date)
    # .filter(shifts.sensor_id.in_(sensors))
    filter_list = []
    for sensor_id, err_value in sensor_map.items():
        filter_list.append((db.T.Sensor.id == sensor_id) & (db.T.Shift_sensor.error_factor <= err_value))
    q = q.filter(or_(*filter_list))
    q = q.join(db.T.Sensor, shifts.sensor_id == db.T.Sensor.id).order_by(
        shifts.sensor_id, shifts.time_utc.desc()
    )
    return pd.read_sql(q.statement, q.session.bind)


def get_sensor_first_valid_value(db, sensor_id, error_thresh, start_date):
    time_utc = db.T.Shift_sensor.time_utc
    shifts = db.T.Shift_sensor
    if isinstance(start_date, str):
        start_date = datetime.fromisoformat(start_date)
    q = db.session.query(shifts).filter(time_utc < start_date).filter_by(sensor_id=sensor_id)
    q = q.filter(db.T.Shift_sensor.error_factor <= error_thresh)
    q = q.order_by(shifts.time_utc.desc()).limit(1)
    return pd.read_sql(q.statement, q.session.bind)


def get_sensors_data(sensors, start_date, end_date, threshold=False):
    """
    get data from AIMsight database and substract reference measurement

    sensors is the output of build_sensor_list
    start_date, end_date: date format "YYYY-MM-DD:HH:mm"
    threshold: Boolean, whether or not shifts data is filtered base on error_threshold
    """
    db = AIMsightDB_new_session()
    time_0 = time.time()
    refs = get_sensor_references(sensors, db=db)
    list_sensor = list(sensors.sensor_id)
    time_1 = time.time()
    logger.debug("get_sensor_references(%s), got %d refs in %.2f[s]", list_sensor, len(refs), time_1 - time_0)
    shifts = get_optical_shifts_from_sensors(list_sensor, start_date, end_date, db=db)
    time_2 = time.time()
    logger.debug(
        "get_optical_shifts_from_sensors(%s), got %d shifts in %.2f[s]",
        list_sensor,
        len(shifts),
        time_2 - time_1,
    )
    shifts = shifts.pipe(substract_reference, refs)
    # valids = shifts.loc[shifts.error_value <= shifts.error_threshold].groupby("sensor_id", group_keys=False).first().reset_index()
    r = refs[["sensor_id", "error_threshold"]].set_index("sensor_id")["error_threshold"].to_dict()
    first_values = []
    for k, v in r.items():
        val = get_sensor_first_valid_value(db, k, v, start_date)
        if len(val) == 0:
            val = shifts.loc[(shifts.sensor_id == k)]
            val = val.loc[val.time_utc == val.time_utc.min()]
        first_values.append(val)
    if len(first_values) == 0:
        return pd.DataFrame()
    m = pd.concat(first_values)
    default = m[["sensor_id", "horizontal_shift", "vertical_shift"]].rename(
        columns={"horizontal_shift": "x_default", "vertical_shift": "y_default"}
    )

    if threshold:
        shifts = shifts.pipe(filter_error_from_threshold)
    return shifts.merge(default, on="sensor_id", suffixes=(None, None))


def get_sensors_from_device(dev):
    default_params = {
        "error_threshold": 0.7,
        "pattern_type": "target",
        "measurement_id": dev.reference_measurement,
    }
    return pd.DataFrame(build_sensor_list(dev.serial_number, default_params, start_limit=dev.start_date))


def build_sensor_list(acquisition_unit_id, load_values=None, load_archived=False, db=None, start_limit=None):
    """
    retreive every sensors for a given acquisition unit from the database.

    return their json fields.
    By default load all values present in processing_param
    load_values: dict of key:value, load keys from processing_param and if not present, set value as
    the default value.
    """
    if db is None:
        db = AIMsightDB_new_session()
    acq_unit = db.acquisition_units.filter_by(id=acquisition_unit_id).one()
    table_output = []

    for s in acq_unit.sensors:
        processing_chain = s.get_active_processing_chain(datetime.now(), from_limit=start_limit)
        if processing_chain is None:
            continue
        param = processing_chain.parameters.copy()
        if not load_archived and param.get("archive", False):
            continue
        if load_values is not None:
            param = {k: param.get(k, v) for k, v in load_values.items()}

        param["sensor_name"] = s.name
        param["sensor_id"] = s.id
        param["start_date"] = processing_chain.start_date
        table_output.append(param)
    table_output.sort(key=lambda x: repr(x["sensor_name"]))
    return table_output


def sensor_process_from_ref(sensors, start_date=None, observer=None):
    """Process images from a given sensor"""

    db = AIMsightDB_new_session()
    sensor = db.get_sensor(sensors[0])

    ref_id = sensor.parameters["measurement_id"]
    ref_measurement = db.get_measurement(ref_id)
    if start_date is None:
        start_date = ref_measurement.time_utc

    meas_type = ref_measurement.measurement_type
    q = db.session.query(db.T.Measurement.id)
    q = q.filter_by(acquisition_unit=sensor.acquisition_unit)
    q = q.filter(db.T.Measurement.time_utc >= start_date)
    q = q.filter_by(measurement_type=meas_type)
    total = q.count()

    to_delete = db.session.query(db.T.Shift_sensor).filter(db.T.Shift_sensor.measurement_id.in_(q))
    to_delete = to_delete.filter(db.T.Shift_sensor.sensor_id.in_(sensors))
    to_delete = to_delete.filter(db.T.Shift_sensor.time_utc >= start_date)

    number_of_row_deleted = to_delete.delete(synchronize_session=False)
    logger.info("%d rows of shift_sensors were deleted.", number_of_row_deleted)
    db.commit_session()

    for i, mid in enumerate(q):
        (mid,) = mid
        # sensor.shift_sensor.filter_by(measurement_id=mid).delete()
        results = process_shift_sensors(db.get_measurement(mid), db, sensors)
        for r in results.filter(db.T.Shift_sensor.sensor_id.in_(sensors)):
            logger.info(
                "sensor_process_from_ref:[%d/%d]: processed %s mid(%d), error_factor: %f",
                i + 1,
                total,
                r.sensor,
                mid,
                r.error_factor,
            )
            if callable(observer):
                observer(i + 1, total)
        db.commit_session()


def get_battery_voltage(system_name=None, days_limit=1):
    db = AIMsightDB_new_session()
    limit = datetime.now() - timedelta(days=days_limit)

    q = db.session.query(
        db.T.Status.time_utc, db.T.Status.value.label("BusVoltage"), db.T.Account.username.label("account")
    )

    q = q.filter_by(flag=db.get_flag("BusVoltage"))
    if isinstance(system_name, str):
        system_name = db.get_account(system_name)
        if system_name is not None:
            q = q.filter_by(account=system_name)
    q = q.filter(db.T.Status.time_utc > limit)
    q = q.join(db.T.Account)
    return pd.read_sql(q.statement, q.session.bind)


def get_battery_report(days_limit=1):
    df = get_battery_voltage(days_limit=days_limit)

    dff = df.set_index("time_utc").groupby(["account"]).resample("15T").mean().reset_index()
    dff = dff.set_index("time_utc").pivot(columns=["account"]).interpolate(limit_direction="both")
    return dff


def get_system_values(system_name: str, flag_name: str, days_limit: int = 1) -> pd.DataFrame:
    """
    Fetches status values for a specific system with a particular flag.
    Include also timestamps from 'last_sync' status.

    Parameters:
        - system_name (str): The name of the system for which status values are requested.
        - flag_name (str): The name of the flag for which status values are requested.
        - days_limit (int): The maximum number of past days for which status values should be retrieved.
                            Default value is 1, indicating the last day only.

    Returns:
        - pd.DataFrame: A pandas DataFrame with time_utc as the index and flag_name's values as columns.
                       The DataFrame contains status values for the specified system and flag,
                       along with corresponding synchronization timestamps.
    """

    db = AIMsightDB_new_session()
    flags = {x.name: x.id for x in db.flags}
    limit = datetime.now() - timedelta(days=days_limit)
    system_name = db.get_gateway(system_name)
    q = db.session.query(db.T.Flag.name, db.T.Status.time_utc, db.T.Status.value, db.T.Status.comment)
    q = q.filter(
        ((db.T.Flag.id == flags[flag_name]) | (db.T.Flag.id == flags["last_sync"]))
        & (db.T.Status.account_id == system_name.account_id)
    )
    q = q.filter(db.T.Status.time_utc >= limit)
    q = q.join(db.T.Flag, db.T.Status.flag_id == db.T.Flag.id)
    df = pd.read_sql(q.statement, q.session.bind)
    df.sort_values("time_utc", inplace=True)
    df = df[["time_utc", "name", "value"]].pivot(columns=["name"], index="time_utc")
    df.drop(columns=[("value", "last_sync")], inplace=True, errors="ignore")
    try:
        df.columns = pd.Index([flag_name])
    except ValueError:
        pass  # No column with 'flag_name' values.. df is empty
    return df


def get_system_info(system_name, flags) -> dict:
    """
    Fetch the last received status values from a System
    Returns dict with the value's description as key
    """
    db = AIMsightDB_new_session()
    system = db.get_gateway(system_name)

    r = {}
    if system is None:
        return r
    for flag in flags:
        f = system.account.last_status(flag)
        if f is not None:
            r[flag] = f.comment if f.comment is not None else f.value

    return r
