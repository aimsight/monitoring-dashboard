"""
  Copyright (c) 2020-2021  AIMsight SA
  Written By Olivier Chabloz
"""
from dash import Dash, no_update, callback_context, exceptions
import dash_bootstrap_components as dbc
from dash.dependencies import Input, State, Output, ClientsideFunction
from dash import CeleryManager
from .Dash_fun import (
    apply_layout_with_auth,
    query_and_process_outputs,
    create_plot,
)
from app.base.auth.common import can_explore_device
from app.base.auth import is_authenticated
from app.base.models import Device
from dash import dcc, html
from datetime import date, timedelta, datetime
from flask import request, send_file, abort
import time, io
import pandas as pd, numpy as np
import logging

logger = logging.getLogger()

url_base = "/dash/explore/"

layout = html.Div(
    [
        dcc.Location(id="explore_location"),
        dbc.Row(
            [
                dbc.Col(
                    dcc.DatePickerRange(
                        id="sensors-date-range",
                        min_date_allowed=date.today() - timedelta(days=120),
                        start_date=date.today() - timedelta(days=7),
                        end_date=date.today(),
                        display_format="D/M/YYYY",
                    ),
                ),
                dbc.Col(
                    [
                        dbc.Label("Email Subscription", style={"margin-right": "15px"}),
                        dbc.Checklist(
                            options=[
                                {"label": "Threshold Alarm", "value": "threshold_alarm"},
                                {"label": "Obstruction Alarm", "value": "obstruction_alarm"},
                            ],
                            value=[],
                            id="user_notification_enable_checklist",
                            inline=True,
                            style={"display": "inline"},
                        ),
                    ],
                ),
                dbc.Col(
                    [
                        dbc.Button(
                            dbc.Spinner("download CSV", id="download_csv_spin"),
                            color="secondary",
                            id="download_csv-btn",
                        ),
                        dcc.Download(id="download-explore_data"),
                    ],
                    style={"text-align": "right"},
                ),
            ]
        ),
        dbc.Row(
            dbc.Col(
                [
                    dcc.Loading(
                        color="#73879C",
                        children=html.Div(id="container_graph_diffv", children=[]),
                    )
                ]
            )
        ),
        dbc.Row(dbc.Col([html.Div(id="container_graph_diffh", children=[])])),
        dbc.Row(dbc.Col([html.Div(id="container_graph_dist", children=[])])),
        dbc.Row(dbc.Col([html.Div(id="container_graph_angle", children=[])])),
        html.H1(id="clickText"),
    ],
    style={"overflow-x": "hidden", "min-height": "390px"},
)


def Add_Dash(server):
    celery_manager = None
    if hasattr(server, "celery_handle"):
        celery_manager = CeleryManager(server.celery_handle)
    app = Dash(
        server=server,
        url_base_pathname=url_base,
        external_stylesheets=[dbc.themes.BOOTSTRAP],
        background_callback_manager=celery_manager,
        external_scripts=[
            "/static/build/js/iframeResizer.contentWindow.min.js",
            "/static/build/js/explore.js",
        ],
    )
    apply_layout_with_auth(app, layout)

    @app.callback(
        [
            Output("download-explore_data", "data"),
            Output("download_csv_spin", "children"),
        ],
        [
            Input("download_csv-btn", "n_clicks"),
        ],
        [
            State("explore_location", "pathname"),
            State("sensors-date-range", "start_date"),
            State("sensors-date-range", "end_date"),
        ],
        prevent_initial_call=True,
        background=True,
    )
    def update_download_links(nclicks, pathname, start_date, end_date):
        if nclicks < 1:
            raise exceptions.PreventUpdate
        with app.server.app_context():
            proc = app.server.celery_handle.tasks["app.tasks.async_tasks.prepare_explore_download"]
            dev = Device.get_from_pathname(pathname)
            kwargs = dict(start_date=start_date, end_date=end_date, filtered_outputs=[])
            r = proc.apply(args=(dev.name, "csv"), kwargs=kwargs)
            try:
                filename, content = r.result
            except TypeError:
                r.forget()

            return (dict(base64=False, content=content, filename=filename), no_update)

    @app.callback(
        [
            Output("container_graph_diffv", "children"),
            Output("container_graph_diffh", "children"),
            Output("container_graph_dist", "children"),
            Output("container_graph_angle", "children"),
        ],
        [
            Input("explore_location", "pathname"),
            Input("sensors-date-range", "start_date"),
            Input("sensors-date-range", "end_date"),
            Input("explore_location", "search"),
        ],
        background=True,
    )
    def process_output_graph(pathname, start_date, end_date, parameters):
        with app.server.app_context():
            dev = Device.get_from_pathname(pathname)

            parameters = parameters[1:].split("&")

            theme = dev.theme
            outputs = [d.as_dict() for _, d in dev.outputs.items()]
            # 1st, get the list of needed sensors
            start_datetime = datetime.strptime(start_date, "%Y-%m-%d")
            start_date = start_date if start_datetime > dev.start_date else dev.start_date

            query_start = time.time()
            diff_data, dist_data, angle_data = query_and_process_outputs(
                dev,
                outputs,
                dev.threshold,
                start_date,
                end_date,
                dev.reference_measurement,
            )
            qdelta = time.time() - query_start

            if diff_data is None and dist_data is None and angle_data is None:
                return (no_update,) * 4

            notifications = dev.get_notifications(from_date=start_date, until_date=end_date)

            diffV_graph, diffH_graph, dist_graph, angle_graph = (no_update,) * 4

            if diff_data is not None:
                diff_data = diff_data.reset_index()  # reset index to put back time_utc as a column
                if dev.show_hdiff:
                    diffH_graph = create_plot(
                        diff_data,
                        "time_utc",
                        ["horizontal_shift"],
                        "signal_name",
                        rows=1,
                        y_title="displacement [mm]",
                        # x_title="Time UTC",
                        sub_titles=("Horizontal relative displacement",),
                        theme=theme,
                        notifications=notifications,
                        min_autoscale=dev.min_autoscale,
                        max_autoscale=dev.max_autoscale,
                    )
                    diffH_graph = dcc.Graph(
                        id="output_graph_diffH", figure=diffH_graph, style={"height": "800px"}
                    )
                if dev.show_vdiff:
                    diffV_graph = create_plot(
                        diff_data,
                        "time_utc",
                        ["vertical_shift"],
                        "signal_name",
                        rows=1,
                        y_title="displacement [mm]",
                        # x_title="Time UTC",
                        sub_titles=("Vertical relative displacement",),
                        theme=theme,
                        notifications=notifications,
                        min_autoscale=dev.min_autoscale,
                        max_autoscale=dev.max_autoscale,
                    )
                    diffV_graph = dcc.Graph(
                        id="output_graph_diffV", figure=diffV_graph, style={"height": "800px"}
                    )

            if dist_data is not None:
                dist_data = dist_data.reset_index()  # reset index to put back time_utc as a column
                dist_graph = create_plot(
                    dist_data,
                    "time_utc",
                    ["length"],
                    "signal_name",
                    rows=1,
                    y_title="distances [mm]",
                    # x_title="Time UTC",
                    sub_titles=("Distance between two points",),
                    theme=theme,
                    notifications=notifications,
                    min_autoscale=dev.min_autoscale,
                    max_autoscale=dev.max_autoscale,
                )
                dist_graph = dcc.Graph(id="output_graph_dist", figure=dist_graph, style={"height": "800px"})
            if angle_data is not None:
                angle_data = angle_data.reset_index()  # reset index to put back time_utc as a column
                angle_graph = create_plot(
                    angle_data,
                    "time_utc",
                    ["alpha"],
                    "signal_name",
                    rows=1,
                    y_title="angle [rad]",
                    # x_title="Time UTC",
                    sub_titles=("Angle between two points",),
                    theme=theme,
                    notifications=notifications,
                    min_autoscale=dev.min_autoscale,
                    max_autoscale=dev.max_autoscale,
                )
                angle_graph = dcc.Graph(
                    id="output_graph_angle", figure=angle_graph, style={"height": "800px"}
                )

            qtotal = time.time() - query_start
            alert = "Query Completed. (query {:2.2f}s, processing {:2.2f}s)".format(qdelta, qtotal - qdelta)
        return (diffV_graph, diffH_graph, dist_graph, angle_graph)

    @app.callback(
        [
            Output("sensors-date-range", "start_date"),
            Output("sensors-date-range", "end_date"),
            Output("sensors-date-range", "min_date_allowed"),
            Output("sensors-date-range", "max_date_allowed"),
        ],
        [Input("explore_location", "pathname")],
    )
    def load_datepicker(pathname):
        DEFAULT_SHOW_ALL = False  # ? Add this Behavior Flag just in case someone change its mind.
        dev = Device.get_from_pathname(pathname)
        min_date = dev.start_date
        max_date = date.today()
        if dev.end_date is not None and dev.end_date.date() < max_date:
            max_date = dev.end_date.date()

        if DEFAULT_SHOW_ALL:
            start = dev.start_date
            stop = max_date
        else:
            stop = max_date
            start = max_date - timedelta(days=10)
            if start < dev.start_date.date():  # set default date to the start of Device if there is
                start = dev.start_date.date()  # less than 10 days of data

        return start, stop, min_date, max_date + timedelta(days=1)

    app.clientside_callback(
        ClientsideFunction(namespace="clientside", function_name="display"),
        Output("clickText", "children"),
        [Input("output_graph_diffV", "clickData")],
        prevent_initial_call=True,
    )

    return app.server
