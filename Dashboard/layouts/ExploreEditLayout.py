"""
  Copyright (c) 2020-2021  AIMsight SA
  Written By Olivier Chabloz
"""
import dash_bootstrap_components as dbc
from dash import dcc, html
from dash import dash_table
from datetime import date

options_checkbox = [
    {"label": "filter out based on threshold", "value": "threshold"},
    {"label": "show Horizontal displacement plot", "value": "show_hdiff"},
    {"label": "show Vertical displacement plot", "value": "show_vdiff"},
]
options_checkbox_values = [v["value"] for v in options_checkbox]


def output_dropdown_template(sensorDict):
    sensors = [{"label": k, "value": x} for k, x in sensorDict.items()]
    return {
        "process_type": {
            "options": [
                {"label": "Sensor1 - Sensor2", "value": "relative_coord"},
                {"label": "Euclidean transform on Sensor1", "value": "euclidean_transform"},
                {"label": "Similarity transform on Sensor1", "value": "similarity_transform"},
                {"label": "Similarity transform with filtered ref", "value": "similarity_transform_interp"},
                {"label": "Distance between Sensor1 and Sensor2", "value": "dist"},
                {"label": "Angle between Sensor1 and Sensor2", "value": "angle"},

            ]
        },
        "sensor1": {"options": sensors},
        "sensor2": {"options": sensors},
        "interp": {
            "clearable": True,
            "options": [
                {"label": "Standard", "value": "standard"},
                {"label": "With resampling", "value": "resample"},
                {"label": "Prioritize", "value": "interpolate_priority"},
            ],
        },
    }


layout = html.Div(
    [
        dcc.Location(id="explore_location"),
        dbc.Alert(color="primary", id="notification-alert", is_open=False),
        dbc.Row(
            [
                dbc.Col(
                    [
                        dcc.DatePickerRange(
                            id="sensors-date-range",
                            min_date_allowed=date(2020, 10, 1),
                            start_date=date.today(),
                            end_date=date.today(),
                            display_format="D/M/YYYY",
                        ),
                    ]
                ),
            ]
        ),
        dbc.Row(
            dbc.Col(
                dcc.Tabs(
                    id="tabs-example",
                    value="tab-raw_data",
                    children=[
                        dcc.Tab(label="Statistic plots", value="tab-statistics"),
                        dcc.Tab(label="Raw values plots", value="tab-raw_data"),
                    ],
                ),
            ),
        ),
        dbc.Row(
            dbc.Col(
                [
                    dcc.Loading(
                        id="loading-1",
                        type="circle",
                        children=[
                            html.Div([
                                html.Div(
                                [
                                    dbc.Select(
                                        id="select_graph",
                                    ),
                                    dcc.Graph(id="graph_sensors", style={"height": 800}),
                                    dcc.Store(id="clientside-figure-store-sensors"),
                                ]
                            )
                            ],
                            style={"min-height": 800}),
                        ],
                    ),
                ],
            )
        ),
        dbc.Row(
            [
                dbc.Col(
                    [
                        html.H3(children="Options:"),
                        dcc.Checklist(
                            id="options_checkbox",
                            options=options_checkbox,
                            value=[],
                            labelStyle={"display": "block"},
                            # style={"display": "inline-block"},
                        ),
                    ]
                ),
                dbc.Col([html.Div(id="option_status")]),
            ]
        ),
        dbc.Row(dbc.Col(html.H3(children="Output"))),
        dbc.Row(dbc.Col(dbc.Alert("Please note that the alarm threshold value is absolute and triggers the alarm on both positive and negative values of the signal being monitored.", color="warning"))),
        dbc.Row(
            dbc.Col(
                dash_table.DataTable(
                    id="output_table",
                    columns=[
                        {"id": "name", "name": "Name"},
                        {"id": "process_type", "name": "Type", "presentation": "dropdown"},
                        {"id": "sensor1", "name": "First Sensor", "presentation": "dropdown"},
                        {"id": "sensor2", "name": "Second Sensor", "presentation": "dropdown"},
                        {"id": "interp", "name": "Interpolate", "presentation": "dropdown"},
                        {"id": "roll_med", "name": "Rolling_median", "type": "numeric"},
                        {"id": "scale", "name": "Scaling Factor", "type": "numeric"},
                        {"id": "vertical_offset", "name": "Vertical Offset", "type": "numeric"},
                        {
                            "id": "horizontal_offset",
                            "name": "Horizontal Offset",
                            "type": "numeric",
                        },
                        {
                            "id": "Valarm_threshold",
                            "name": "Alarm threshold (V, dist, angle)",
                            "type": "numeric",
                        },
                        {
                            "id": "Halarm_threshold",
                            "name": "Alarm threshold (H)",
                            "type": "numeric",
                        },
                    ],
                    editable=True,
                    row_deletable=True,
                    dropdown={},
                    data=[],
                )
            )
        ),
        dbc.Row(
            dbc.Col(
                dbc.Button(
                    "Add Row", color="primary", className="mr-1", id="addRow-Button", n_clicks=0
                )
            )
        ),
        dbc.Row(children=" ", style={"height": "500"}),
    ]
)