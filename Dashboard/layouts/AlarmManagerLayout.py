import dash_bootstrap_components as dbc
from dash import dcc, html
from dash import dash_table

layout_base = html.Div(
    children=[
        dcc.Location(id="explore_location"),
        dcc.Store(id="summary_trigger", data={}),
        dcc.Store(id="edit_trigger", data={}),
        html.Div(id="dash_alarm_content"),
    ],
    style={"overflow-x": "hidden", "min-height": "390px"},
)


def layout_summary(device):
    return [
        dbc.Row([dbc.Col(dcc.Graph(id="alarm_summary_graph"))]),
        dbc.Row(
            [
                dbc.Accordion(
                    [alarm_parent_layout(a) for a in device.alarms if a.parent is None], id="alarm_container"
                ),
            ]
        ),
        dbc.Button("New", id="new_alarm_button"),
    ]


def layout_history(device, alarm):
    return [
        dbc.Row([dbc.Col(dcc.Graph(id="alarm_history_graph"))]),
        dbc.Row(
            [
                dash_table.DataTable(
                    id="alarm_history_table",
                    columns=[
                        {"name": "ID", "id": "id"},
                        {"name": "Event Date", "id": "event_date"},
                        {"name": "Status", "id": "status"},
                    ],
                    data=[
                        {"id": x.id, "event_date": x.event_date, "status": x.status}
                        for x in alarm.alarm_events
                    ],
                    row_selectable="single",
                ),
            ]
        ),
        dbc.Row(
            [
                dbc.Col(
                    [
                        dbc.Button("Back", color="secondary", href=f"/dash/alarm_manager/{device.name}"),
                    ]
                )
            ]
        ),
    ]


def load_alarms_table_data(alarm_childs):
    return [
        {
            "id": x.id,
            "alarm_label": x.label,
            "signal_type": x.signal_type,
            "signal_id": x.sensor_id,
            "signal_value": x.signal_value_type,
            "min_range": x.min_range,
            "max_range": x.max_range,
            "alarm_status": x.get_status(),
        }
        for x in alarm_childs
    ]


def alarm_parent_layout(alarm):
    childs = alarm.children if len(alarm.children) > 0 else [alarm]
    has_triggered_alarm = "triggered" in [x.get_status() for x in childs]
    return dbc.AccordionItem(
        [
            dash_table.DataTable(
                id={"type": "alarm_list", "index": alarm.id},
                columns=[
                    {"name": "Label", "id": "alarm_label"},
                    {"name": "signal type", "id": "signal_type"},  # device_output ou raw signal
                    {"name": "signal", "id": "signal_id"},  # deviceOutput_id or sensor_id
                    {"name": "signal value", "id": "signal_value"},
                    {"name": "minimum range", "id": "min_range"},
                    {"name": "maximum range", "id": "max_range"},
                    {"name": "Status", "id": "alarm_status"},
                ],
                data=load_alarms_table_data(childs),
                row_selectable="single",
                style_data_conditional=[
                    {
                        "if": {"filter_query": "{alarm_status} = 'triggered'"},
                        "backgroundColor": "orange",
                        "color": "white",
                    },
                    {
                        "if": {"filter_query": "{alarm_status} = 'inactive'"},
                        "backgroundColor": "grey",
                        "color": "white",
                    },
                    {
                        "if": {"filter_query": "{alarm_status} = 'unknown'"},
                        "backgroundColor": "brown",
                        "color": "white",
                    },
                ],
            ),
            dbc.ButtonGroup(
                [
                    dbc.Button("Edit", color="info", id={"type": "edit_alarm_button", "index": alarm.id}),
                    dbc.Button(
                        "Edit parent",
                        color="primary",
                        id={"type": "edit_parent_alarm_button", "index": alarm.id},
                    ),
                    dbc.Button(
                        "Rearm", id={"type": "rearm_alarm_button", "index": alarm.id}, color="warning"
                    ),
                    dbc.Button(
                        "History", id={"type": "history_alarm_button", "index": alarm.id}, color="secondary"
                    ),
                    dbc.Button(
                        "Deactivate",
                        id={"type": "deactivate_alarm_button", "index": alarm.id},
                        color="secondary",
                    ),
                    dbc.Button(
                        "Delete", id={"type": "delete_alarm_button", "index": alarm.id}, color="danger"
                    ),
                ]
            ),
        ],
        title=alarm.label,
        item_id={"type": "accordion_item_alarm", "index": alarm.id},
        className="warn" if has_triggered_alarm else None,
    )


def drpdown_or_select(id, alarm):
    children = getattr(alarm, "children", [])
    if alarm is None or len(children) > 0:
        return dcc.Dropdown(
            options=[],  # dynamic
            multi=True,
            id=id,
            value=list(set([getattr(x, id, None) for x in children])),
        )
    else:
        return dbc.Select(
            options=[],  # dynamic
            id=id,
            value=getattr(alarm, id, None),
        )


def layout_new_edit_alarm(device, alarm=None):
    return [
        dbc.Row(
            [
                dbc.Col(
                    dcc.Graph(
                        id="alarm_graph",
                    ),
                ),
            ]
        ),
        dbc.Row(
            [
                dbc.Col(
                    [
                        dbc.InputGroup(
                            [
                                dbc.InputGroupText("Label"),
                                dbc.Input(
                                    placeholder="Alarm name",
                                    id="alarm_label",
                                    value=getattr(alarm, "label", ""),
                                ),
                            ],
                            className="mb-3",
                        ),
                        dbc.InputGroup(
                            [
                                dbc.InputGroupText("Signal Type"),
                                dbc.Select(
                                    options=[
                                        {"label": "Processed Output", "value": "device_output"},
                                        {"label": "Raw signal", "value": "sensor"},
                                    ],
                                    id="signal_type",
                                    value=getattr(alarm, "signal_type", "device_output"),
                                    disabled=alarm is not None,
                                ),
                                drpdown_or_select("sensor_id", alarm),
                                drpdown_or_select("signal_value_type", alarm),
                            ],
                            className="mb-3",
                        ),
                        dbc.InputGroup(
                            [
                                dbc.InputGroupText("Minimum value"),
                                dbc.Input(
                                    type="number", id="min_val", value=getattr(alarm, "min_range", None)
                                ),
                                dbc.InputGroupText("Maximum value"),
                                dbc.Input(
                                    type="number", id="max_val", value=getattr(alarm, "max_range", None)
                                ),
                            ],
                            className="mb-3",
                        ),
                        dbc.InputGroup(
                            [
                                dbc.InputGroupText("Alarm Logic"),
                                dbc.Select(
                                    options=[
                                        {
                                            "label": "consecutive events outside range",
                                            "value": "consecutive_count",
                                        },
                                        {
                                            "label": "a percentage of events outside range",
                                            "value": "percentage",
                                        },
                                    ],
                                    value=getattr(alarm, "alarm_logic", "consecutive_count"),
                                    id="alarm_logic_type",
                                ),
                                dbc.InputGroupText("value"),
                                dbc.Input(
                                    type="number",
                                    id="alarm_logic_val",
                                    value=getattr(alarm, "logic_value", None),
                                ),
                            ],
                            className="mb-3",
                        ),
                        dbc.InputGroup(
                            [
                                dbc.InputGroupText("Query Period"),
                                dbc.Input(
                                    type="number",
                                    id="query_period_h",
                                    value=getattr(alarm, "query_period", 2),
                                ),
                                dbc.InputGroupText("hours"),
                            ],
                            className="mb-3",
                        ),
                        dbc.InputGroup(
                            [
                                dbc.InputGroupText("Rearming Period"),
                                dbc.Input(
                                    type="number",
                                    id="rearming_time_h",
                                    value=getattr(alarm, "rearming_period", None),
                                ),
                                dbc.InputGroupText("hours"),
                            ],
                            className="mb-3",
                        ),
                        dbc.InputGroup(
                            [
                                dbc.InputGroupText("Assigned group"),
                                dbc.Select(
                                    options=[{"label": x.name, "value": x.id} for x in device.user_groups],
                                    id="assigned_group",
                                    value=getattr(alarm, "group_id", None),
                                ),
                            ],
                            className="mb-3",
                        ),
                    ],
                ),
            ]
        ),
        dbc.Row(
            [
                dbc.Col(
                    [
                        dbc.Button("Save", id="save_button"),
                        dbc.Button("Cancel", color="secondary", href=f"/dash/alarm_manager/{device.name}"),
                    ]
                )
            ]
        ),
    ]
