import dash_bootstrap_components as dbc
from dash import html, dcc, dash_table
from datetime import date, datetime
import numpy as np

layout = html.Div(
    children=[
        dcc.Location(id="explore_location"),
        dcc.Store(id="datastore"),
        dbc.Row(
            [
                dbc.Col(
                    [
                        dcc.DatePickerSingle(
                            id="date-selector",
                            min_date_allowed=datetime(2020, 10, 1),
                            max_date_allowed=date.today(),
                            initial_visible_month=date.today(),
                            date=date.today(),
                            display_format="D/M/YYYY",
                        )
                    ]
                ),
                dbc.Col(
                    [
                        dbc.Col(
                            dcc.Checklist(
                                id="imIRON",
                                options=[
                                    {"label": "View IRON images", "value": "IRON"},
                                ],
                            ),
                        ),
                        dcc.Dropdown(id="timeSelector"),
                    ]
                ),
            ],
        ),
        dbc.Row(
            [
                dbc.Col(
                    dcc.Loading(
                        id="loading-1",
                        type="circle",
                        children=dcc.Graph(
                            id="graph",
                            config={"modeBarButtonsToAdd": ["drawrect", "eraseshape"]},
                            style={"height": 800},
                        ),
                    )
                ),
            ]
        ),
        dbc.Row(
            [
                dbc.Col(
                    dbc.ButtonGroup(
                        [
                            dbc.Button(
                                "Refresh Image",
                                color="secondary",
                                className="mr-1",
                                id="btn-refresh",
                            ),
                            dbc.Button(
                                "Download",
                                color="secondary",
                                href="", # the link
                                download="image", # filename
                                className="mr-1",
                                id="btn-download",
                                external_link=True,
                                disabled=True,
                            ),
                        ],
                    ),
                    width=2,
                ),
                dbc.Col(
                    html.Div("Slider Value: 5.00E-04", id="quantile_slider-value"),
                    width=2,
                ),
                dbc.Col(
                    [
                        dcc.Slider(
                            0,
                            3,
                            0.01,
                            id="quantile_slider",
                            marks={i: "{}".format(10 ** (-i - 2)) for i in range(4)},
                            value=(-np.log10(5e-4) - 2),
                        ),
                    ],
                    width=8,
                ),
            ]
        ),
        dbc.Row(
            dbc.Col(
                dcc.Checklist(
                    id="imSelectOptions",
                    options=[
                        {"label": "Edit Selected", "value": "EDIT"},
                    ],
                    value=[],
                ),
            )
        ),
        dbc.Row(
            dbc.Col(
                dcc.Loading(
                    dash_table.DataTable(
                        id="pattern_table",
                        data=[],
                        columns=(
                            [
                                {"id": "sensor_name", "name": "Name"},
                                {"id": "pattern_type", "name": "Type", "presentation": "dropdown"},
                                {"id": "pattern_size", "name": "Size", "presentation": "dropdown"},
                                {"id": "x", "name": "x", "editable": True, "type": "numeric"},
                                {"id": "y", "name": "y", "editable": True, "type": "numeric"},
                                {
                                    "id": "error_threshold",
                                    "name": "Error Threshold",
                                    "editable": True,
                                    "type": "numeric",
                                },
                                {
                                    "id": "start_date",
                                    "name": "Reference date",
                                    "editable": False,
                                },
                            ]
                        ),
                        editable=True,
                        dropdown={
                            "pattern_type": {
                                "clearable": False,
                                "options": [
                                    {"label": "Global reference", "value": "global_ref"},
                                    {"label": "Local reference", "value": "ref"},
                                    {"label": "Target Group 1", "value": "target"},
                                    {"label": "Target Group 2", "value": "target02"},
                                    {"label": "Target Group 3", "value": "target03"},
                                    {"label": "Target Group 4", "value": "target04"},
                                    {"label": "Target Group 5", "value": "target05"},
                                ],
                            },
                            "pattern_size": {
                                "clearable": False,
                                "options": [
                                    {"label": "20x20", "value": 20},
                                    {"label": "30x30", "value": 30},
                                    {"label": "40x40", "value": 40},
                                    {"label": "50x50", "value": 50},
                                    {"label": "100x100", "value": 100},
                                    {"label": "200x200", "value": 200},
                                    {"label": "300x300", "value": 300},
                                    {"label": "500x500", "value": 500},
                                ],
                            },
                        },
                        row_selectable="multi",
                        style_cell_conditional=[
                            {"if": {"column_id": "pattern_type"}, "textAlign": "left"}
                        ],
                    ),
                    type="circle",
                ),
            )
        ),
        dbc.Row(
            [
                dbc.Col(
                    [
                        dbc.Button(
                            "Save patterns",
                            color="primary",
                            className="mr-1",
                            id="btn-save_parm",
                            style={"margin-top": "50px"},
                        ),
                        dbc.Button(
                            "Save metadata only",
                            color="secondary",
                            id="btn-save_meta",
                            style={"margin-top": "50px"},
                        ),
                        dbc.Button(
                            "Archive selected",
                            color="warning",
                            id="btn-archive",
                            style={"margin-top": "50px"},
                        ),
                        dbc.Button(
                            "Delete selected",
                            color="danger",
                            id="btn-delete",
                            style={"margin-top": "50px"},
                        ),
                        dbc.Button("Export", color="secondary", id="btn-export", style={"margin-top": "50px"}),
                        dcc.Download(id="download-export"),
                        dcc.Loading(
                            [html.Div(id="save_status")],
                            type="circle",
                            style={"display": "inline-block"},
                        ),
                    ]
                ),
            ]
        ),
        dbc.Row([],id="progress_container"),
        dcc.Interval(id="progress_update_interval", interval=2000),
    ],
)