"""
  Copyright (c) 2020-2021  AIMsight SA
  Written By Olivier Chabloz
"""
from skimage.transform import EuclideanTransform, SimilarityTransform
import numpy as np, pandas as pd
from math import ceil
from datetime import timedelta
import warnings
from logging import getLogger

logger = getLogger()

"""
! Work in progress, theses functions ultimately should be moved to AIMsight_optical
"""


def calculateSimilarityTransform(df):
    """must have x, x_ref, y, y_ref"""
    dst = np.array(df.loc[df.x.notna(), ["x", "y"]])
    src = np.array(df.loc[df.x_ref.notna(), ["x_ref", "y_ref"]])
    model = SimilarityTransform()
    with warnings.catch_warnings():  # ignore warning when a NaN value is encountered
        warnings.simplefilter("ignore", category=RuntimeWarning)
        try:
            model.estimate(src, dst)
        except np.linalg.LinAlgError:
            # most likely both src and dst are empty. Filtering worked correctly.
            logger.debug(f"LinAlgError:: while calculateSimilarityTransform {src=},{dst=}")
            model.params.fill(np.nan)
        except ZeroDivisionError:
            logger.warning(f"ZeroDivisionError: while calculateSimilarityTransform {src=},{dst=}")
            model.params.fill(np.nan)
        return pd.Series(
            {
                "matrix": model.params.copy(),
                "rotation": np.degrees(model.rotation),
                "scale": model.scale,
                "scale_corr": np.linalg.norm(model.params),
                "tx": model.translation[0],
                "ty": model.translation[1],
            }
        )


def calculateEuclideanTransform(df):
    """must have x, x_ref, y, y_ref"""
    dst = np.array(df.loc[df.x.notna(), ["x", "y"]])
    src = np.array(df.loc[df.x_ref.notna(), ["x_ref", "y_ref"]])
    model = EuclideanTransform()
    with warnings.catch_warnings():  # ignore warning when a NaN value is encountered
        warnings.simplefilter("ignore", category=RuntimeWarning)
        try:
            model.estimate(src, dst)
        except np.linalg.LinAlgError:
            # most likely both src and dst are empty. Filtering worked correctly.
            logger.debug(f"LinAlgError: while calculateEuclideanTransform {src=},{dst=}")
            model.params.fill(np.nan)
        except ZeroDivisionError:
            logger.warning(f"ZeroDivisionError: while calculateSimilarityTransform {src=},{dst=}")
            model.params.fill(np.nan)
        return pd.Series(
            {
                "matrix": model.params.copy(),
                "rotation": np.degrees(model.rotation),
                "tx": model.translation[0],
                "ty": model.translation[1],
            }
        )


def process_Sim_matrices(shifts):
   return process_transform(shifts, calculateSimilarityTransform)

def process_Sim_matrices_interp(shifts):
   return process_transform_interpolate(shifts, calculateSimilarityTransform)

def process_Euc_matrices(shifts):
    return process_transform(shifts, calculateEuclideanTransform)

def process_transform(shifts, apply_function):
    columns = ["time_utc", "sensor_name", "x", "y", "x_ref", "y_ref"]
    ref_shifts = shifts.loc[shifts.pattern_type == "global_ref", columns]
    ref_shifts.loc[ref_shifts.x.isna(), ["x_ref", "y_ref"]] = np.nan
    return ref_shifts.groupby(["time_utc"], group_keys=False).apply(apply_function)

def process_transform_interpolate(shifts, apply_function):
    columns = ["time_utc", "sensor_name", "x", "y", "x_ref", "y_ref", "x_default", "y_default"]
    ref_shifts = shifts.loc[shifts.pattern_type == "global_ref", columns].copy()
    selector = (ref_shifts.time_utc == ref_shifts.time_utc.min()) & (ref_shifts.x.isna())
    ref_shifts.loc[selector, "x"] = ref_shifts.loc[selector, "x_default"]
    ref_shifts.loc[selector, "y"] = ref_shifts.loc[selector, "y_default"]
    ref_shifts.interpolate(method="ffill", limit_direction="forward", inplace=True)
    return ref_shifts.groupby(["time_utc"], group_keys=False).apply(apply_function)


def substract_reference(df, refs):
    """
    add 2 columns: horizontal_shift and vertical_shift
    """
    shifts = df.merge(refs, on=["sensor_name", "sensor_id"], suffixes=["", "_ref"])
    shifts.loc[:, "horizontal_shift"] = shifts.x - shifts.x_ref
    shifts.loc[:, "vertical_shift"] = shifts.y - shifts.y_ref
    return shifts


def filter_error_from_threshold(shifts):
    """
    Filter out keys_to_filter if error_value field is greater than error_threshold
    """
    keys_to_filter = ["x", "y", "horizontal_shift", "vertical_shift"]
    shifts.loc[shifts.error_value > shifts.error_threshold, keys_to_filter] = np.NaN
    return shifts


def angle_processing(sensor1, sensor2, scaling_factor):
    # stack to create array of 2D coordinates
    # A = np.stack((sensor1["y"].compressed(), sensor1["x"].compressed()))
    # compressed: only use non-masked values
    # B = np.stack((sensor2["y"].compressed(), sensor2["x"].compressed()))
    ref1x = sensor1.iloc[0].horizontal_shift + sensor1.iloc[0].x
    ref1y = sensor1.iloc[0].vertical_shift + sensor1.iloc[0].y
    ref2x = sensor2.iloc[0].horizontal_shift + sensor2.iloc[0].x
    ref2y = sensor2.iloc[0].vertical_shift + sensor2.iloc[0].y
    V_ref = np.array([ref1y, ref1x]) - np.array([ref2y, ref2x])
    V = sensor1[["y", "x"]] - sensor2[["y", "x"]]  # Pattern location difference vector

    ref_nrm = np.linalg.norm(V_ref)  # longeur nominale
    V = np.stack((V["y"], V["x"]))

    inr = np.inner(V.transpose(), V_ref.reshape(1, 2))
    nrm = np.linalg.norm(V, axis=0)
    theta = np.arccos(np.divide(inr.flatten(), ref_nrm * nrm))  # Unsigned angle difference
    sgn = np.sign(V[0] * V_ref[1] - V[1] * V_ref[0])

    # Angle difference. TODO: Ensure consistent A-B order to ensure positive rotation is clockwise
    alpha = sgn * theta
    # Warning: conv_normdist != pix scale factor!
    length_var = (nrm - ref_nrm) * scaling_factor / ref_nrm
    length = nrm * scaling_factor / ref_nrm
    df = pd.DataFrame(dict(alpha=alpha, length=length, length_var=length_var), index=sensor1.index)
    return df


def interpolation_prioritize(signal, rolling_median):
    last_valid_point = get_last_valid_index(signal, rolling_median)
    if last_valid_point is None:
        return signal  # signal is Nan
    # create fake rows
    projection_range = rolling_median // 2  # ! projection range must be an odd number
    projection_range = projection_range if projection_range % 2 == 1 else projection_range + 1

    botched_signal = apply_rolling_median(signal, 10).interpolate(method="linear")
    # get median value of the n last VALID values
    median = signal[pd.notna(signal["vertical_shift"])][-projection_range:].median()

    first = signal.index[-1] + timedelta(minutes=15)
    last = first + timedelta(minutes=(rolling_median // 2) * 15)
    new_index = pd.to_datetime(np.linspace(first.value, last.value, rolling_median // 2))
    df2 = pd.DataFrame(
        {
            "time_utc": new_index,
            "horizontal_shift": [median.horizontal_shift] * (rolling_median // 2),
            "vertical_shift": [median.vertical_shift] * (rolling_median // 2),
        }
    )
    df2 = df2.set_index("time_utc")
    signal = pd.concat([signal, df2])

    sig = signal.copy()
    vals_to_fill = botched_signal.loc[signal.iloc[:, 0].isna() & (signal.index <= last_valid_point)]
    signal.loc[signal.iloc[:, 0].isna() & (signal.index <= last_valid_point)] = vals_to_fill

    signal = apply_rolling_median(signal, rolling_median)
    return signal


def interpolation_standard(signal, rolling_median):
    signal = apply_rolling_median(signal, rolling_median)
    return signal.interpolate(method="linear", limit_area="inside")


def interpolation_with_resampling(signal, rolling_median):
    signal = apply_rolling_median(signal, rolling_median)
    return signal.resample("15min").bfill().interpolate(limit_area="inside")


def apply_rolling_median(signal, rolling_median):
    if rolling_median > 1:
        signal = signal.rolling(rolling_median, center=True).median()
    return signal


def mask_isolated_values(signal):
    mask = ~signal.iloc[:, 0].isna()
    eroded_mask = np.convolve(mask.values, [1, 1, 1]) >= 3
    dilation = np.convolve(eroded_mask, [1, 1, 1]) > 1
    filtered_signal = signal.copy()
    filtered_signal.loc[~dilation[2:-2]] = np.nan
    return filtered_signal


def obstruction_index(signal, kernel_length):
    mask = ~signal.iloc[:, 0].isna()
    quality_index = np.convolve(mask.values, [1] * (kernel_length + 1))
    return quality_index[:-kernel_length]


def get_last_valid_index(signal, quality_index):
    (r,) = np.where(obstruction_index(signal, quality_index) >= ceil(quality_index * 0.2))
    try:
        return signal.iloc[r[-1]].name
    except IndexError:
        try:
            return signal.loc[~signal.iloc[:, 0].isna()].iloc[-1].name
        except IndexError:
            return None  # every point is nan
