"""
  Copyright (c) 2020-2021  AIMsight SA
  Written By Olivier Chabloz
"""
from dash import Dash, callback_context, no_update
from dash.exceptions import PreventUpdate
from dash.dependencies import Input, State, Output
from .layouts.PatternCreationLayout import layout
from .Dash_fun import apply_layout_for_admin
from .queries import sensor_process_from_ref, build_sensor_list
from app.base.models import Device
from flask import current_app

import plotly.express as px
import dash_bootstrap_components as dbc
from datetime import date, datetime, timedelta
import io, logging, numpy as np
from app.extensions import AIMsightDB
from app.defines import PATTERN_TYPES_COLORS
from sqlalchemy.orm import exc
from sqlalchemy import Date
from PIL import Image
from base64 import b64encode
import pandas as pd
from aimsight_optical.utils import blend_images, np_to_png
from aimsight_optical.imagePatternMono import ImagePatternMono
from aimsight_optical.pattern_converter import load_pattern_collection_from_acquisition_unit

logger = logging.getLogger()

url_base = "/dash/pattern_creator/"


# Helper function to add alpha channel to a color
def color_add_alpha(c, alpha=0.4):
    return f"rgba({int(c[1:3], base=16)},{int(c[3:5], base=16)},{int(c[5:8], base=16)},{alpha})"


# Helper function to map a number to a color
def number_to_color(number):
    annotation_colormap = px.colors.qualitative.Light24
    return annotation_colormap[number % len(annotation_colormap)]


def draw_annotated_figure(im, table, selected_rows, x_range=None, y_range=None):
    # Create a figure using plotly express
    fig = px.imshow(im, binary_string=True, binary_backend="jpg")

    # Create a list of dictionaries containing the annotation shapes
    shapes = [
        {
            "editable": True,
            "type": "rect",
            "x0": x["corners"][0][0],
            "y0": x["corners"][0][1],
            "x1": x["corners"][1][0],
            "y1": x["corners"][1][1],
            "line": {
                "color": number_to_color(i),
                "width": 3,
                "dash": "solid",
            },
            # Set the fill color to the corresponding color from the colormap
            # if the row is selected, otherwise use transparent fill
            "fillcolor": color_add_alpha(number_to_color(i)) if i in selected_rows else "rgba(0,0,0,0)",
        }
        for i, x in enumerate(table)
    ]

    # Create a list of dictionaries containing the annotations
    annotations = [
        {
            "text": x["sensor_name"],
            "x": x["corners"][0][0],
            "y": x["corners"][0][1],
            "font": {
                "family": "Courier New, monospace",
                "size": 16,
                "color": "#ffffff",
            },
            "showarrow": False,
            "xanchor": "left",
            "yanchor": "bottom",
            # Set the background color to the corresponding color from the colormap
            "bgcolor": number_to_color(i),
            "opacity": 0.8,
        }
        for i, x in enumerate(table)
    ]

    # Update the figure layout with the shapes and annotations
    fig.update_layout(
        shapes=shapes,
        annotations=annotations,
        newshape_line_color=number_to_color(len(table)),
        # reduce space between image and graph edges
        margin={"l": 0, "r": 0, "b": 0, "t": 0, "pad": 4},
        dragmode="drawrect",
    )

    # Update the x and y ranges if specified
    if x_range is not None:
        fig.update_xaxes(range=x_range)
    if y_range is not None:
        fig.update_yaxes(range=y_range)

    return fig


def get_closest_iron(measurement, time_utc):
    m_type = AIMsightDB.get_measurement_type("OPTICAL_SHIFT-IRON")
    q = AIMsightDB.measurements.filter_by(
        measurement_type=m_type, acquisition_unit=measurement.acquisition_unit
    )
    q = q.filter(AIMsightDB.T.Measurement.time_utc <= time_utc)
    q = q.order_by(AIMsightDB.T.Measurement.time_utc.desc())
    return q.first()


def create_sensor(sensor_name, acquisition_unit):
    try:
        channel = int(sensor_name.split(".")[-1])
    except ValueError:
        channel = 0
    sensor = acquisition_unit.new_sensor(sensor_name, channel)
    logger.info("Created new sensor: %s", sensor)
    return sensor


def Add_Dash(server):
    app = Dash(
        server=server,
        url_base_pathname=url_base,
        external_stylesheets=[
            dbc.themes.BOOTSTRAP,
            "/static/build/css/style.css",
        ],
    )
    apply_layout_for_admin(app, layout)

    @app.callback(
        [
            Output("graph", "figure"),
            Output("quantile_slider-value", "children"),
            Output("btn-download", "href"),
            Output("btn-download", "disabled"),
        ],
        [
            Input("explore_location", "pathname"),
            Input("timeSelector", "value"),
            Input("quantile_slider", "value"),
            Input("btn-refresh", "n_clicks"),
        ],
        [
            State("pattern_table", "data"),
            State("imIRON", "value"),
        ],
    )
    def load_image(pathname, measurement_id, quantile_value, refresh, data_table, im_iron):
        im_iron = "IRON" in (im_iron or [])
        if measurement_id is None:
            logger.info("Measurement_id is None")
            return (no_update, no_update, no_update, no_update)
        measurement = AIMsightDB.get_measurement(measurement_id)

        output_quantile_value = "Slider Value: {:0.2E}".format(10 ** (-quantile_value - 2))
        quantile_value = 1 - (10 ** (-quantile_value - 2))
        fg_im = Image.open(io.BytesIO(measurement.data))
        if not im_iron:
            bg_meas = get_closest_iron(measurement, datetime.now())
            bg_im = Image.open(io.BytesIO(bg_meas.data))
            blended_image = blend_images(bg_im, fg_im, quantile_value)
            logger.info("PatternCreator: %d Background image date: %s", measurement_id, bg_meas.time_utc)
        else:
            blended_image = fg_im

        # fig = draw_annotated_figure(blended_image, data_table, [])

        fig = px.imshow(np.array(blended_image), color_continuous_scale="gray")
        logger.info("drawing %d patterns on the image", len(data_table))
        for item in data_table:
            color = PATTERN_TYPES_COLORS[item["pattern_type"]]
            s = item["pattern_size"] // 2
            if item["x"] is None or item["y"] is None:
                logger.info("Pattern id %d, %s cannot be drawn", item["sensor_id"], item["sensor_name"])
                continue
            fig.add_shape(
                editable=True,
                line_color=color,
                x0=item["x"] - s,
                x1=item["x"] + s,
                y0=item["y"] - s,
                y1=item["y"] + s,
                xref="x",
                yref="y",
            )
            if len(item["sensor_name"]) == 0:
                continue
            fig.add_annotation(
                x=item["x"],
                y=item["y"] - s - 10,
                text=item["sensor_name"],
                xref="x",
                yref="y",
                showarrow=False,
                font_size=15,
                font_color=color,
            )
        fig.update_layout(uirevision=str(measurement_id))
        return (fig, output_quantile_value, f"/image/{measurement_id}", False)

    @app.callback(
        [
            Output("date-selector", "date"),
            # Output("date-selector", "disabled_days"),
            Output("date-selector", "min_date_allowed"),
            Output("date-selector", "max_date_allowed"),
        ],
        [Input("explore_location", "pathname")],
    )
    def load_default_date(pathname):
        dev = Device.get_from_pathname(pathname)
        # Define end_date as today if dev.end_date is None
        end_date = dev.end_date or datetime.now()

        t = AIMsightDB.T.Measurement.time_utc.cast(Date)
        q = AIMsightDB.session.query(t).filter_by(acquisition_unit_id=dev.serial_number)
        q = q.filter(AIMsightDB.T.Measurement.time_utc >= dev.start_date)
        if not end_date is None:
            q = q.filter(AIMsightDB.T.Measurement.time_utc <= end_date)
        q = q.distinct().order_by(t.asc())
        q = pd.read_sql(q.statement, q.session.bind)
        valid_dates = q.iloc[:, 0].to_numpy()
        all_dates = pd.date_range(start=dev.start_date, end=end_date).date
        last_day = valid_dates[-1]
        # TODO: Return invalid dates as disabled_days attribute
        invalid_dates = list(all_dates[~np.in1d(all_dates, valid_dates)])
        return last_day, dev.start_date.date(), last_day

    @app.callback(
        [Output("timeSelector", "options"), Output("timeSelector", "value")],
        [
            Input("explore_location", "pathname"),
            Input("date-selector", "date"),
            Input("imIRON", "value"),
        ],
    )
    def load_hour_dropdown(pathname, date_selected, im_iron):
        im_iron = "IRON" in (im_iron or [])
        date_selected = datetime.strptime(date_selected, "%Y-%m-%d").date()
        dev = Device.get_from_pathname(pathname)
        if im_iron:
            m_type = AIMsightDB.get_measurement_type("OPTICAL_SHIFT-IRON")
        else:
            m_type = AIMsightDB.get_measurement_type("OPTICAL_SHIFT-DIFF")
        node = AIMsightDB.nodes.filter_by(id=dev.serial_number).one()
        table = AIMsightDB.session.query(AIMsightDB.T.Measurement.id, AIMsightDB.T.Measurement.time_utc)
        q = table.filter_by(acquisition_unit_id=node.id).filter_by(measurement_type=m_type)

        q = q.filter(AIMsightDB.T.Measurement.time_utc >= dev.start_date)
        q = q.filter(AIMsightDB.T.Measurement.time_utc >= date_selected)
        q = q.filter(AIMsightDB.T.Measurement.time_utc <= (date_selected + timedelta(days=1)))
        q = q.order_by(AIMsightDB.T.Measurement.time_utc.asc())

        options = [{"value": v.id, "label": datetime.strftime(v.time_utc, "%H:%M:%S")} for v in q]
        return options, options[0]["value"] if len(options) > 0 else no_update

    @app.callback(Output("pattern_table", "row_selectable"), Input("imSelectOptions", "value"))
    def select_mode(create_edit):
        edit_mode = "EDIT" in create_edit
        return "single" if edit_mode else "multi"

    @app.callback(
        Output("progress_container", "children"),
        Input("progress_update_interval", "n_intervals"),
        State("explore_location", "pathname"),
        State("progress_container", "children"),
    )
    def update_processing_progress(n, pathname, actual_content):
        dev = Device.get_from_pathname(pathname)
        task_name = "app.tasks.async_tasks.process_sensor_from_ref"
        ccelery = current_app.celery_handle
        i = ccelery.control.inspect()
        active = i.active()
        if active is None:
            raise PreventUpdate
        active_tasks = [x for y in active.values() for x in y]
        progress_list = []
        for active_task in active_tasks:
            if active_task["name"] == task_name:
                if active_task["args"][0] == int(dev.serial_number):
                    task = ccelery.AsyncResult(active_task["id"])
                    current, total = task.info.get("current"), task.info.get("total")
                    progress_list.append(dbc.Progress(value=current, max=total, label=f"{current}/{total}"))
        if len(progress_list) == 0 and len(actual_content) == 0:
            raise PreventUpdate
        return progress_list

    @app.callback(
        Output("download-export", "data"),
        Input("btn-export", "n_clicks"),
        State("explore_location", "pathname"),
        prevent_initial_call=True,
    )
    def download_exported_patterns(nclic, pathname):
        dev = Device.get_from_pathname(pathname)
        acq_unit = AIMsightDB.nodes.filter_by(id=dev.serial_number).one().acquisition_unit
        pc = load_pattern_collection_from_acquisition_unit(
            acq_unit, use_names=True, start_limit=dev.start_date
        )
        serialized_patterns = pc.serialize()
        return dict(content=serialized_patterns, filename=f"{acq_unit.serial_number}_patterns.json")

    @app.callback(
        [Output("pattern_table", "data"), Output("save_status", "children")],
        [
            Input("graph", "clickData"),
            Input("explore_location", "pathname"),
            Input("btn-save_parm", "n_clicks"),
            Input("btn-save_meta", "n_clicks"),
            Input("btn-delete", "n_clicks"),
            Input("btn-archive", "n_clicks"),
        ],
        [
            State("pattern_table", "data"),
            State("imSelectOptions", "value"),
            State("pattern_table", "selected_rows"),
            State("timeSelector", "value"),
        ],
    )
    def table_interaction(
        click_data,
        pathname,
        save_btn,
        meta_btn,
        del_btn,
        arc_btn,
        content,
        create_edit,
        selected_rows,
        measurement_id,
    ):
        edit_mode = "EDIT" in create_edit
        dev = Device.get_from_pathname(pathname)

        changed_id = [p["prop_id"] for p in callback_context.triggered][0]
        if "btn-save_parm" in changed_id:
            return save_patterns(dev, measurement_id, content, selected_rows)
        elif "btn-save_meta" in changed_id:
            return save_metadata(dev, measurement_id, content, selected_rows=selected_rows)
        elif "btn-delete" in changed_id:
            return delete_sensor(dev, content, selected_rows)
        elif "btn-archive" in changed_id:
            return delete_sensor(dev, content, selected_rows, archive_only=True)

        elif "explore_location" in changed_id:
            sensor_table = build_sensor_list(dev.serial_number, start_limit=dev.start_date)
            for item in sensor_table:
                item["start_date"] = item["start_date"].strftime("%d.%m.%Y %H:%M:%S")
                if "corners" in item:
                    corners = item["corners"]
                    x, y = (corners[0][0] + corners[1][0]) / 2, (corners[0][1] + corners[1][1]) / 2
                    item["x"], item["y"] = x, y

            return (sensor_table, "")

        # Click Data
        x = click_data["points"][-1]["x"]
        y = click_data["points"][-1]["y"]
        if edit_mode:
            try:
                row = selected_rows[0]
            except TypeError:
                return (no_update, "")
            content[row]["x"] = x
            content[row]["y"] = y
        else:
            content.append(
                {
                    "shape_pos": len(content),
                    "sensor_name": "",
                    "pattern_type": "target",
                    "pattern_size": 20,
                    "error_threshold": 0.7,
                    "x": x,
                    "y": y,
                }
            )
        return (content, "")

    def save_patterns(dev, measurement_id, data_table, selected_rows):
        if measurement_id is None:
            return (no_update, "Please select an image first")
        if selected_rows is None:
            return (no_update, "No rows selected")

        measurement = AIMsightDB.get_measurement(measurement_id)
        acq_unit = AIMsightDB.nodes.filter_by(id=dev.serial_number).one().acquisition_unit
        assert acq_unit == measurement.acquisition_unit
        time_utc_table = AIMsightDB.T.Measurement.time_utc
        q = AIMsightDB.measurements.filter_by(
            acquisition_unit=acq_unit,
            measurement_type=measurement.measurement_type,
        )
        q = q.filter(time_utc_table >= measurement.time_utc).order_by(time_utc_table.asc())
        q = q.limit(1)

        images = [np.array(Image.open(io.BytesIO(i.data))) for i in q]
        image_stack = np.sum(images, axis=0, dtype=np.uint32)
        logger.info("image stack shape: %s", image_stack.shape)
        if len(image_stack.shape) == 3:
            image_stack = np.sum(image_stack, axis=2, dtype=np.uint32)
            logger.info("image stack reshaped: %s", image_stack.shape)

        for i, pattern in enumerate(data_table):
            """
            1. get the image or stack off diff
            2. crop the pattern
            3. convert the thing as b64 png 16bits
            4. get the related sensor, create it if not exist
            5. push the whole json in it

            6. when loading the data_table, extract only the data to display
            """
            if not i in selected_rows:
                continue
            s = pattern["pattern_size"] // 2
            corners = [[pattern["x"] - s, pattern["y"] - s], [pattern["x"] + s, pattern["y"] + s]]

            p = ImagePatternMono.from_image(
                image_stack,
                corners,
                pattern_type=pattern["pattern_type"],
                measurement_id=measurement_id,
                error_threshold=pattern["error_threshold"],
                pattern_size=pattern["pattern_size"],
            )
            parameters = p.to_dict()

            sensor_id = pattern.get("sensor_id")
            if sensor_id is None:
                sensor = create_sensor(pattern["sensor_name"], acq_unit)
                pattern["sensor_id"] = sensor.id
            else:
                sensor = AIMsightDB.sensors.filter_by(id=sensor_id).one()
            # we need to do:
            #   1. get the last valid processing chain
            #   2. query the last N valid processed datapoint. average it
            #   3. add result to processing chain
            prev_proc_chain = sensor.get_active_processing_chain(measurement.time_utc)
            if prev_proc_chain is not None:
                prev_error = prev_proc_chain.parameters["error_threshold"]
                q = sensor.shift_sensor.filter(AIMsightDB.T.Shift_sensor.error_factor <= prev_error)
                q = q.filter(AIMsightDB.T.Shift_sensor.time_utc < measurement.time_utc)
                q = q.order_by(AIMsightDB.T.Shift_sensor.time_utc.desc()).limit(25)
                df = pd.read_sql(q.statement, q.session.bind)
                mean = df.mean()
                if not np.isnan(mean.horizontal_shift):
                    parameters["x"] = mean.horizontal_shift
                    parameters["y"] = mean.vertical_shift

            sensor.new_processing_chain("optical_shift", "1.8.0", parameters, start_date=measurement.time_utc)
            sensor.name = pattern["sensor_name"]

        AIMsightDB.commit_session()

        sensorsProcess = []
        for i, pattern in enumerate(data_table):
            if not i in selected_rows:
                continue
            sensor_id = pattern.get("sensor_id")
            if sensor_id is None:
                continue
            else:
                sensor = AIMsightDB.sensors.filter_by(id=sensor_id).one()
                sensorsProcess.append(sensor.id)

        proc = current_app.celery_handle.tasks["app.tasks.async_tasks.process_sensor_from_ref"]

        if len(sensorsProcess) > 0:
            proc.delay(acq_unit.id, sensorsProcess)
            # app.server.tasks.add_task({"task": sensor_process_from_ref, "sensors": sensorsProcess})
        AIMsightDB.commit_session()

        now = datetime.now().strftime("%H:%M:%S")
        return (data_table, f"Sensors parameters saved at {now}")

    def save_metadata(dev, measurement_id, data_table, selected_rows):
        if selected_rows is None:
            return (no_update, "No rows selected")

        for i, pattern in enumerate(data_table):
            if not i in selected_rows:
                continue
            active_date = datetime.strptime(pattern["start_date"], "%d.%m.%Y %H:%M:%S") + timedelta(seconds=1)
            parameters = {
                "pattern_type": pattern["pattern_type"],
                "error_threshold": pattern["error_threshold"],
            }
            try:
                acq_unit = AIMsightDB.nodes.filter_by(id=dev.serial_number).one().acquisition_unit
            except exc.NoResultFound:
                return ("Device not found",)
            sensor_id = pattern.get("sensor_id")
            if sensor_id is None:
                sensor = create_sensor(pattern["sensor_name"], acq_unit)
                pattern["sensor_id"] = sensor.id
                sensor.new_processing_chain("optical_shift", "1.4.0", parameters)
            else:
                sensor = AIMsightDB.sensors.filter_by(id=sensor_id).one()
                proc_chain = sensor.get_active_processing_chain(active_date)
                proc_chain.parameters["pattern_type"] = pattern["pattern_type"]
                proc_chain.parameters["error_threshold"] = pattern["error_threshold"]

            sensor.name = pattern["sensor_name"]
        AIMsightDB.commit_session()

        now = datetime.now().strftime("%H:%M:%S")
        return (no_update, f"Sensors metadata saved at {now}")

    def delete_sensor(dev, data_table, selected_rows, archive_only=False):
        if selected_rows is None:
            return (no_update, "No rows selected")

        deleted_list = []
        to_delete = []
        # or
        not_delete = []

        for i, row in enumerate(data_table):
            if i in selected_rows:
                to_delete.append(row)
            else:
                not_delete.append(row)

        for pattern in to_delete:
            sensor_id = pattern.get("sensor_id")
            if sensor_id is None:
                continue  # * Row was never recorded to the DB. Just remove from the data_table
            sensor = AIMsightDB.sensors.filter_by(id=sensor_id).one()
            if archive_only:
                pc = sensor.get_active_processing_chain()
                pc.parameters["archive"] = True
            else:
                AIMsightDB.sensors.filter_by(id=sensor.id).delete()
                AIMsightDB.nodes.filter_by(id=sensor.id).delete()
            deleted_list.append(pattern["sensor_name"])

        AIMsightDB.commit_session()

        now = datetime.now().strftime("%H:%M:%S")
        process_type = "archived" if archive_only else "deleted"
        return (not_delete, f"Sensors {','.join(deleted_list)} {process_type} at {now}")

    return app.server
