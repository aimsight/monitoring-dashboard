"""
  Copyright (c) 2020-2021  AIMsight SA
  Written By Olivier Chabloz
"""
from flask import send_file, current_app
from dash import Dash
from dash.dependencies import Input, State, Output, ClientsideFunction
from dash import CeleryManager
from .Dash_fun import apply_layout_for_admin
from .queries import build_sensor_list, get_sensors_data
from .processing import process_Sim_matrices as process_matrices
from app.base.auth import requires_admin
import plotly.express as px
import plotly.graph_objects as go
import dash_bootstrap_components as dbc
from dash import dcc, html
from .layouts.ExploreEditLayout import layout, options_checkbox_values, output_dropdown_template
from datetime import date, timedelta, datetime
import pandas as pd
import io, pickle, logging
from uuid import uuid4
from app.base.models import Device

logger = logging.getLogger()

url_base = "/dash/explore_edit/"


def Add_Dash(server):
    cache_uuid = uuid4()
    celery_manager = None
    if hasattr(server, "celery_handle"):
        celery_manager = CeleryManager(server.celery_handle, cache_by=[lambda: cache_uuid], expire=600)
    app = Dash(
        server=server,
        url_base_pathname=url_base,
        background_callback_manager=celery_manager,
        external_stylesheets=[
            dbc.themes.BOOTSTRAP,
            "/static/build/css/style.css",
        ],
        external_scripts=[
            "/static/build/js/explore.js",
        ],
    )
    apply_layout_for_admin(app, layout)

    @app.callback(
        [
            Output("options_checkbox", "value"),
            Output("sensors-date-range", "min_date_allowed"),
            Output("sensors-date-range", "start_date"),
            Output("sensors-date-range", "end_date"),
            Output("output_table", "dropdown"),
        ],
        [Input("explore_location", "pathname")],
    )
    def load_device(pathname):
        """
        Endpoint that load the entire page (except the plots)
        """
        dev = Device.get_from_pathname(pathname)
        sensor_table = build_sensor_list(
            dev.serial_number, {"error_threshold": 0.7}, load_archived=True, start_limit=dev.start_date
        )
        end_date = datetime.now()
        start_date = end_date - timedelta(days=31)
        start_date = start_date if dev.start_date < start_date else dev.start_date

        sensor_table.sort(key=lambda x: x["sensor_name"])
        options = [v for v in options_checkbox_values if getattr(dev, v, False)]
        dropdown = output_dropdown_template({s["sensor_name"]: s["sensor_id"] for s in sensor_table})

        return (options, dev.start_date, start_date, end_date.date(), dropdown)

    @app.callback(
        [
            Output("notification-alert", "children"),
            Output("notification-alert", "color"),
            Output("notification-alert", "is_open"),
            Output("notification-alert", "duration"),
        ],
        [Input("output_table", "data")],
        [State("explore_location", "pathname")],
    )
    def save_output_table(output_table, pathname):
        """
        Endpoint that saves the Output Table.
        Reflect the output table into the database

        ! there must be a better way
        ! Maybe add a hidden outpout_id and sync based on that ?
        ! Also, the mechanics should be in the model
        """
        dev = Device.get_from_pathname(pathname)
        output_len = len(output_table)
        for i, row in dev.outputs.items():
            if i >= output_len:
                row.delete()
                dev.db_commit()
        outputs = dev.outputs
        for i, row in enumerate(output_table):
            if i not in outputs:
                o = dev.new_output()
                o.from_dict(row)
            else:
                outputs[i].from_dict(row)
        dev.db_commit()
        return ("Output signals saved", "success", False, 5000)

    @app.callback(
        Output("output_table", "data"),
        [Input("addRow-Button", "n_clicks"), Input("explore_location", "pathname")],
        [State("output_table", "data")],
    )
    def add_row(n_clicks, pathname, rows):
        """
        Add a new row to the Output Table
        """
        rows = list() if rows is None else rows
        if n_clicks > 0:
            rows.append(
                {
                    "name": None,
                    "process_type": None,
                    "sensor1": None,
                    "sensor2": None,
                    "roll_med": 1,
                    "interp": "standard",
                    "scale": 1,
                    "vertical_offset": 0,
                    "horizontal_offset": 0,
                    "Halarm_threshold": None,
                    "Valarm_threshold": None,
                }
            )
            return rows
        dev = Device.get_from_pathname(pathname)
        return [v.as_dict() for k, v in dev.outputs.items()]

    @app.callback(
        Output("option_status", "children"),
        [Input("explore_location", "pathname"), Input("options_checkbox", "value")],
    )
    def save_options(pathname, options):
        """
        Endpoint that saves options checkboxes (such as "show horizontal plot", "use thresholds", etc)
        """
        dev = Device.get_from_pathname(pathname)
        for item in options_checkbox_values:
            setattr(dev, item, item in options)
        dev.db_commit()
        return ""

    @app.callback(
        Output("clientside-figure-store-sensors", "data"),
        Output("select_graph", "options"),
        Output("select_graph", "value"),
        [
            Input("sensors-date-range", "start_date"),
            Input("sensors-date-range", "end_date"),
            Input("explore_location", "pathname"),
            Input("tabs-example", "value"),
        ],
        background=True,
    )
    def update_figure(start_date, end_date, pathname, tab):
        with app.server.app_context():
            if tab == "tab-raw_data":
                return load_raw_plots(start_date, end_date, pathname)
            if tab == "tab-statistics":
                return load_stats_plots(start_date, end_date, pathname)

    def load_stats_plots(start_date, end_date, pathname):
        dev = Device.get_from_pathname(pathname)
        default_params = {
            "error_threshold": 0.7,
            "pattern_type": "target",
            "measurement_id": dev.reference_measurement,
        }
        sensors_params = pd.DataFrame(
            build_sensor_list(dev.serial_number, default_params, start_limit=dev.start_date)
        )

        shifts = get_sensors_data(sensors_params, start_date, end_date, threshold=False)
        sn_list = list(shifts["sensor_name"].sort_values().unique())
        figs = {}
        for label in ["vertical_shift", "horizontal_shift"]:
            figs[label] = px.scatter(
                shifts,
                x="error_value",
                y=label,
                color="sensor_name",
                hover_name="sensor_name",
                hover_data=["time_utc"],
                category_orders={"sensor_name": sn_list},
            )
        options = [
            {"label": "Relative Horizontal Shift", "value": "horizontal_shift"},
            {"label": "Relative Vertical Shift", "value": "vertical_shift"},
        ]
        default_value = "vertical_shift"
        return figs, options, default_value

    def load_raw_plots(start_date, end_date, pathname):
        """
        Endpoint that loads the entire device data and displays it as a plot
        """
        dev = Device.get_from_pathname(pathname)
        default_params = {
            "error_threshold": 0.7,
            "pattern_type": "target",
            "measurement_id": dev.reference_measurement,
        }
        sensors_params = pd.DataFrame(
            build_sensor_list(dev.serial_number, default_params, start_limit=dev.start_date)
        )

        shifts = get_sensors_data(sensors_params, start_date, end_date, threshold=False)
        matrices = process_matrices(shifts)

        mat_fig = go.Figure()
        if len(matrices) > 0:
            m = matrices.reset_index()

            mat_fig.add_trace(go.Scatter(x=m["time_utc"], y=m["rotation"], mode="lines", name="rotation"))
            mat_fig.add_trace(go.Scatter(x=m["time_utc"], y=m["scale"], mode="lines", name="scale"))
            mat_fig.add_trace(go.Scatter(x=m["time_utc"], y=m["tx"], mode="lines", name="x"))
            mat_fig.add_trace(go.Scatter(x=m["time_utc"], y=m["ty"], mode="lines", name="y"))

        figs = {"matrix": mat_fig}
        sn_list = list(shifts["sensor_name"].sort_values().unique())
        for i in [
            "error_value",
            "horizontal_shift",
            "vertical_shift",
            "x",
            "y",
            "brightness_median",
            "brightness_p99",
        ]:
            if len(shifts) == 0:
                fig = go.Figure()
            else:
                fig = px.scatter(
                    shifts,
                    x="time_utc",
                    y=i,
                    color="sensor_name",
                    hover_name="sensor_name",
                    category_orders={"sensor_name": sn_list},
                )
                fig.update_traces(mode="lines+markers")
            figs[i] = fig
            # added the following log to have a grasp of the actual time needed to build a plot
            logger.info("(ExploreEdit) %s: plot %s complete", pathname, i)

        options = [
            {"label": "Error value", "value": "error_value"},
            {"label": "X [px]", "value": "x"},
            {"label": "Y [px]", "value": "y"},
            {"label": "Relative Horizontal Shift", "value": "horizontal_shift"},
            {"label": "Relative Vertical Shift", "value": "vertical_shift"},
            {"label": "Brightness median ratio", "value": "brightness_median"},
            {"label": "Brightness p99 ratio", "value": "brightness_p99"},
            {"label": "references", "value": "matrix"},
        ]
        logger.info("(ExploreEdit) %s: query completed with %s datapoints", pathname, len(shifts))
        default_value = "error_value"
        return figs, options, default_value

    app.clientside_callback(
        ClientsideFunction(namespace="clientside", function_name="select_y_data"),
        Output("graph_sensors", "figure"),
        [Input("clientside-figure-store-sensors", "data"), Input("select_graph", "value")],
    )

    @app.server.route(url_base + "full_download/<device>/<ext>")
    @requires_admin
    def full_download(device, ext):
        """
        Endpoint that sends a pickle with the entire content of the device
        """
        mime = {
            "csv": "text/csv",
            "xlsx": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "pickle": "application/octet-stream",
        }
        dev = Device.get_from_pathname(device)
        default_params = {
            "error_threshold": 0.7,
            "pattern_type": "target",
            "measurement_id": dev.reference_measurement,
        }
        sensors_params = pd.DataFrame(
            build_sensor_list(dev.serial_number, default_params, start_limit=dev.start_date)
        )

        shifts = get_sensors_data(sensors_params, dev.start_date, end_date=None, threshold=False)
        fp = io.BytesIO(pickle.dumps(shifts))
        return send_file(
            fp,
            max_age=60,
            mimetype=mime[ext],
            download_name=f"{dev.name}_from_{dev.start_date}.{ext}",
            as_attachment=True,
        )

    return app.server
