"""
  Copyright (c) 2020-2021  AIMsight SA
  Written By Olivier Chabloz
"""
import logging
import datetime
import json, pytz
import pandas as pd
from dash import Dash, callback_context, dcc, html, no_update, MATCH, ALL
from dash.exceptions import PreventUpdate
from dash.dependencies import Input, Output, ClientsideFunction, State
import dash_bootstrap_components as dbc
import numpy as np
from plotly.subplots import make_subplots
import plotly.graph_objects as go
import sqlalchemy
from sqlalchemy.exc import NoResultFound

from app.base.models import DeviceOutput, Alarm, Device, AlarmEvent
from app.base.auth import is_authenticated
from .Dash_fun import (
    apply_layout_with_auth,
    query_and_process_outputs,
    load_alarm,
    load_parent_alarm_from_model,
    load_parent_alarm,
)
from .queries import build_sensor_list, get_sensors_data, get_sensors_from_device
from .layouts.AlarmManagerLayout import (
    layout_base,
    layout_summary,
    layout_new_edit_alarm,
    layout_history,
    load_alarms_table_data,
)

logger = logging.getLogger()

url_base = "/dash/alarm_manager/"


def draw_alarm_fig(alarm_data, min_, max_):
    alarm_data_plot = alarm_data.copy()
    alarm_data_plot.loc[alarm_data_plot["out_of_bound"], "out_of_bound"] = alarm_data_plot.value
    alarm_data_plot.loc[alarm_data_plot["out_of_bound"] == False, "out_of_bound"] = np.nan
    alarm_data_plot.loc[alarm_data_plot["trigger"], "trigger"] = alarm_data_plot.value
    alarm_data_plot.loc[alarm_data_plot["trigger"] == False, "trigger"] = np.nan

    sub_plots = {y: x + 1 for x, y in enumerate(alarm_data_plot.signal_value_type.unique())}
    subplot_titles = tuple(alarm_data_plot.signal_value_type.unique())
    fig = make_subplots(
        rows=len(sub_plots), subplot_titles=subplot_titles, shared_xaxes=True, vertical_spacing=0.1
    )

    # Add traces
    def draw_alarm_single(df):
        row = sub_plots[df.name[1]]
        label = df.iloc[0].label
        fig.add_trace(go.Scatter(x=df.time_utc, y=df.value.values, mode="lines", name=label), row=row, col=1)
        fig.add_trace(
            go.Scatter(
                x=df.time_utc,
                y=df.out_of_bound.values,
                mode="markers",
                text=df.sensor_name.values,
                name=label,
                marker=dict(size=5, color="Silver"),
                showlegend=False,
            ),
            row=row,
            col=1,
        )

        fig.add_trace(
            go.Scatter(
                x=df.time_utc,
                y=df.trigger.values,
                mode="markers",
                text=df.sensor_name.values,
                name=label,
                showlegend=False,
                marker=dict(size=8, color="Tomato"),
            ),
            row=row,
            col=1,
        )

    alarm_data_plot.groupby(["alarm_id", "signal_value_type"]).apply(draw_alarm_single)

    fig.add_hrect(
        y0=min_,
        y1=max_,
        fillcolor="rgb(218, 247, 166)",
        layer="below",
        line_width=0,
        opacity=1,
    )
    height = 100 + (350 * len(sub_plots))
    fig.update_layout(height=height, plot_bgcolor="LightPink")

    return fig


def Add_Dash(server):
    app = Dash(
        server=server,
        url_base_pathname=url_base,
        external_stylesheets=[
            dbc.themes.BOOTSTRAP,
            "/static/build/css/style.css",
        ],
        external_scripts=[
            "/static/build/js/iframeResizer.contentWindow.min.js",
            "/static/build/js/explore.js",
        ],
    )
    apply_layout_with_auth(app, layout_base)

    @app.callback(
        [Output("dash_alarm_content", "children")],
        [Input("explore_location", "pathname")],
    )
    def content_manager(pathname):
        dev = Device.get_from_pathname(pathname)
        p = pathname.split("/")
        if p[-2] == "alarm_manager":
            return (layout_summary(dev),)
        elif p[-2] == "new_alarm":
            return (layout_new_edit_alarm(dev),)
        else:
            try:
                alarm_id = int(p[-2])
            except ValueError:
                raise PreventUpdate
            else:
                alarm = Alarm.query.filter_by(id=alarm_id).one()
                if p[-3] == "history":
                    return (layout_history(dev, alarm),)
                return (layout_new_edit_alarm(dev, alarm),)

    @app.callback(
        Output("explore_location", "pathname"),
        [Input("edit_trigger", "data"), Input("summary_trigger", "data")],
    )
    def manage_pathname(edit_trigger, summary_trigger):
        trig = callback_context.triggered_id
        if trig == "edit_trigger" and "device" in edit_trigger:
            action, row_id, dev = edit_trigger["action"], edit_trigger.get("row_id"), edit_trigger["device"]
            if not row_id is None:
                return f"{url_base}/{action}/{row_id}/{dev}"
            return f"{url_base}/{action}/{dev}"
        elif trig == "summary_trigger" and "device" in summary_trigger:
            return f"{url_base}{summary_trigger['device']}"
        raise PreventUpdate

    @app.callback(
        [Output("edit_trigger", "data")],
        [
            Input({"type": "edit_parent_alarm_button", "index": ALL}, "n_clicks"),
            Input({"type": "edit_alarm_button", "index": ALL}, "n_clicks"),
            Input({"type": "alarm_list", "index": ALL}, "selected_row_ids"),
            Input({"type": "history_alarm_button", "index": ALL}, "n_clicks"),
            Input("new_alarm_button", "n_clicks"),
        ],
        [State("alarm_container", "active_item"), State("explore_location", "pathname")],
        prevent_initial_call=True,
    )
    def trigger_edit(p_edit, edit, sel_row_id, hist, new_alarm, active_item, pathname):
        dev = Device.get_from_pathname(pathname)
        if callback_context.triggered_id == "new_alarm_button":
            action = "new_alarm"
            return ({"device": dev.name, "action": action},)
        index = active_item["index"]
        if callback_context.triggered_id["type"] == "edit_parent_alarm_button":
            _id = active_item["index"]
        elif callback_context.triggered_id["type"] in ["edit_alarm_button", "history_alarm_button"]:
            try:
                _id = callback_context.inputs[f'{{"index":{index},"type":"alarm_list"}}.selected_row_ids'][0]
            except (KeyError, TypeError) as exc:
                raise PreventUpdate from exc
        else:
            raise PreventUpdate

        if _id is None:
            raise PreventUpdate

        action, *_ = callback_context.triggered_id["type"].split("_")
        return ({"device": dev.name, "row_id": _id, "action": action},)

    @app.callback(
        [
            Output({"type": "alarm_list", "index": MATCH}, "data"),
            Output({"type": "alarm_list", "index": MATCH}, "selected_rows"),
        ],
        [
            Input({"type": "rearm_alarm_button", "index": MATCH}, "n_clicks"),
            Input({"type": "deactivate_alarm_button", "index": MATCH}, "n_clicks"),
            Input({"type": "delete_alarm_button", "index": MATCH}, "n_clicks"),
        ],
        [
            State({"type": "alarm_list", "index": MATCH}, "selected_row_ids"),
            State("explore_location", "pathname"),
        ],
        prevent_initial_call=True,
    )
    def rearm_alarm(clicks1, clicks2, clicks3, selected_id, pathname):
        dev = Device.get_from_pathname(pathname)
        row_deleted = False

        try:
            alarm = Alarm.query.filter_by(id=selected_id[0]).one()
        except NoResultFound:
            raise PreventUpdate

        parent_alarm = alarm.parent or alarm

        if callback_context.triggered_id["type"] == "rearm_alarm_button":
            if alarm.active:
                raise PreventUpdate
            if alarm.rearming_period == -1:
                alarm.update(active=True, rearming_period=None)
            else:
                alarm.update(active=True)
            alarm_event = alarm.get_last_event()
            if alarm_event is not None and alarm_event.status == "active":
                user = is_authenticated()
                alarm_event.status = json.dumps({"mode": "manual", "user_id": user["user_id"]})
                alarm_event.db_commit()

        elif callback_context.triggered_id["type"] == "deactivate_alarm_button":
            alarm.update(active=False, rearming_period=-1)
        elif callback_context.triggered_id["type"] == "delete_alarm_button":
            alarm.delete()
            row_deleted = True
        else:
            raise PreventUpdate

        try:
            data = load_alarms_table_data(parent_alarm.children)
        except sqlalchemy.orm.exc.DetachedInstanceError:
            data = []

        return data, [] if row_deleted else no_update

    @app.callback(
        Output("alarm_summary_graph", "figure"),
        [
            Input({"type": "alarm_list", "index": ALL}, "selected_row_ids"),
            Input("alarm_container", "active_item"),
        ],
        [State("explore_location", "pathname")],
    )
    def update_summary_graph(selected_rows_ids, active_item, pathname):
        dev = Device.get_from_pathname(pathname)
        try:
            trigged = list(callback_context.triggered_prop_ids.keys())[0]
        except IndexError:
            trigged = None
        shifts = pd.DataFrame()
        if (trigged == "alarm_container.active_item" or trigged is None) and isinstance(active_item, dict):
            index = active_item["index"]
            alarm = Alarm.query.filter_by(id=index).one()
            shifts = load_parent_alarm_from_model(dev, alarm)
        else:
            ids = callback_context.inputs[trigged]
            if ids is None:
                raise PreventUpdate
            alarm = Alarm.query.filter_by(id=ids[0]).one()
            if alarm.get_status() == "triggered":
                last_event = alarm.get_last_event()
                shifts = pd.read_json(last_event.description, convert_dates=["time_utc"])
            else:
                shifts = load_parent_alarm_from_model(dev, alarm)

        return draw_alarm_fig(shifts, shifts.min_.min(), shifts.max_.max())

    ######### * ALARM HISTORY CALLBACKS *   #########

    @app.callback(
        Output("alarm_history_graph", "figure"),
        Input("alarm_history_table", "selected_row_ids"),
        State("explore_location", "pathname"),
    )
    def display_alarm_event(selected_row_ids, pathname):
        ev = AlarmEvent.query.filter_by(id=selected_row_ids[0]).one()
        df = pd.read_json(ev.description, convert_dates=["time_utc"])
        return draw_alarm_fig(df, df.min_.min(), df.max_.max())

    ######### * NEW/EDIT ALARMS CALLBACKS * #########

    @app.callback(
        Output("sensor_id", "options"),
        [Input("signal_type", "value")],
        [State("explore_location", "pathname")],
    )
    def load_sensors(signal_type, pathname):
        dev = Device.get_from_pathname(pathname)
        if signal_type == "device_output":
            to_return = [{"label": v.name, "value": v.id} for k, v in dev.outputs.items()]
        elif signal_type == "sensor":
            sensor_table = build_sensor_list(
                dev.serial_number, load_archived=True, start_limit=dev.start_date
            )
            sensor_table.sort(key=lambda x: x["sensor_name"])
            to_return = [{"label": s["sensor_name"], "value": s["sensor_id"]} for s in sensor_table]
        else:
            raise PreventUpdate
        l = {"label": html.Span(["All"], style={"color": "Gold"}), "value": "all"}
        new_or_edit = pathname.split("/")[-2]
        if new_or_edit == "new_alarm":
            return [l] + to_return
        return to_return

    @app.callback(
        [Output("signal_value_type", "options")],
        [Input("sensor_id", "value"), Input("signal_type", "value")],
    )
    def load_sensors_value_type(sensor_id, signal_type):
        if sensor_id is None:
            raise PreventUpdate
        if signal_type == "device_output":
            return (
                [
                    {"label": "vertical", "value": "vertical_shift"},
                    {"label": "horizontal", "value": "horizontal_shift"},
                ],
            )

        elif signal_type == "sensor":
            return (
                [
                    {"label": "Error value", "value": "error_value"},
                    {"label": "X [px]", "value": "x"},
                    {"label": "Y [px]", "value": "y"},
                    {"label": "Relative Horizontal Shift", "value": "horizontal_shift"},
                    {"label": "Relative Vertical Shift", "value": "vertical_shift"},
                    {"label": "Brightness median ratio", "value": "brightness_median"},
                    {"label": "Brightness p99 ratio", "value": "brightness_p99"},
                ],
            )

        raise PreventUpdate

    @app.callback(
        [Output("alarm_graph", "figure"), Output("summary_trigger", "data"), Output("sensor_id", "value")],
        [
            Input("sensor_id", "value"),
            Input("signal_type", "value"),
            Input("signal_value_type", "value"),
            Input("alarm_label", "value"),
            Input("min_val", "value"),
            Input("max_val", "value"),
            Input("alarm_logic_type", "value"),
            Input("alarm_logic_val", "value"),
            Input("query_period_h", "value"),
            Input("rearming_time_h", "value"),
            Input("assigned_group", "value"),
            Input("save_button", "n_clicks"),
        ],
        [State("sensor_id", "options"), State("explore_location", "pathname")],
        prevent_initial_call=True,
    )
    def update_graph_and_save(*args):
        kwargs = dict(
            sensor_ids=args[0],
            signal_type=args[1],
            signal_value_type=args[2],
            alarm_label=args[3],
            min_=args[4] or 0,
            max_=args[5] or 0,
            logic_type=args[6],
            logic_val=args[7] or 1,
            query_period=args[8],
            rearming_period=args[9],
            group_id=args[10],
            path=args[-1],
        )
        options = args[12]

        try:
            kwargs["sensor_ids"] = [int(kwargs["sensor_ids"])]
        except TypeError:
            if kwargs["sensor_ids"] == ["all"]:
                values = [x["value"] for x in options if x["value"] != "all"]
                return (no_update, no_update, values)
        try:
            kwargs["sensor_ids"].remove("all")
        except (ValueError, AttributeError):
            pass

        if isinstance(kwargs["signal_value_type"], str):
            kwargs["signal_value_type"] = [kwargs["signal_value_type"]]

        new_or_edit = args[-1].split("/")[-2]
        if new_or_edit == "new_alarm":
            if kwargs["sensor_ids"] is None or len(kwargs["sensor_ids"]) == 0:
                raise PreventUpdate
            if kwargs["signal_type"] is None:
                raise PreventUpdate
            elif len(kwargs["signal_value_type"]) == 0:
                raise PreventUpdate

        dev = Device.get_from_pathname(kwargs["path"])
        if callback_context.triggered_id == "save_button":
            if new_or_edit == "new_alarm":
                alarm = save_alarm(**kwargs)
            else:
                alarm = save_alarm(int(new_or_edit), **kwargs)
            return (no_update, {"device": dev.name}, no_update)

        fig = None
        alarm_parameters = []
        alarm_fake_id = 0
        for sid in kwargs["sensor_ids"]:
            for stype in kwargs["signal_value_type"]:
                alarm_fake_id += 1
                alarm_parameters.append(
                    {
                        "alarm_id": alarm_fake_id,
                        "sensor_id": sid,
                        "label": kwargs["alarm_label"],
                        "signal_type": kwargs["signal_type"],
                        "signal_value_type": stype,
                        "min_": kwargs["min_"],
                        "max_": kwargs["max_"],
                        "logic_type": kwargs["logic_type"],
                        "logic_value": kwargs["logic_val"],
                    }
                )
        alarm_data = load_parent_alarm(dev, alarm_parameters, kwargs["query_period"])
        fig = draw_alarm_fig(alarm_data, kwargs["min_"], kwargs["max_"])

        return (fig, no_update, no_update)

        raise PreventUpdate

    def save_alarm(alarm_id=None, **kwargs):
        dev = Device.get_from_pathname(kwargs["path"])
        alarms_created = []

        # save parent alarm
        parent_label = kwargs["alarm_label"]
        communs_arguments = dict(
            min_range=kwargs["min_"] or 0,
            max_range=kwargs["max_"] or 0,
            signal_type=kwargs["signal_type"],
            alarm_logic=kwargs["logic_type"],
            logic_value=kwargs["logic_val"],
            query_period=kwargs["query_period"],
            rearming_period=kwargs["rearming_period"],
            group_id=kwargs["group_id"],
        )
        try:
            if len(kwargs["signal_value_type"]) == 1:
                communs_arguments["signal_value_type"] = kwargs["signal_value_type"].pop()
        except TypeError:
            pass
        try:
            if len(kwargs["sensor_ids"]) == 1:
                communs_arguments["sensor_id"] = kwargs["sensor_ids"].pop()
        except TypeError:
            pass

        if alarm_id is None:
            parent_alarm = dev.new_alarm(label=parent_label, **communs_arguments)
            alarms_created.append(parent_alarm)
        else:
            parent_alarm = Alarm.query.filter_by(id=alarm_id).one()
            # update parent alarm first (logic on what field to update is in the model)
            parent_alarm.update(label=parent_label, **communs_arguments)

        # ! delete children that are not in the list anymore
        q_sid_list = [None] if len(kwargs["sensor_ids"]) == 0 else kwargs["sensor_ids"]
        del_sid = (
            Alarm.query.filter_by(parent_id=parent_alarm.id)
            .filter(~Alarm._sensor_id.in_(q_sid_list))
            .delete()
        )

        q_vtype_list = [None] if len(kwargs["signal_value_type"]) == 0 else kwargs["signal_value_type"]
        del_vtype = (
            Alarm.query.filter_by(parent_id=parent_alarm.id)
            .filter(~Alarm._signal_value_type.in_(q_vtype_list))
            .delete()
        )
        if (del_sid + del_vtype) > 0:
            Alarm.db_commit()
            logger.info("Deleted %d alarms from parent %s", (del_sid + del_vtype), parent_alarm.label)

        sensor_ids_existing = [x.sensor_id for x in parent_alarm.children]
        if "sensor_id" in communs_arguments:
            sensor_ids_existing.append(communs_arguments.pop("sensor_id"))
        sensor_ids_existing = list(set(sensor_ids_existing))  # remove duplicate

        vtype_existing = [x.signal_value_type for x in parent_alarm.children]
        if "signal_value_type" in communs_arguments:
            vtype_existing.append(communs_arguments.pop("signal_value_type"))
        vtype_existing = list(set(vtype_existing))  # remove duplicate

        sensor_ids_to_add = list(set(kwargs["sensor_ids"]).difference(sensor_ids_existing))
        vtypes_to_add = list(set(kwargs["signal_value_type"]).difference(set(vtype_existing)))

        def add_children_alarm(sensor_ids, sig_val_types):
            for sid in sensor_ids:
                for vtype in sig_val_types:
                    alarm = dev.new_alarm(
                        **communs_arguments,
                        sensor_id=sid,
                        signal_value_type=vtype,
                        parent_id=parent_alarm.id,
                    )
                    alarms_created.append(alarm)

        add_children_alarm(kwargs["sensor_ids"], vtypes_to_add)
        add_children_alarm(sensor_ids_to_add, vtype_existing)

        return alarms_created

    return app.server
