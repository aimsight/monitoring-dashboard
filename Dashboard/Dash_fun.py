"""
  Copyright (c) 2020-2021  AIMsight SA
  Written By Olivier Chabloz
"""
from logging import getLogger
import datetime

from dash import html
import numpy as np, pandas as pd
from skimage.transform import EuclideanTransform, SimilarityTransform

from plotly.subplots import make_subplots
import plotly.graph_objects as go
import plotly.io as pio
import plotly.express as px
import pytz

from app.base.auth import is_authenticated, is_admin
from app.extensions import AIMsightDB_new_session

from .queries import build_sensor_list, get_sensors_data, get_sensors_from_device
from .processing import (
    mask_isolated_values,
    process_Sim_matrices,
    process_Euc_matrices,
    process_Sim_matrices_interp,
    angle_processing,
)
from .processing import (
    interpolation_prioritize,
    interpolation_standard,
    interpolation_with_resampling,
    apply_rolling_median,
)

logger = getLogger()


def apply_layout_with_auth(app, layout):
    def serve_layout():
        s = is_authenticated()
        if s is not None:
            return layout
        return html.Div("403 Access Denied")

    app.config.suppress_callback_exceptions = True
    app.layout = serve_layout


def apply_layout_for_admin(app, layout):
    def serve_layout():
        if is_admin():
            return layout
        return html.Div("403 Access Denied")

    app.config.suppress_callback_exceptions = True
    app.layout = serve_layout


def query_and_process_outputs(
    device,
    output_list,
    threshold,
    start_date,
    end_date,
    ref_measurement,
    attr_include=None,
):
    """
    Produce plots for the Explore page

    ! TODO: split this function and add bits to processing
    """
    sensors = []
    for item in output_list:
        sensors.append(item["sensor1"]), sensors.append(item["sensor2"])
    sensors = list(set(sensors))  # drop duplicates
    if None in sensors:
        sensors.remove(None)

    diff_data = None
    angle_data = None
    dist_data = None

    if len(sensors) == 0:
        return diff_data, angle_data, dist_data

    # add to the list of needed sensors, the one for global ref
    default_params = {
        "error_threshold": 0.7,
        "pattern_type": "target",
        "measurement_id": ref_measurement,
    }
    sensors_device = pd.DataFrame(
        build_sensor_list(
            device.serial_number,
            default_params,
            start_limit=device.start_date,
            load_archived=device.is_archive,
        )
    )
    if len(sensors_device) == 0:
        return diff_data, angle_data, dist_data
    sensors_to_fetch = sensors_device[
        sensors_device.sensor_id.isin(sensors) | (sensors_device.pattern_type == "global_ref")
    ]

    shifts = get_sensors_data(sensors_to_fetch, start_date, end_date, threshold=threshold)

    process_types = [o["process_type"] for o in output_list]
    corr_matrixes = {
        "euclidean_transform": None,
        "similarity_transform": None,
        "similarity_transform_interp": None,
    }
    if "similarity_transform" in process_types:
        corr_matrixes["similarity_transform"] = process_Sim_matrices(shifts)
    if "similarity_transform_interp" in process_types:
        corr_matrixes["similarity_transform_interp"] = process_Sim_matrices_interp(shifts)
    if "euclidean_transform" in process_types:
        corr_matrixes["euclidean_transform"] = process_Euc_matrices(shifts)

    for row in output_list:
        if row["sensor1"] is None or row["process_type"] is None:
            continue
        if not row["sensor1"] in set(shifts.sensor_id):
            logger.warning("Output %s configured with unknown sensor %s", row["name"], row["sensor1"])
            continue
        values_columns = [
            "measurement_id",
            "horizontal_shift",
            "vertical_shift",
            "x",
            "y",
            "x_ref",
            "y_ref",
        ]
        q = shifts[["sensor_id", "time_utc"] + values_columns]
        try:
            q = q.pivot(index="time_utc", columns="sensor_id", values=values_columns)
        except ValueError:
            dup = q[q.duplicated(subset=["sensor_id", "time_utc"], keep=False)]
            logger.critical("A duplicate time_utc has occurred: \n%s", dup)
        d1 = q.loc[:, (slice(None), row["sensor1"])]

        if row["sensor2"] is None:  # creating a "zero" sensor
            d2 = q.loc[:, (slice(None), row["sensor1"])].copy()
            d2.loc[:, values_columns] = 0.0
        else:
            d2 = q.loc[:, (slice(None), row["sensor2"])]
        d1.columns = d2.columns = d2.columns.get_level_values(0)

        if row["process_type"] == "relative_coord":
            diff = d1 - d2
            result = diff[["horizontal_shift", "vertical_shift"]].apply(lambda i: i * row["scale"])
        elif row["process_type"] in corr_matrixes:
            corr_mat = corr_matrixes[row["process_type"]]
            if corr_mat is None:
                logger.warning(
                    "Output %s configured as an %s when no Correction matrix are defined",
                    row["name"],
                    row["process_type"],
                )
                continue

            d1 = d1.merge(corr_mat, left_index=True, right_index=True)

            def apply_transform(row):
                t = SimilarityTransform(row.matrix)
                coords = np.array([row.x, row.y])
                tr = t.inverse(coords)
                row.x = tr[0][0]
                row.y = tr[0][1]
                return row

            d1 = d1.apply(apply_transform, axis=1)
            rx = d1["x"] - d1["x_ref"]
            ry = d1["y"] - d1["y_ref"]
            result = pd.DataFrame({"horizontal_shift": rx, "vertical_shift": ry})
            result = result * row["scale"]

        elif row["process_type"] == "dist":
            result = angle_processing(d1, d2, row["scale"])
        elif row["process_type"] == "angle":
            result = angle_processing(d1, d2, row["scale"])

        roll_med = int(row["roll_med"])
        # result = mask_isolated_values(result)
        if row["interp"] == "standard":
            result = interpolation_standard(result, roll_med)
        elif row["interp"] == "resample":
            result = interpolation_with_resampling(result, roll_med)

        elif row["interp"] == "interpolate_priority":
            result = interpolation_prioritize(result, roll_med)
            if result is None:
                logger.warning("%s: No valid points founds", row["name"])
                continue
        else:
            result = apply_rolling_median(result, roll_med)

        result.loc[:, "signal_name"] = row["name"]
        result["horizontal_shift"] += row["horizontal_offset"]
        result["vertical_shift"] += row["vertical_offset"]

        if attr_include is not None:
            for attr in attr_include:
                result.loc[:, attr] = row.get(attr, None)

        assign_or_append = lambda x, df: x if df is None else pd.concat([df, x])
        if row["process_type"] in ["relative_coord", *corr_matrixes.keys()]:
            diff_data = assign_or_append(result, diff_data)
        elif row["process_type"] == "dist":
            dist_data = assign_or_append(result, dist_data)
        elif row["process_type"] == "angle":
            angle_data = assign_or_append(result, angle_data)

    return diff_data, dist_data, angle_data


def create_plot(
    df,
    x,
    y,
    groupby,
    rows=1,
    y_title="",
    x_title="",
    sub_titles=("",),
    theme="plotly",
    notifications=[],
    min_autoscale=None,
    max_autoscale=None,
):
    """
    Create a figure
    df: pandas.DataFrame, The full DataFrame
    x: str, column name for the X axis
    y: str, column name for the Y axis
    groupby: str, column name for the signal name
    rows: int, 1. Other values will add a row but no way to add data to this row
    y_title: str, axis name
    x_title: str, axis name
    sub_titles: tuple of str, subplots titles
    theme: str, theme name, part of pio.templates.keys()
    notifications, list of dict, dict must be {"title": str, "date": date(), "description": str}
    """
    fig = make_subplots(
        rows=rows,
        cols=1,
        shared_xaxes=True,
        vertical_spacing=0.04,
        y_title=y_title,
        x_title=x_title,
        subplot_titles=sub_titles,
    )
    fig.layout.template = theme

    colors = pio.templates[theme].layout["colorway"]
    if colors is None:
        colors = px.colors.qualitative.D3
    max_color = len(colors)
    for i, ysel in enumerate(y):
        for j, signal in enumerate(df[groupby].unique()):
            logger.info("%s: color: %s", signal, colors[j % max_color])
            fig.add_trace(
                go.Scattergl(  # Scatter or Scattergl to use
                    x=df[x][df[groupby] == signal],
                    y=df[ysel][df[groupby] == signal],
                    mode="lines",
                    line=dict(color=colors[j % max_color]),
                    name=signal,
                    showlegend=i == 0,
                ),
                row=i + 1,
                col=1,
            )
    for n in notifications:
        draw_notification_on_fig(fig, n["title"], n["date"], n["description"])

    min_yrange, max_yrange = df[y[0]].min(), df[y[0]].max()
    full_yrange = abs(min_yrange - max_yrange)
    # ? display marks on the Y axis every millimeter. ONLY when the full range is lower than 50mm.
    if full_yrange < 50:
        fig.update_yaxes(tickmode="linear", tick0=0.0, dtick=1.0)

    fig.update_layout(transition_duration=500)
    if min_autoscale is not None:
        min_yrange = min(min_autoscale, min_yrange)
    if max_autoscale is not None:
        max_yrange = max(max_autoscale, max_yrange)

    fig.update_layout(yaxis_range=[min_yrange, max_yrange])
    return fig


def draw_notification_on_fig(fig, title, date, description):
    line_style = dict(color="Black", width=2)
    fig.add_shape(type="line", xref="x", yref="paper", x0=date, y0=0, x1=date, y1=1, line=line_style)
    fig.add_annotation(
        xref="x",
        yref="paper",
        x=date,
        y=0.8,
        text=title,
        showarrow=False,
        xanchor="right",
        hovertext=description,
        textangle=90,
    )


def save_sensors_params(sensorsList, parameters_to_save, db=None):
    """
    save parameters to the db.

    sensorsList: a dict such as {"serialNumber": {"key1": val1, "key2: val2, "key3": val3}}
    parameters_to_save: a list of keys to update/add: ["key1", "key3"]

    ! This way of updating the sensors is a bit weird.
    ! Ideally this method would be implemented in the sensor model.
    """
    if db is None:
        db = AIMsightDB_new_session()
    if len(parameters_to_save) == 0:
        raise ValueError("parameters_to_save is empty")
    need_to_commit = False
    for sensor, parameters in sensorsList.items():
        sensor_id = parameters["sensor_id"]
        s = db.get_sensor(sensor_id)
        for p in parameters_to_save:
            r = s.parameters.get(p, None)
            if r != parameters[p]:
                s.parameters[p] = parameters[p]
                need_to_commit = True

    if need_to_commit:
        db.commit_session()
    return need_to_commit


def load_alarm(
    dev,
    sensor_id,
    signal_type,
    signal_value_type,
    min_val,
    max_val,
    alarm_logic_type,
    alarm_logic_val,
    query_period_h,
    end_date=None,
):
    end_date = end_date or datetime.datetime.now().astimezone(pytz.timezone("Europe/Zurich"))
    start_date = end_date - datetime.timedelta(hours=query_period_h)

    shifts = None
    if signal_type == "device_output":
        outs = {v.id: v for v in dev.outputs.values()}
        if sensor_id in outs:
            shifts, _, _ = query_and_process_outputs(
                dev,
                [outs[sensor_id].as_dict()],
                dev.threshold,
                start_date=start_date,
                end_date=end_date,
                ref_measurement=dev.reference_measurement,
            )
            shifts = shifts[[signal_value_type]]

    elif signal_type == "sensor":
        sensors = get_sensors_from_device(dev)
        sensors = sensors.query(f"sensor_id =={sensor_id}")
        shifts = get_sensors_data(sensors, start_date, end_date, threshold=dev.threshold)
        shifts = shifts[["time_utc", signal_value_type]].set_index("time_utc")

    shifts.loc[:, "out_of_bound"] = (shifts.iloc[:, 0] < min_val) | (shifts.iloc[:, 0] > max_val)

    if alarm_logic_type == "percentage":
        total = len(shifts)

        cumulative_count = shifts.out_of_bound.astype(int).cumsum()
        shifts.loc[:, "trigger_value"] = (cumulative_count / total) * 100
    elif alarm_logic_type == "consecutive_count":
        alarm_logic_val = 1 if alarm_logic_val is None else alarm_logic_val
        conv_kernel = [1] * int(alarm_logic_val)
        shifts.loc[:, "trigger_value"] = np.nan
        out_of_bound_int = shifts.out_of_bound.astype(int)
        shifts.iloc[(int(alarm_logic_val) - 1) :, -1] = np.convolve(out_of_bound_int, conv_kernel, "valid")

    else:
        raise ValueError(f"alarm_logic_type invalid value '{alarm_logic_type}'")
    shifts.loc[:, "trigger"] = shifts.loc[:, "trigger_value"] >= alarm_logic_val
    return shifts


def set_value(df):
    df.loc[:, "value"] = df.loc[:, df.name]
    return df


def evaluate_cumulative_count(df):
    conv_kernel = [1] * int(df.iloc[0].logic_value)
    df.loc[:, "trigger_value"] = np.nan
    out_of_bound_int = df.out_of_bound.astype(int)
    df.iloc[(len(conv_kernel) - 1) :, -1] = np.convolve(out_of_bound_int, conv_kernel, "valid")
    return df


def load_parent_alarm_from_model(dev, parent_alarm, active_only=False):
    alarms_to_query = parent_alarm.children if len(parent_alarm.children) > 0 else [parent_alarm]
    alarm_parameters = [
        {
            "alarm_id": x.id,
            "sensor_id": x.sensor_id,
            "label": x.label,
            "signal_type": x.signal_type,
            "signal_value_type": x.signal_value_type,
            "min_": x.min_range,
            "max_": x.max_range,
            "logic_type": x.alarm_logic,
            "logic_value": x.logic_value,
        }
        for x in alarms_to_query
        if (not active_only) or x.active
    ]
    return load_parent_alarm(dev, alarm_parameters, parent_alarm.query_period)


def load_parent_alarm(dev, alarm_parameters, query_period):
    end_date = datetime.datetime.now().astimezone(pytz.timezone("Europe/Zurich"))
    start_date = end_date - datetime.timedelta(hours=query_period)
    # * query the parent alarm when no children

    alarm_parameters = pd.DataFrame(alarm_parameters)
    if alarm_parameters.empty:
        return pd.DataFrame()
    sensor_ids = alarm_parameters.sensor_id.to_list()
    signal_types = alarm_parameters.signal_type.unique().tolist()
    if len(signal_types) > 1:
        raise ValueError("does not support multiple signal types alarms")

    if signal_types[0] == "sensor":
        sensors = get_sensors_from_device(dev)
        sensors = sensors.query(f"sensor_id =={sensor_ids}")
        shifts = get_sensors_data(sensors, start_date, end_date, threshold=dev.threshold)

        signal_value_types = alarm_parameters.signal_value_type.unique().tolist()
        shifts = shifts[["time_utc", "sensor_id", "sensor_name"] + signal_value_types]
        alarm_data = alarm_parameters.merge(shifts)

    elif signal_types[0] == "device_output":
        shifts, _, _ = query_and_process_outputs(
            dev,
            [x.as_dict() for x in dev.outputs.values() if x.id in sensor_ids],
            dev.threshold,
            start_date=start_date,
            end_date=end_date,
            ref_measurement=dev.reference_measurement,
            attr_include=["device_output_id"],
        )
        shifts = shifts.reset_index()
        shifts.rename(columns={"device_output_id": "sensor_id", "signal_name": "sensor_name"}, inplace=True)
        alarm_data = alarm_parameters.merge(shifts)
    else:
        return pd.DataFrame()

    alarm_data = alarm_data.groupby("signal_value_type").apply(set_value)
    alarm_data.drop(columns=alarm_data.signal_value_type.unique(), inplace=True)
    alarm_data.drop(columns=["sensor_id"], inplace=True)

    alarm_data.loc[:, "out_of_bound"] = (alarm_data.value < alarm_data.min_) | (
        alarm_data.value > alarm_data.max_
    )
    alarm_data.loc[alarm_data.logic_value.isna(), "logic_value"] = 1.0
    cc = alarm_data.loc[alarm_data.logic_type == "consecutive_count"]
    cc = cc.groupby("alarm_id").apply(evaluate_cumulative_count)

    pc = alarm_data.loc[alarm_data.logic_type == "percentage"]
    pc.loc[:, "trigger_value"] = pc[["alarm_id", "out_of_bound"]].groupby("alarm_id").transform("cumsum")
    try:
        total = pc[["alarm_id", "out_of_bound"]].groupby("alarm_id").transform(len)
    except ValueError:  # pc is empty
        pass
    else:
        pc.loc[:, "trigger_value"] = (pc.loc[:, "trigger_value"] / total.out_of_bound) * 100

    alarm_data = pd.concat((cc, pc))
    alarm_data.reset_index(drop=True, inplace=True)
    alarm_data.loc[:, "trigger"] = alarm_data.loc[:, "trigger_value"] >= alarm_data.loc[:, "logic_value"]

    return alarm_data
