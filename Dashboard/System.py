"""
  Copyright (c) 2020-2021  AIMsight SA
  Written By Olivier Chabloz
"""
import logging
import datetime
import json
import pandas as pd
from base64 import b64encode
from dash import Dash, callback_context, dcc, html
from dash.exceptions import PreventUpdate
from dash.dependencies import Input, Output, ClientsideFunction
import dash_bootstrap_components as dbc
import plotly.graph_objects as go

from paho.mqtt import publish
from app.extensions import AIMsightDB_new_session, AIMsight_db
from .Dash_fun import apply_layout_with_auth
from .queries import get_system_info, get_system_values

logger = logging.getLogger()

url_base = "/dash/system/"

flags = {
    "last_sync": "Last Server Syncronisation",
    "BusVoltage": "Battery Voltage",
    "Power": "Power Consumption",
    "BusCurrent": "Current Consumption",
    "ShuntVoltage": "Shunt Voltage",
    "sensor_CPU_deg_c": "CPU tempertature °c",
    "sensor_GPU_deg_c": "GPU tempertature °c",
    "sensor_A0_deg_c": "A0 tempertature °c",
    "sensor_thermal_deg_c": "thermal tempertature °c",
    "sensor_PLL_deg_c": "PLL tempertature °c",
    "mode": "Mode",
    "avail_disk_1k-blk": "available disk blocs",
    "total_disk_1k-blk": "Total disk blocs",
    "used_disk_1k-blk": "Used disk bloks",
    "eth0": "Ethernet IP address",
    "modem": "Modem IP address",
    "nebula1": "Nebula IP address",
    "SIM_CCID": "Sim card CCID",
    "rsrp_dBm": "Cellular signal dBm",
    "rsrq_dB": "Cellular signal dB",
    "sonar_distance_mm": "Sonar distance [mm]",
    "sonar_confidence": "Confidence %",
    "sonar_temp_deg": "Sonar Board temperature °c",
    "sonar_cpu_temp_deg": "Sonar CPU temperature °c",
}


def render_acq_unit(acq_unit, meas_type):
    style = {}
    serial_number = acq_unit.serial_number
    m = (
        acq_unit.measurements.filter_by(measurement_type=meas_type)
        .order_by(AIMsight_db.T.Measurement.time_utc.desc())
        .first()
    )
    img = None
    video = None
    body = None

    if len(m.data) > 5 and m.data[:4] == b"\x89PNG":
        img = f"data:image/png;base64,{b64encode(m.data).decode('utf-8')}"
    elif len(m.data) > 5 and m.data[:3] == b"\xff\xd8\xff" and m.data[-2:] == b"\xff\xd9":
        # JPEG signature
        img = f"data:image/jpeg;base64,{b64encode(m.data).decode('utf-8')}"
    elif len(m.data) > 25 and m.data[15:23] == b"matroska":
        video = f"data:video/mkv;base64,{b64encode(m.data).decode('utf-8')}"  # not supported by browsers
    elif meas_type.subtype in ["JSON"]:
        if meas_type.name == "SYSLOG":
            d = [json.loads(l) for l in m.data.decode("utf-8").splitlines()]
            body = html.Pre(
                "\n".join([f'{l["@timestamp"]}: {l["message"]}' for l in d]), style={"max-height": "300px"}
            )
        else:
            body = html.Pre(m.data.decode("utf-8"))
    elif len(m.data) < 100:
        body = html.Pre(m.data.decode("utf-8"))
    if img is not None:
        style = {
            "background-image": f"url('{img}')",
            "background-size": "cover",
        }

    text_style = {"color": "#a3a3a3", "mix-blend-mode": "exclusion"}
    if video is not None:
        body_bloc = html.Video(
            [html.Source(src=video, type="video/mp4")], autoPlay=True, muted=True, loop=True
        )
    else:
        body_bloc = html.P(body, style=text_style)

    jumbo = dbc.Col(
        html.Div(
            [
                html.H2([serial_number], className="display-3", style=text_style),
                html.P(meas_type.full_name, className="lead", style=text_style),
                html.Hr(className="my-2"),
                body_bloc,
            ],
            className="h-100 p-5 border rounded-3",
            style=style,
        ),
        md=6,
    )
    return jumbo


layout = html.Div(
    [
        dcc.Location(id="explore_location"),
        dcc.Store(id="system_datastore", data={}),
        dbc.Row(
            dbc.Select(
                id="select",
                options=[{"label": label, "value": value} for value, label in flags.items()],
                value="BusVoltage",
            ),
        ),
        dbc.Row(
            [
                dbc.Col(
                    dcc.Graph(
                        id="bat_graph",
                    ),
                ),
            ]
        ),
        dbc.Row(dbc.Col([html.Div(id="system_status", children=[])])),
        dbc.Row(
            dbc.Col(
                [
                    dbc.Select(
                        id="device_mode",
                        options=[{"label": "Waken up", "value": "WUP"}, {"label": "Sleep", "value": "SLP"}],
                    )
                ]
            )
        ),
        dbc.Row(children=[], id="system_acquisition_units", style={"margin-top": "40px"}),
    ],
    style={"overflow-x": "hidden", "min-height": "390px"},
)


def Add_Dash(server):
    app = Dash(
        server=server,
        url_base_pathname=url_base,
        external_stylesheets=[dbc.themes.BOOTSTRAP],
        external_scripts=[
            "/static/build/js/iframeResizer.contentWindow.min.js",
            "/static/build/js/explore.js",
        ],
    )
    apply_layout_with_auth(app, layout)

    @app.callback(
        [Output("bat_graph", "figure")],
        [Input("explore_location", "pathname"), Input("bat_graph", "relayoutData"), Input("select", "value")],
    )
    def update_battery_graph(pathname, relayout, flag):
        system_name = pathname.split("/")[-1]
        try:
            min_date = relayout.get("xaxis.range[0]", None)
        except AttributeError:
            min_date = None
        if not min_date is None:
            min_date = datetime.datetime.strptime(min_date, "%Y-%m-%d %H:%M:%S.%f")
            days_limit = (datetime.datetime.now() - min_date).days + 1
        elif callback_context.triggered_id == "bat_graph":
            if (relayout is not None) and ("autosize" in relayout):
                days_limit = 10
            else:
                raise PreventUpdate
        else:
            days_limit = 10

        batt = get_system_values(system_name, flag, days_limit)
        if batt.empty:
            raise PreventUpdate
        batt.rename(columns={flag: "value"}, inplace=True)
        bat = batt.interpolate("time")
        bat.reset_index(inplace=True)

        bat.loc[:, "date"] = bat.time_utc.apply(lambda x: x.date())

        diff = bat.time_utc.diff()
        interruption_time_d = diff.median() * 2

        interrupted = bat.time_utc.diff() > interruption_time_d
        interr_ranges = bat.loc[(interrupted.shift(-1)) | interrupted]
        interr = bat.loc[(interrupted.shift(-1)) | False].time_utc + pd.Timedelta(minutes=1)
        i = pd.DataFrame(dict(time_utc=interr, volt=[None] * len(interr), date=[None] * len(interr)))
        bat = pd.concat([bat, i], ignore_index=True)
        bat = bat.sort_values("time_utc").reset_index(drop=True)

        idx_min = bat.loc[~bat.value.isnull(), ["date", "value"]].groupby(["date"]).idxmin()["value"]
        idx_max = bat.loc[~bat.value.isnull(), ["date", "value"]].groupby(["date"]).idxmax()["value"]

        def timedelta_fmt(tdelta):
            h, rem = divmod(tdelta.seconds, 3600)
            if tdelta.days > 0:
                return f"{tdelta.days} days, {h} hours"
            m, _ = divmod(rem, 60)
            return f"{h} hours, {m} minutes"

        fig = go.Figure()
        interr_ranges_diff = interr_ranges.diff()
        for i in range(len(interr_ranges) // 2):
            fig.add_vrect(
                x0=interr_ranges.iloc[i * 2]["time_utc"],
                x1=interr_ranges.iloc[i * 2 + 1]["time_utc"],
                fillcolor="LightCoral",
                layer="below",
                line_width=0,
                opacity=0.5,
            )
            fig.add_annotation(
                text=timedelta_fmt(interr_ranges_diff.iloc[i * 2 + 1]["time_utc"]),
                xref="x",
                yref="paper",
                font=dict(color="red"),
                x=interr_ranges.iloc[i * 2]["time_utc"],
                y=0.8,
                showarrow=False,
                xanchor="left",
                textangle=90,
            )

        mi = dict(color="LightSkyBlue", size=8, line=dict(color="MediumPurple", width=2))
        ma = dict(color="MediumAquamarine", size=8, line=dict(color="Teal", width=2))
        fig.add_trace(go.Scatter(x=bat["time_utc"], y=bat["value"], mode="lines", name="Battery Voltage"))
        fig.add_trace(
            go.Scatter(
                x=bat.loc[idx_min, "time_utc"],
                y=bat.loc[idx_min, "value"],
                text=bat.loc[idx_min, "value"],
                textposition="bottom center",
                mode="markers+text",
                name="lowest daily voltage",
                marker=mi,
            )
        )
        fig.add_trace(
            go.Scatter(
                x=bat.loc[idx_max, "time_utc"],
                y=bat.loc[idx_max, "value"],
                text=bat.loc[idx_max, "value"],
                textposition="top center",
                mode="markers+text",
                name="highest daily voltage",
                marker=ma,
            )
        )
        if isinstance(relayout, dict):
            min_range = relayout.get("xaxis.range[0]", None)
            max_range = relayout.get("xaxis.range[1]", None)
            if min_range is not None:
                fig.update_layout(xaxis_range=[min_range, max_range])

        return (fig,)

    @app.callback(
        [
            Output("system_status", "children"),
            Output("device_mode", "value"),
            Output("select", "options"),
        ],
        [
            Input("device_mode", "value"),
            Input("explore_location", "pathname"),
        ],
    )
    def update_system_status(device_mode, pathname):
        system_name = pathname.split("/")[-1]
        if callback_context.triggered_id == "explore_location":
            info = get_system_info(system_name=system_name, flags=list(flags.keys()))
            body = []
            for k, v in info.items():
                val = html.Td(html.Code(v)) if isinstance(v, str) else html.Td(html.Var(str(v)))
                body.append(html.Tr([html.Th(flags[k], scope="row"), val]))

            table_body = [html.Tbody(body)]
            device_mode = info.get("mode", "WUP")

            options = [
                {"label": label, "value": value, "disabled": value not in info}
                for value, label in flags.items()
            ]

            return (dbc.Table(table_body, bordered=True), device_mode, options)
        elif callback_context.triggered_id == "device_mode":
            publish.single(
                f"aimsight/MAD/{system_name}/mode",
                device_mode.encode("utf-8"),
                retain=True,
                hostname="10.0.1.20",
                port=32427,
                auth={"username": "gland_test", "password": "gland_test"},
            )
        raise PreventUpdate

    @app.callback(Output("system_acquisition_units", "children"), Input("explore_location", "pathname"))
    def display_system_acquisition_units(pathname):
        system_name = pathname.split("/")[-1]
        db = AIMsightDB_new_session()
        acc = db.get_account(system_name)
        q = db.session.query(
            db.T.Measurement.acquisition_unit_id, db.T.Acquisition_unit, db.T.Measurement_type
        ).filter_by(account=acc)
        q = q.filter(db.T.Measurement.time_utc > datetime.datetime.now() - datetime.timedelta(days=5))
        q = q.join(db.T.Acquisition_unit).join(db.T.Measurement_type).distinct()
        acq_units = [r[1:] for r in q]

        return [render_acq_unit(a[0], a[1]) for a in acq_units]

    return app.server


# publish.single(
#                 f"aimsight/MAD/{system_name}/mode",
#                 device_mode.encode("utf-8"),
#                 retain=True,
#                 hostname="10.0.1.20",
#                 port=32427,
#                 auth={"username": "gland_test", "password": "gland_test"},
#             )
